/**
 * Author   : Sugiarto
 * Email    : agie0925@gmail.com
 * Date     : 20 April 2014
 */
$(document).ready(function() {



    /*
     * Document
     */
    $('#DocumentCategoryName').keyup(function() {
        var name = $('#DocumentCategoryName').val();
        var permalink = name.replace(/\s+/g, '-').toLowerCase();
        $('#DocumentCategoryPermalink').val(permalink);
    });
    $('#DocumentCategoryNameEn').keyup(function() {
        var permalink = $('#DocumentCategoryNameEn').val();
        permalink = permalink.replace(/\s+/g, '-').toLowerCase();
        $('#DocumentCategoryPermalinkEn').val(permalink);
    })

    $('#generateKey').click(function() {
        $.ajax({
            method: "POST",
            url: SERVER + 'documents/generateKey',
            success: function(msg) {
                if (msg) {
                    $('#DocumentAccessKey').val(msg);
                } else {
                    alert('Failed in generate key !');
                }
            }

        });
    });

    /**
     * End Document Js  
     */


});
    