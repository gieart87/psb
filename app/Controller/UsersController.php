<?php

App::uses('AppController', 'Controller');
App::uses('Sanitize', 'Utility');
//App::import('Vendor', 'rest/response');

/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

    var $uploadTempdir = "files/uploads";
    public $components = array('Recaptcha.Recaptcha');
    public $helpers = array('Recaptcha.Recaptcha');

    /**
     * index method
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array_merge($this->Auth->allowedActions, array(
            'sync',
            'login',
            'logout',
            'register',
            'activate',
            'deactivate',
            'do_deactivate',
            'forgot_password',
            'reset_password',
            'autocomplete',
            'confirm_sync',
            'facebook_auth',
            'view'
                )
        );

        $this->set('status', $this->User->status);
        $this->AclFilter->protection();
        $this->uploadTempdir = "files/uploads/";
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function admin_profile() {
        $this->User->id = $this->Auth->user('id');
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->request->data['User']['password'] == '' || empty($this->request->data['User']['password'])) {
                unset($this->request->data['User']['password']);
                unset($this->request->data['User']['confirm_password']);
            } else {
                $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
            }
//            if ($this->request->data['User']['avatar']['error'] == 4) {
//                unset($this->request->data['User']['avatar']);
//            }
            // var_dump($this->request->data);exit;
            if ($this->User->save($this->request->data)) {


                $this->Session->setFlash(__('The user has been saved'), 'success');
                $this->redirect(array('action' => 'profile'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
            }
        }
        $this->request->data = $this->User->read(null, $this->User->id);
    }

    public function forgot_password() {
        if ($this->request->is('post')) {
            $user = $this->User->findByUsername($this->request->data['User']['username']);


            if (!empty($user['User'])) {
                $resetToken = $this->General->generateReservationCode(20);
                $saveToken['User']['id'] = $user['User']['id'];
                $saveToken['User']['request_reset_token'] = $resetToken;
                $this->User->save($saveToken);

                $getToken = $this->User->findById($user['User']['id']);
                $token = $getToken['User']['request_reset_token'];

                $resetPassword['link_reset'] = Router::url('/', true) . 'users/reset_password?token=' . $token . '&email=' . $user['User']['username'];
                $resetPassword['username'] = $user['User']['username'];
                if ($this->CakeMailer->resetPassword($resetPassword) == TRUE) {
                    $this->Session->setFlash(__('Instrusksi reset password telah dikirim ke email Anda. Silahkan dicek !. Terima kasih'), 'success');
                } else {
                    $this->Session->setFlash(__('Proses gagal dilakukan, kontak ke support kami'), 'error');
                }
            } else {
                $this->Session->setFlash(__('Username dan email tidak ditemukan'), 'error');
            }
            $this->redirect(array('action' => 'forgot_password'));
        }
    }

    public function reset_password() {
        $errors = '';
        if (empty($this->params->query['token'])) {
            $errors .= 'Kode reset password harus disertakan<br/>';
        }

        if (empty($this->params->query['email'])) {
            $errors .= 'Email harus disertakan<br/>';
        }

        if (!empty($this->params->query['token']) && !empty($this->params->query['email'])) {
            $token = $this->params->query['token'];
            $username = $this->params->query['email'];

            $conditions = array(
                'User.username' => $username,
                'User.request_reset_token' => $token,
                'User.status' => 1
            );

            $user = ClassRegistry::init('User')->find('first', array('conditions' => $conditions));
            if (empty($user['User'])) {
                $this->Session->setFlash(__('Token untuk proses reset password tidak valid'), 'error');
                $this->redirect(array('action' => 'login'));
            } else {
                $this->request->data['User']['id'] = $user['User']['id'];
                $newPassword = $this->General->generateRandom(8);
                $this->request->data['User']['password'] = md5($newPassword);
                $this->request->data['User']['confirm_password'] = $newPassword;

                $mailData['username'] = $user['User']['username'];
                $mailData['password'] = $newPassword;
                $mailData['link_login'] = $this->General->getSetting('Main.SiteUrl') . 'users/login';
                if (ClassRegistry::init('User')->save($this->request->data)) {
                    $this->CakeMailer->newPassword($mailData);
                    $this->Session->setFlash(__('Password baru telah dikirimkan ke email Anda'), 'success');
                    $this->redirect(array('action' => 'login'));
                } else {
                    $this->Session->setFlash(__('Proses reset password gagal,ulangi lagi!'), 'error');
                    $this->redirect(array('action' => 'login'));
                }
            }
        }
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        ClassRegistry::init('User')->unbindModel(array(
            'hasOne' => array('Admin', 'Author', 'Student', 'Librarian', 'PaymentPoint', ''),
            'hasMany' => array('Page', 'Comment', 'SynchronizeToken', 'Student'),
            'hasAndBelongsToMany' => array('Collection', 'Category')
        ));
        $users = $this->User->find('all', array('order' => array('User.id' => 'DESC')));
        $this->set('users', $users);
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
            }
        }
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'), 'error');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->User->read(null, $id);
        }
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'), 'success');
        }
        if ($this->User->delete()) {
            $conditions = array(
                'UserFollowing.user_id' => $id,
            );
            ClassRegistry::init('UserFollowing')->deleteAll($conditions);

            $conditions = array(
                'UserFollowing.recipient_type' => 'User',
                'UserFollowing.recipient_key' => $id
            );
            ClassRegistry::init('UserFollowing')->deleteAll($conditions);

            $this->Session->setFlash(__('User deleted'), 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

    public function login() {
        if ($this->request->is('post')) {
            $options = array();
            $options['fields'] = array('User.id,User.name,User.username,User.password,User.status');
            $options['conditions'] = array(
                'User.username' => $this->request->data['User']['username'],
                'User.password' => md5($this->request->data['User']['password']),
            );

            $user = ClassRegistry::init('User')->find('first', $options);

            if (!empty($user)) {
                if ($user['User']['status'] == 1) {

                    $this->Session->write('Auth.User', $user['User']);

                    if ($this->General->isAdmin($this->Auth->user('id')) == TRUE) {
                        $this->redirect(array('controller' => 'siswas', 'action' => 'index', 'admin' => true));
                    } else {
                        $this->redirect('/');
                    }
                } elseif ($user['User']['status'] == 2) {
                    $this->Session->setFlash(__('Akun sedang di blok'), 'error');
                    $this->redirect(array('controller' => 'users', 'action' => 'login'));
                } else {
                    $this->Session->setFlash(__('Akun belum di aktifasi'), 'error');
                    $this->redirect(array('controller' => 'users', 'action' => 'login'));
                }
            } else {

                $this->Session->setFlash(__('Kombinasi username dan password tidak valid, ulangi lagi !'), 'error');
                $this->redirect(array('controller' => 'users', 'action' => 'login'));
            }
        }
        $this->set('title_for_layout', 'Please sign in!');
    }

    public function logout() {
        $this->uploadTempdir = $this->uploadTempdir . $this->sessionId . '/';

        $this->General->rrmdir($this->uploadTempdir);

        $this->Session->setFlash(__('Log out successful.', true), 'success');
        //$this->Auth->logout();
        $this->Session->delete('Auth');
        $this->redirect('/');
    }

    public function register() {
        Configure::load('Recaptcha.key');
        if ($this->request->is('post')) {

            $this->request->data['User']['password'] = md5($this->request->data['User']['password']);
            $this->request->data['User']['status'] = 1;
            $this->request->data['Customer']['name'] = $this->request->data['User']['name'];
            $this->request->data['User']['balance'] = $this->initDeposit;
//            $this->request->data['User']['request_reset_token'] = $this->General->generateReservationCode(20);
            unset($this->User->validate['avatar']);
            if ($this->User->saveAll($this->request->data)) {
//                $this->request->data['Customer']['name'] = $this->request->data['User']['name'];
//                $this->request->data['Customer']['phone'] = $this->request->data['User']['phone'];
//                $this->request->data['Customer']['address'] = $this->request->data['User']['address'];
//                $this->request->data['Customer']['user_id'] = $this->User->id;
//                ClassRegistry::init('Customer')->save($this->request->data);
                #--------------------------Newsletter------------------#
                $categories = ClassRegistry::init('Category')->find('list');
                $categoryIds = array();
                if (!empty($categories)) {
                    foreach ($categories as $id => $category) {
                        $categoryIds[] = $id;
                    }

                    $newsletter['Category']['Category'] = $categoryIds;
                    $newsletter['User']['id'] = $this->User->id;
                    $this->User->save($newsletter);
                }

                $user = $this->User->findById($this->User->id);
                // Save Default Notification Configs
                $notificationConfigs = ClassRegistry::init('Notification')->configs;
                foreach ($notificationConfigs as $key => $value) {
                    ClassRegistry::init('NotificationConfig')->create();
                    $config['NotificationConfig']['user_id'] = ClassRegistry::init('User')->id;
                    $config['NotificationConfig']['key'] = $key;
                    $config['NotificationConfig']['value'] = $value;
                    ClassRegistry::init('NotificationConfig')->save($config);
                }
                //============================================

                $this->CakeMailer->welcome($user);

                $this->Session->setFlash(__('Selamat, anda telah terdaftar sebagai Moco user!'), 'success');
                $this->redirect(array('action' => 'login'));
            } else {
                $this->Session->setFlash(__('Registrasi gagal karena suatu masalah, silahkan ulangi lagi!'), 'error');
            }
        }
        $this->set('title_for_layout', 'Please Register');
    }

    public function activate() {
        $errors = '';
        if (empty($this->params->query['token'])) {
            $errors .= 'Kode aktivasi harus disertakan<br/>';
        }

        if (empty($this->params->query['email'])) {
            $errors .= 'Email harus disertakan<br/>';
        }

        if (!empty($this->params->query['token']) && !empty($this->params->query['email'])) {
            $conditions = array(
                'User.username' => $this->params->query['email'],
            );
            $user = ClassRegistry::init('User')->find('first', array('conditions' => $conditions));
            if (!empty($user)) {
                $conditions = array(
                    'SynchronizeToken.user_id' => $user['User']['id'],
                    'SynchronizeToken.type' => 'User',
                    'SynchronizeToken.key' => $user['User']['id'],
                    'SynchronizeToken.description' => 'activate',
                    'SynchronizeToken.expired >' => date("Y-m-d")
                );
                $token = ClassRegistry::init('SynchronizeToken')->find('first', array('conditions' => $conditions));
                if ($token) {
                    $conditions = array(
                        'User.id' => $user['User']['id']
                    );

                    $fields = array(
                        'User.status' => 1,
                    );

                    if (ClassRegistry::init('User')->updateAll($fields, $conditions)) {
                        $this->CakeMailer->welcome($user);
                        $this->Session->setFlash(__('Akun Anda saat ini sudah aktif, silahkan login dengan username dan password pada saat mendaftar'), 'success');
                        $this->redirect('http://moco.co.id/');
                    } else {
                        $this->Session->setFlash(__('Proses aktivasi akun gagal, silahkan kontak Administrator'), 'error');
                        $this->redirect(array('action' => 'login'));
                    }
                } else {
                    $this->Session->setFlash(__('Proses aktivasi gagal karena kode telah expired'), 'error');
                    $this->redirect(array('action' => 'login'));
                }
            } else {
                $this->Session->setFlash(__('Proses aktivasi gagal karena kode aktivasi / email Anda tidak valid'), 'error');
                $this->redirect(array('action' => 'login'));
            }
        }
    }

    public function deactivate() {
        $errors = '';
        if (empty($this->params->query['token'])) {
            $errors .= 'Kode aktivasi harus disertakan<br/>';
        }

        if (empty($this->params->query['email'])) {
            $errors .= 'Email harus disertakan<br/>';
        }

        if (!empty($this->params->query['token']) && !empty($this->params->query['email'])) {
            $conditions = array(
                'User.username' => $this->params->query['email'],
            );
            $user = ClassRegistry::init('User')->find('first', array('conditions' => $conditions));

            if (!empty($user)) {
                $conditions = array(
                    'SynchronizeToken.user_id' => $user['User']['id'],
                    'SynchronizeToken.type' => 'User',
                    'SynchronizeToken.key' => $user['User']['id'],
                    'SynchronizeToken.description' => 'deactivate',
//                    'SynchronizeToken.expired >' => date("Y-m-d")
                );
                $token = ClassRegistry::init('SynchronizeToken')->find('first', array('conditions' => $conditions));
                $this->request->data['User']['username'] = $token['User']['username'];
                $this->request->data['User']['token'] = $token['SynchronizeToken']['token'];
            } else {
                $this->Session->setFlash(__('Proses deaktivasi gagal karena kode aktivasi / email Anda tidak valid'), 'error');
                $this->redirect(array('action' => 'login'));
            }
        }
    }

}
