<?php

/*
 * Controller/SiswasController.php
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */

class SiswasController extends AppController {

    var $paginate = array(
        'limit' => 15
    );
//    var $PhpExcel = '';
    var $components = array(
        'PhpExcel.PhpExcel'
    );
    var $kelamins = array(
        1 => 'laki-laki',
        2 => 'perempuan'
    );
    var $jurusans = array();
    var $sekolahs = array();

    function beforeFilter() {
        parent::beforeFilter();
        $this->AclFilter->protection();

//        CakePlugin::load('PhpExcel');
//        $this->PhpExcel = new PHPExcel();

        $this->set('status', $this->Siswa->status);
        $this->activeMenu = 'event';
        $this->set('activeMenu', $this->activeMenu);

        $this->set('kelamins', $this->kelamins);

        $this->sekolahs = ClassRegistry::init('Sekolah')->find('list', array('order' => array('Sekolah.nama' => 'ASC')));
        $this->set('sekolahs', $this->sekolahs);
        $this->jurusans = ClassRegistry::init('Jurusan')->find('list');
        $this->set('jurusans', $this->jurusans);
    }

    function admin_index() {

        // report ke dinas
//        Nama Sekolah , daya tampung, jumlah pendaftar(l,p, total), jml diterima (l,p,total), rata (b indo, ing, mat, ipa), nilai un diterima (tertinggi, terendah)


        $this->set('siswas', $this->Siswa->find('all'));

        $rataBIndo = $this->Siswa->rata2Mapel('nilai_b_indonesia');
        if (!empty($_GET['export']) && $_GET['export'] == 'excel') {
//            $this->render('admin_index_excel');
            $this->PhpExcel->createWorksheet()->setDefaultFont('Calibri', 12);
            $options = array();
            if (!empty($this->params->query['jurusan']) && $this->params->query['jurusan'] != 0) {
                $options['conditions'] = array(
                    'Siswa.pilihan_satu' => $this->params->query['jurusan']
                );
            }

            $options['fields'] = array('Siswa.*,((4*Siswa.nilai_matematika)+(2*Siswa.nilai_b_inggris)+(3*Siswa.nilai_ipa) + (1*Siswa.nilai_b_indonesia)) as bobot');
            $options['order'] = array('bobot' => 'DESC');
            $siswas = $this->Siswa->find('all', $options);

// define table cells
            $table = array(
                array('label' => __('No')),
                array('label' => __('No Pendaftaran')),
                array('label' => __('Nama')),
                array('label' => __('Asal Sekolah')),
                array('label' => __('Tempat Lahir')),
                array('label' => __('Tanggal Lahir')),
                array('label' => __('Kelamin')),
                 array('label' => __('Agama')),
                array('label' => __('Alamat')),
                array('label' => __('Nilai B. Indonesia')),
                array('label' => __('Nilai B. Inggris')),
                array('label' => __('Nilai Matematika')),
                array('label' => __('Nilai IPA')),
                array('label' => __('Bobot')),
                array('label' => __('Pilihan 1')),
                array('label' => __('Pilihan 2')),
                array('label' => __('Status')),
                array('label' => __('Tanggal Daftar'))
            );

// add heading with different font and bold text
            $this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true));

            if (!empty($siswas)) {
                $no = 1;
                foreach ($siswas as $siswa) {
                    $bobot = 0;
                    $bobot = (4 * $siswa['Siswa']['nilai_matematika']) + (2 * $siswa['Siswa']['nilai_b_inggris']) + (3 * $siswa['Siswa']['nilai_ipa']) + (1 * $siswa['Siswa']['nilai_b_indonesia']);


                    $this->PhpExcel->addTableRow(array(
                        $no,
                        $siswa['Siswa']['no_pendaftaran'],
                        $siswa['Siswa']['nama'],
                        $this->sekolahs[$siswa['Siswa']['sekolah_id']],
                        $siswa['Siswa']['tempat_lahir'],
                        $siswa['Siswa']['tgl_lahir'],
                        $this->kelamins[$siswa['Siswa']['kelamin']],
                        $siswa['Siswa']['agama'],
                        $siswa['Siswa']['alamat'],
                        $siswa['Siswa']['nilai_b_indonesia'],
                        $siswa['Siswa']['nilai_b_inggris'],
                        $siswa['Siswa']['nilai_matematika'],
                        $siswa['Siswa']['nilai_ipa'],
                        $bobot,
                        $this->jurusans[$siswa['Siswa']['pilihan_satu']],
                        $this->jurusans[$siswa['Siswa']['pilihan_dua']],
                        $this->Siswa->status[$siswa['Siswa']['status']],
                        $siswa['Siswa']['created']
                    ));

                    $no++;
                }
            }
// close table and output
            $rekap = 'semua_jurusan';
            if (!empty($this->params->query['jurusan']) && $this->params->query['jurusan'] != 0) {
                $rekap = strtolower(str_replace(' ', '_', $this->jurusans[$this->params->query['jurusan']]));
            }
            $this->PhpExcel->addTableFooter()->output("rekap_pendaftar_" . $rekap . '.xlsx', "Excel2007");
        }
    }

    function admin_add() {
        if (!empty($this->data)) {
            $this->Siswa->create();
            if ($this->Siswa->save($this->data)) {
                $this->Session->setFlash(__('The event has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.', true), 'error');
            }
        }
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid event', true), 'error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Siswa->save($this->data)) {
                if (!empty($this->request->data['Siswa']['image'])) {
                    if ($this->request->data['Siswa']['image']['error'] != 4) {
                        //Delete exist image
                        $conditions = array(
                            'Image.type' => $this->Siswa->alias,
                            'Image.key' => $this->Siswa->id
                        );
                        $images = ClassRegistry::init('Image')->find('all', array('conditions' => $conditions));
                        if (!empty($images)) {
                            ClassRegistry::init('Image')->deleteAll($conditions);
                        }
                        //------------------------
                        $image['Image']['type'] = $this->Siswa->alias;
                        $image['Image']['key'] = $this->Siswa->id;
                        $image['Image']['description'] = $this->request->data['Siswa']['title'];
                        $image['Image']['image'] = $this->request->data['Siswa']['image'];
                        $image['Image']['mime'] = $this->request->data['Siswa']['image']['type'];

                        ClassRegistry::init('Image')->save($image);
                    }
                }
                $this->Session->setFlash(__('The event has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.', true), 'error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Siswa->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for event', true), 'error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Siswa->delete($id)) {
            $this->Session->setFlash(__('Siswa deleted', true), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Siswa was not deleted', true), 'error');
        $this->redirect(array('action' => 'index'));
    }

    // The feed action is called from "webroot/js/ready.js" to get the list of siswas (JSON)
    function feed($id = null) {
        $this->layout = "ajax";
        $vars = $this->params['url'];
        $conditions = array('conditions' => array('UNIX_TIMESTAMP(start) >=' => $vars['start'], 'UNIX_TIMESTAMP(start) <=' => $vars['end']));
        $siswas = $this->Siswa->find('all', $conditions);
        foreach ($siswas as $event) {
            if ($event['Siswa']['all_day'] == 1) {
                $allday = true;
                $end = $event['Siswa']['start'];
            } else {
                $allday = false;
                $end = $event['Siswa']['end'];
            }
            $data[] = array(
                'id' => $event['Siswa']['id'],
                'title' => $event['Siswa']['title'],
                'start' => $event['Siswa']['start'],
                'end' => $end,
                'allDay' => $allday,
                'url' => Router::url('/') . 'full_calendar/siswas/view/' . $event['Siswa']['id'],
                'details' => $event['Siswa']['details'],
                'className' => $event['SiswaType']['color']
            );
        }
        $this->set("json", json_encode($data));
    }

    // The update action is called from "webroot/js/ready.js" to update date/time when an event is dragged or resized
    function update() {
        $vars = $this->params['url'];
        $this->Siswa->id = $vars['id'];
        $this->Siswa->saveField('start', $vars['start']);
        $this->Siswa->saveField('end', $vars['end']);
        $this->Siswa->saveField('all_day', $vars['allday']);
    }

}

?>
