<?php

App::uses('AppController', 'Controller');

/**
 * Slides Controller
 *
 * @property Slide $Slide
 */
class SlidesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->set('status', $this->Slide->status);
        $this->AclFilter->protection();
        $this->activeMenu = 'gallery';
        $this->set('activeMenu', $this->activeMenu);
    }

    public function beforeRender() {
        parent::beforeRender();

//        $this->set('breadcrumb', $this->Breadcrumb->setBreadcrumb($this->params->url));
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $this->paginate = array('order' => array('Slide.position' => 'ASC'));
        $this->set('slides', $this->paginate());
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Slide->id = $id;
        if (!$this->Slide->exists()) {
            throw new NotFoundException(__('Invalid slide'), 'error');
        }
        $this->set('slide', $this->Slide->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Slide->create();
            if ($this->Slide->save($this->request->data)) {
                $conditions = array(
                    'Slide.id' => $this->Slide->id
                );

                $fields = array(
                    'Slide.position' => $this->Slide->id
                );

                $this->Slide->updateAll($fields, $conditions);
                if ($this->request->data['Slide']['image']['error'] != 4) {
                    $image['Image']['type'] = $this->Slide->alias;
                    $image['Image']['key'] = $this->Slide->id;
                    $image['Image']['description'] = $this->request->data['Slide']['title'];
                    $image['Image']['image'] = $this->request->data['Slide']['image'];
                    $image['Image']['mime'] = $this->request->data['Slide']['image']['type'];
                    ClassRegistry::init('Image')->save($image);
                }
                $this->Session->setFlash(__('The slide has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The slide could not be saved. Please, try again.'), 'error');
            }
        }
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Slide->id = $id;
        if (!$this->Slide->exists()) {
            throw new NotFoundException(__('Invalid slide'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {

            if ($this->Slide->save($this->request->data)) {
                if ($this->request->data['Slide']['image']['error'] != 4) {
                    //Delete exist image
                    $conditions = array(
                        'Image.type' => $this->Slide->alias,
                        'Image.key' => $this->Slide->id
                    );
                    $images = ClassRegistry::init('Image')->find('all', array('conditions' => $conditions));
                    if (!empty($images)) {
                        ClassRegistry::init('Image')->deleteAll($conditions);
                    }
                    //------------------------
                    $image['Image']['type'] = $this->Slide->alias;
                    $image['Image']['key'] = $this->Slide->id;
                    $image['Image']['description'] = $this->request->data['Slide']['title'];
                    $image['Image']['image'] = $this->request->data['Slide']['image'];
                    $image['Image']['mime'] = $this->request->data['Slide']['image']['type'];

                    ClassRegistry::init('Image')->save($image);
                }
                $this->Session->setFlash(__('The slide has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The slide could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Slide->read(null, $id);
        }
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Slide->id = $id;
        if (!$this->Slide->exists()) {
            throw new NotFoundException(__('Invalid slide'), 'error');
        }
        if ($this->Slide->delete()) {
            $this->Session->setFlash(__('Slide deleted'), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Slide was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_up($position = null) {
        if ($position == null) {
            $this->Session->setFlash(__('Invalid Slide'), 'error');
            $this->redirect(array('action' => 'index'));
        } else {
            $currentSlide = $this->Slide->findByPosition($position);

            $prevSlide = $this->Slide->getPrevSlide($position);

            if (!empty($prevSlide)) {
                $currentSlideId = $currentSlide['Slide']['id'];
                $currentSlideNewPosition = $prevSlide['Slide']['position'];

                $this->Slide->setPosition($currentSlideId, $currentSlideNewPosition);

                $prevSlideId = $prevSlide['Slide']['id'];
                $prevSlideNewPosition = $currentSlide['Slide']['position'];
                $this->Slide->setPosition($prevSlideId, $prevSlideNewPosition);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Prev Slide not Exist'), 'error');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    function admin_down($position = null) {
        if ($position == null) {
            $this->Session->setFlash(__('Invalid Slide'), 'error');
            $this->redirect(array('action' => 'index'));
        } else {
            $currentSlide = $this->Slide->findByPosition($position);

            $nextSlide = $this->Slide->getNextSlide($position);

            if (!empty($nextSlide)) {
                $currentSlideId = $currentSlide['Slide']['id'];
                $currentSlideNewPosition = $nextSlide['Slide']['position'];
                $this->Slide->setPosition($currentSlideId, $currentSlideNewPosition);

                $nextSlideId = $nextSlide['Slide']['id'];
                $nextSlideNewPosition = $currentSlide['Slide']['position'];
                $this->Slide->setPosition($nextSlideId, $nextSlideNewPosition);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Next Slide not Exist'), 'success');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

}
