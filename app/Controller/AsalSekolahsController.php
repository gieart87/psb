<?php

App::uses('AppController', 'Controller');

/**
 * Categories Controller
 *
 * @property Category $Category
 */
class AsalSekolahsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->set('status', $this->AsalSekolah->status);
        $this->AclFilter->protection();
        $this->activeMenu = 'article';
        $this->set('activeMenu', $this->activeMenu);
    }

    public function beforeRender() {
        parent::beforeRender();
//        $this->set('breadcrumb', $this->Breadcrumb->setBreadcrumb($this->params->url));
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $this->AsalSekolah->recursive = 0;
        $this->set('sekolahs', $this->paginate());
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->AsalSekolah->id = $id;
        if (!$this->AsalSekolah->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->set('sekolah', $this->AsalSekolah->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->AsalSekolah->create();
//            $this->request->data['sekolah']['permalink'] = $this->General->permalink($this->request->data['sekolah']['name']);
//            $this->request->data['sekolah']['permalink_en'] = $this->General->permalink($this->request->data['sekolah']['name_en']);

            if ($this->AsalSekolah->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'error');
            }
        }

        $this->set(compact('types'));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->AsalSekolah->id = $id;
        if (!$this->AsalSekolah->exists()) {
            throw new NotFoundException(__('Invalid category'), 'error');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
//            $this->request->data['sekolah']['permalink'] = $this->General->permalink($this->request->data['sekolah']['name']);
            if ($this->AsalSekolah->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->AsalSekolah->read(null, $id);
        }

        $this->set(compact('types'));
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->AsalSekolah->id = $id;
        if (!$this->AsalSekolah->exists()) {
            throw new NotFoundException(__('Invalid category'), 'error');
        }
        $articlesInCategory = ClassRegistry::init('Article')->find('all', array('conditions' => array('Article.category_id' => $id)));
        if (empty($articlesInCategory)) {
            if ($this->AsalSekolah->delete()) {
                $this->Session->setFlash(__('Category deleted'), 'success');
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->Session->setFlash(__('Could not delete this category cause contain articles'), 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Category was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

}
