<?php

class TreeComponent extends Component {

    var $tab = "  ";

    function show($name, $data) {
        list($modelName, $fieldName) = explode('/', $name);
        $output = $this->list_element($data, $modelName, $fieldName, 0);

        return $this->output($output);
    }

    function indentTree($array, $counter = 0) {
        $ret = '';
        $array2 = array();
        $pre = '';
        for ($i = 1; $i < $counter; $i++) {
            $pre .= '--';
        }

        foreach ($array as $key => $value) {
            if ($key == 'children') {
                if (isset($value['Category']) || (isset($value['children']) && sizeof($value['children']) > 0)) {
                    $indented[] = $this->indentTree($value, ++$counter);
                } else {
                    if (sizeof($value) > 0) {
                        $indented[] = $this->indentTree($value, $counter);
                    }
                }
            } elseif ($key == 'Category') {
                $indented[$value['id']] = ' ' . $pre . ' ' . $value['name'];
            } elseif (isset($value['Category']['name'])) {
                $indented[] = $this->indentTree($value, $counter);
            }
        }
        return $this->flatten_array($indented, 2);
    }

    function flatten_array($array, $preserve_keys = 0, &$out = array()) {
        foreach ($array as $key => $child) {
            if (is_array($child)) {
                $out = $this->flatten_array($child, $preserve_keys, $out);
            } elseif ($preserve_keys + is_string($key) > 1) {
                $out[$key] = $child;
            } else {
                $out[] = $child;
            }
        }
        return $out;
    }

    function list_element($data, $modelName, $fieldName, $level) {
        $baseUrl = Router::url('/');
        $tabs = "\n" . str_repeat($this->tab, $level * 2);
        $li_tabs = $tabs . $this->tab;

        $output = $tabs . "<ul>";
        foreach ($data as $key => $val) {

            $output .= $li_tabs . "<li><a href='" . $baseUrl . $val[$modelName]['url'] . "'><span>" . $val[$modelName][$fieldName] . '</span></a>';
            if (isset($val['children'][0])) {
                $output .= $this->list_element($val['children'], $modelName, $fieldName, $level + 1);
                $output .= $li_tabs . "</li>";
            } else {
                $output .= "</li>";
            }
        }
        $output .= $tabs . "</ul>";


        return $output;
    }

    function getCategory() {
        return ClassRegistry::init('Category')->getCategory();
    }

}

?> 