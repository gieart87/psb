<?php

App::uses('GeneralComponent', 'Controller/Component');
App::import('Component', 'Email');

class CakeMailerComponent extends EmailComponent {

    var $name = 'CakeMailer';
    var $smtp;
    var $controller;
    public $components = array('General');


    #variable setting standar
    var $fromNameSetting;
    var $fromEmailSetting;

    function startup(Controller $controller) {
        $this->controller = $controller;
    }

    public function __construct(ComponentCollection $collection, $settings = array()) {
        parent::__construct($collection, $settings);

        $this->smtp = array(
            'port' => $this->General->getSetting('Email.Smtp.Port'),
            'timeout' => $this->General->getSetting('Email.Smtp.Timeout'),
            'host' => $this->General->getSetting('Email.Smtp.Host'),
            'username' => $this->General->getSetting('Email.Smtp.Username'),
            'password' => $this->General->getSetting('Email.Smtp.Password'),
        );
        $this->sendAs = 'html';
        $this->fromNameSetting = $this->General->getSetting('Email.From.Name');
        $this->fromEmailSetting = $this->General->getSetting('Email.From.Email');
    }

    function notification($data) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $data['User']['username'] . '<' . $data['User']['email'] . '>';
        $this->subject = __('Notification Member', true);
        $this->template = 'notification';
        $this->layout = 'blank';
        $this->controller->set('user', $data);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function send_invoice($invoice) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $invoice['Invoice']['username'] . '<' . $invoice['Invoice']['email'] . '>';
        $this->subject = __('Invoise', true);
        $this->template = 'invoice';
        $this->layout = 'blank';
        $this->controller->set('invoice', $invoice);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function confirmation($confirm) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $confirm['confirm']['username'] . '<' . $confirm['confirm']['email'] . '>';
        $this->subject = __('Confirmasi', true);
        $this->template = 'confirmation';
        $this->layout = 'blank';
        $this->controller->set('confirm', $confirm);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function resetPassword($resetPassword) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $resetPassword['username'] . '<' . $resetPassword['username'] . '>';
        $this->subject = __('Reset Password', true);
        $this->template = 'reset_password';
        $this->layout = 'blank';
        $this->controller->set('resetPassword', $resetPassword);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function newPassword($newPassword) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $newPassword['username'] . '<' . $newPassword['username'] . '>';
        $this->subject = __('New Password', true);
        $this->template = 'new_password';
        $this->layout = 'blank';
        $this->controller->set('newPassword', $newPassword);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function activate($user, $linkActivate) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Thanks for joining Aksaramaya.com! Please activate your account.', true);
        $this->template = 'activate';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        $this->controller->set('linkActivate', $linkActivate);
//        var_dump($this->fromEmailSetting);exit;
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function deactivate($user, $linkDeactivate) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Welcome to MOCO', true);
        $this->template = 'deactivate';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        $this->controller->set('linkDeactivate', $linkDeactivate);
//        var_dump($this->fromEmailSetting);exit;
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function synchronizeStudentMoco($user, $linkSync) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['UserMoco']['username'] . '<' . $user['UserMoco']['username'] . '>';
        $this->subject = __('Konfirmasi Sinkronisasi akun Epustaka [' . $user['Library']['name'] . ']', true);

        $this->template = 'synchronize_student_moco';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        $this->controller->set('linkSync', $linkSync);
//        var_dump($this->fromEmailSetting);exit;
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function pustakamayaActivate($user, $linkActivate) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Thanks for joining pustakamaya.aksaramaya.com! Please activate your account.', true);
        $this->template = 'pustakamaya_activate';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        $this->controller->set('linkActivate', $linkActivate);
//        var_dump($this->fromEmailSetting);exit;
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function welcome($user) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Welcome to Aksaramaya.com', true);
        $this->template = 'welcome';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * kirim email ke author
     * 
     * @param Object $user
     * @param Object $author
     * 
     * @return boolean
     */
    public function sendAuthorMail($user, $author, $password = '') {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Welcome to Aksaramaya.com', true);
        $this->template = 'send_author_mail';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        $this->controller->set('author', $author);
        $this->controller->set('password', $password);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * kirim email ke author
     * 
     * @param Object $user
     * @param Object $author
     * 
     * @return boolean
     */
    public function sendAuthorMailEdit($user, $author, $password = '') {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Welcome to Aksaramaya.com', true);
        $this->template = 'send_author_mail_edit';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        $this->controller->set('author', $author);
        $this->controller->set('password', $password);

        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function pustakamayaWelcome($user) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Welcome to pustaka.aksaramaya.com', true);
        $this->template = 'pustakamaya_welcome';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function sendInvoice($order) {
        if (empty($order)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $order['User']['username'] . '<' . $order['User']['username'] . '>';
            $this->subject = __('Invoice Belanja di Aksaramaya', true);
            $this->template = 'send_invoice';
            $this->layout = 'blank';
            $this->controller->set('order', $order);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function invoiceDeposite($order) {
        if (empty($order)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $order['User']['username'] . '<' . $order['User']['username'] . '>';
            $this->subject = __('Invoice Belanja di Aksaramaya (via Pembayaran Deposit)', true);
            $this->template = 'invoice_deposite';
            $this->layout = 'blank';
            $this->controller->set('order', $order);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function confirmationOrder($confirmation) {
        if (empty($confirmation)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $this->General->getSetting('Email.Admin');

            $emailAdminCc = $this->General->getSetting('Email.Admin.cc');
            $emailArray = explode(',', $emailAdminCc);

            $this->cc = $emailArray;
            $this->cc = array('agie0925@gmail.com');
            $this->subject = __('Konfirmasi Pembayaran dari Pelanggan', true);
            $this->template = 'confirmation_order';
            $this->layout = 'blank';
            $this->controller->set('confirmation', $confirmation);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function sendFeedback($feedback) {
        if (empty($feedback)) {
            return false;
        } else {
//            $this->smtpOptions = $this->smtp;
//            $this->delivery = 'smtp';
            $this->from = $feedback['User']['username'] . '<' . $feedback['User']['username'] . '>';
            $this->to = $this->General->getSetting('Email.Feedback');

            $emailAdminCc = $this->General->getSetting('Email.Admin.cc');
            $emailArray = explode(',', $emailAdminCc);

            $this->cc = $emailArray;
            $this->subject = __('Feedback from Moco User <' . isset($feedback['User']['name']) ? $feedback['User']['name'] : $feedback['User']['username'] . '>', true);
            $this->template = 'send_feedback';
            $this->layout = 'blank';
            $this->controller->set('feedback', $feedback);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function notifNewCorporate($client) {
        if (empty($client)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $this->General->getSetting('Email.Admin');

            $emailAdminCc = $this->General->getSetting('Email.Admin.cc');
            $emailArray = explode(',', $emailAdminCc);

            $this->cc = $emailArray;
            $this->cc = array('agie0925@gmail.com');
            $this->subject = __('Pelanggan Corporate Baru', true);
            $this->template = 'notif_new_corporate';
            $this->layout = 'blank';
            $this->controller->set('client', $client);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function newCorporateOrder($corporateOrder, $client) {

        if (empty($corporateOrder)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $this->General->getSetting('Email.Admin');

            $emailAdminCc = $this->General->getSetting('Email.Admin.cc');
            $emailArray = explode(',', $emailAdminCc);

            $this->cc = $emailArray;
            $this->cc = array('agie0925@gmail.com');
            $this->subject = __('Order dari Klien Corporate', true);
            $this->template = 'new_corporate_order';
            $this->layout = 'blank';
            $this->controller->set('corporateOrder', $corporateOrder);
            $this->controller->set('client', $client);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function approveConfirmation($order, $confirmation) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $order['User']['username'] . '<' . $order['User']['username'] . '>';
        $this->subject = __('Konfirmasi Telah Diterima', true);
        $this->template = 'approve_confirmation';
        $this->layout = 'blank';
        $this->controller->set('order', $order);
        $this->controller->set('confirmation', $confirmation);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function approveOrder($order) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $order['User']['username'] . '<' . $order['User']['username'] . '>';
        $this->subject = __('Order Telah di Approve', true);
        $this->template = 'approve_order';
        $this->layout = 'blank';
        $this->controller->set('order', $order);

        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function suggestionNotification($suggestion) {
        if (empty($suggestion)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $suggestion['User']['username'];
            $this->subject = __('Pemberitahuan Usulan Buku', true);
            $this->template = 'suggestion_notification';
            $this->layout = 'blank';
            $this->controller->set('suggestion', $suggestion);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function giftNotification($gift, $fromUser, $toUser) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $toUser['User']['username'];
        $this->subject = __('Hadiah dari Temen Anda di Aksaramaya', true);
        $this->template = 'gift_notification';
        $this->layout = 'blank';
        $this->controller->set('gift', $gift);
        $this->controller->set('fromUser', $fromUser);
        $this->controller->set('toUser', $toUser);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function newsletter($user, $newsletters) {
        if (empty($newsletters)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
            $this->subject = __('Informasi terbitan terbaru di Aksaramaya', true);
            $this->template = 'newsletter';
            $this->layout = 'blank';
            $this->controller->set('newsletters', $newsletters);
            $this->controller->set('user', $user);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function biddingNotification($book, $publisher, $author, $biddingTypes) {
        if (empty($book)) {
            return false;
        } else {
            $this->smtpOptions = $this->smtp;
            $this->delivery = 'smtp';
            $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
            $this->to = $publisher['Publisher']['name'] . '<' . $publisher['User']['username'] . '>';
            //$this->to = 'agie0925@gmail.com';
            $this->subject = __('Informasi Penawaran (Lelang) Buku baru di Aksaramaya', true);
            $this->template = 'bidding_notification';
            $this->layout = 'blank';
            $this->controller->set('book', $book);
            $this->controller->set('publisher', $publisher);
            $this->controller->set('author', $author);
            $this->controller->set('biddingTypes', $biddingTypes);
            if ($this->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function biddingReminder($book, $publisher, $author, $biddingTypes, $range) {

        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $publisher['Publisher']['name'] . '<' . $publisher['User']['username'] . '>';
        $this->subject = __('[Pengingat]Informasi Penawaran (Lelang) Buku di Aksaramaya', true);
        $this->template = 'bidding_reminder';
        $this->layout = 'blank';
        $this->controller->set('book', $book);
        $this->controller->set('publisher', $publisher);
        $this->controller->set('author', $author);
        $this->controller->set('biddingTypes', $biddingTypes);
        $this->controller->set('range', $range);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function newbidNotification($book, $publisher, $author, $biddingTypes, $newbid) {

        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $publisher['Publisher']['name'] . '<' . $publisher['User']['username'] . '>';
        $this->subject = __('[Pengingat]Informasi Penawaran (Lelang) Buku di Aksaramaya', true);
        $this->template = 'newbid_notification';
        $this->layout = 'blank';
        $this->controller->set('book', $book);
        $this->controller->set('publisher', $publisher);
        $this->controller->set('author', $author);
        $this->controller->set('biddingTypes', $biddingTypes);
        $this->controller->set('newbid', $newbid);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function bidWinnerNotification($book, $publisher, $author, $biddingTypes, $finalBid) {

        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $publisher['Publisher']['name'] . '<' . $publisher['User']['username'] . '>';
        $this->subject = __('Pemberitahuan Pemenang Lelang Buku di Aksaramaya', true);
        $this->template = 'bid_winner_notification';
        $this->layout = 'blank';
        $this->controller->set('book', $book);
        $this->controller->set('publisher', $publisher);
        $this->controller->set('author', $author);
        $this->controller->set('biddingTypes', $biddingTypes);
        $this->controller->set('finalBid', $finalBid);

        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function bidAuthorNotification($book, $publisher, $author, $biddingTypes, $finalBid) {

        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $author['Author']['name'] . '<' . $author['User']['username'] . '>';
        $this->subject = __('Pemberitahuan Pemenang Lelang Buku di Aksaramaya', true);
        $this->template = 'bid_author_notification';
        $this->layout = 'blank';
        $this->controller->set('book', $book);
        $this->controller->set('publisher', $publisher);
        $this->controller->set('author', $author);
        $this->controller->set('biddingTypes', $biddingTypes);
        $this->controller->set('finalBid', $finalBid);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function biddingClosedNotification($book, $publisher, $author, $biddingTypes, $finalBid) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $publisher['Publisher']['name'] . '<' . $publisher['User']['username'] . '>';
        $this->subject = __('Pemberitahuan Pemenang Lelang Buku di Aksaramaya', true);
        $this->template = 'bidding_closed_notification';
        $this->layout = 'blank';
        $this->controller->set('book', $book);
        $this->controller->set('publisher', $publisher);
        $this->controller->set('author', $author);
        $this->controller->set('biddingTypes', $biddingTypes);
        $this->controller->set('finalBid', $finalBid);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function corporateApproval($client) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $client['User']['username'] . '<' . $client['User']['username'] . '>';
        $this->subject = __('Persetujuan Akun Corporate', true);
        $this->template = 'corporate_approval';
        $this->layout = 'blank';
        $this->controller->set('client', $client);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function corporateRejection($client) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $client['User']['username'] . '<' . $client['User']['username'] . '>';
        $this->subject = __('Penolakan Akun Corporate', true);
        $this->template = 'corporate_rejection';
        $this->layout = 'blank';
        $this->controller->set('client', $client);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function invoiceCorporate($corporateOrder, $client) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $client['User']['username'] . '<' . $client['User']['username'] . '>';
        $this->subject = __('Invoice Order Member Corporate', true);
        $this->template = 'invoice_corporate';
        $this->layout = 'blank';
        $this->controller->set('client', $client);
        $this->controller->set('corporateOrder', $corporateOrder);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function corporateOrderApproval($corporateOrder, $client) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $client['User']['username'] . '<' . $client['User']['username'] . '>';
        $this->subject = __('Konfirmas Order', true);
        $this->template = 'corporate_order_approval';
        $this->layout = 'blank';
        $this->controller->set('client', $client);
        $this->controller->set('corporateOrder', $corporateOrder);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function corporateOrderRejection($corporateOrder, $client) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $client['User']['username'] . '<' . $client['User']['username'] . '>';
        $this->subject = __('Konfirmasi Order', true);
        $this->template = 'corporate_order_rejection';
        $this->layout = 'blank';
        $this->controller->set('client', $client);
        $this->controller->set('corporateOrder', $corporateOrder);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function newStudentAlert($user) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $user['User']['username'] . '<' . $user['User']['username'] . '>';
        $this->subject = __('Selamat, Anda berhasil mendaftar sebagai anggota ' . $user['Client']['name'], true);
        $this->template = 'new_student_alert';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function epustakaNotifAdminNewStudent($user, $librarianEmail) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $librarianEmail;
        $this->subject = __('Permintaan Approval member Epustaka', true);
        $this->template = 'epustaka_notif_admin_new_student';
        $this->layout = 'blank';
        $this->controller->set('user', $user);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

    function studentApproveAlert($student) {
        $this->smtpOptions = $this->smtp;
        $this->delivery = 'smtp';
        $this->from = $this->fromNameSetting . '<' . $this->fromEmailSetting . '>';
        $this->to = $student['User']['username'] . '<' . $student['User']['username'] . '>';
        $this->subject = __('Welcome to ' . $student['Client']['name'], true);
        $this->template = 'student_approve_alert';
        $this->layout = 'blank';
        $this->controller->set('student', $student);
        if ($this->send()) {
            return true;
        } else {
            return false;
        }
    }

}

?>
