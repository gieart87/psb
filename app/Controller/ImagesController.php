<?php

App::uses('AppController', 'Controller');

/**
 * Images Controller
 *
 * @property Image $Image
 */
class ImagesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->AclFilter->protection();
        $this->activeMenu = 'gallery';
        $this->set('activeMenu', $this->activeMenu);
    }

    public function beforeRender() {
        parent::beforeRender();
        $this->set('breadcrumb', $this->Breadcrumb->setBreadcrumb($this->params->url));
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $images = ClassRegistry::init('Image')->find('all', array('group' => 'Image.id', 'order' => array('Image.created' => 'DESC')));
        $this->set('images', $images);
    }

    /**

      /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Image->create();

            if ($this->Image->save($this->request->data)) {
                $fields = array(
                    'Image.type' => "'" . $this->Image->alias . "'",
                    'Image.key' => $this->Image->id
                );

                $conditions = array(
                    'Image.id' => $this->Image->id
                );
                $this->Image->updateAll($fields, $conditions);
                $this->Session->setFlash(__('The image has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The image could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('photos'));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Image->id = $id;
        if (!$this->Image->exists()) {
            throw new NotFoundException(__('Invalid image'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Image->save($this->request->data)) {
                $this->Session->setFlash(__('The image has been saved'));
                $this->redirect(array('action' => 'index'));
            } else {
                $this->request->data = $this->Image->read(null, $id);
                $this->Session->setFlash(__('The image could not be saved. Please, try again.'));
            }
        } else {
            $this->request->data = $this->Image->read(null, $id);
        }

        $this->set(compact('photos'));
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null, $model_id, $model, $action) {
        $controller = strtolower(Inflector::pluralize($model));
        $this->Image->id = $id;
        if (!$this->Image->exists()) {
            throw new NotFoundException(__('Invalid image'), 'error');
        }
        if ($this->Image->delete()) {
            $this->Session->setFlash(__('Image deleted'), 'success');
            $this->redirect(array('controller' => $controller, 'action' => $action, $model_id));
        }
        $this->Session->setFlash(__('Image was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

}
