<?php

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * settings Controller
 *
 * @property Setting $Setting
 */
class SettingsController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow(array('initdata', 'setprice'));
        $this->AclFilter->protection();
    }

    public function encrypt() {
        $originText = "

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `storedb_production`
--

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `type`, `key`, `value`, `parent_id`, `description`, `created`) VALUES
(1, 'Email', 'Email.Smtp.Port', '25', 1, '<p>Email.Smtp.Port</p>', '2011-10-26 16:47:02'),
(2, 'Email', 'Email.Smtp.Timeout', '60', 1, 'Email.Smtp.Timeout', '2011-10-26 16:47:59'),
(3, 'Email', 'Email.Smtp.Host', 'localhost', 1, '<p>Email.Smtp.Host</p>', '2011-10-26 17:04:04'),
(4, 'Email', 'Email.Smtp.Username', '', 1, '<p>Email.Smtp.Username</p>', '2011-10-26 17:04:56'),
(5, 'Email', 'Email.Smtp.Password', '', 1, '<p>Email.Smtp.Password</p>', '2011-10-26 17:06:11'),
(6, 'Email', 'Email.From.Name', 'Aksaramaya', 1, '<p>Email.From.Name</p>', '2011-10-27 09:35:54'),
(7, 'Email', 'Email.From.Email', 'noreply@aksaramaya.com', 1, '<p>noreply</p>', '2011-10-27 09:36:41'),
(8, 'S3', 'S3.accessKey', 'AKIAIHU2AJNZKFTT4WXQ', 0, '', '0000-00-00 00:00:00'),
(9, 'S3', 'S3.secretKey', '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', 0, '', '0000-00-00 00:00:00'),
(10, 'S3', 'S3.bucket', 'bukutablet', 0, '', '0000-00-00 00:00:00'),
(11, 'Batas Waktu pembayaran', 'Order.DueDate', '7', 0, '', '0000-00-00 00:00:00'),
(12, 'Lama Peminjaman Buku', 'BorrowCollection.DueDate', '7', 0, '', '0000-00-00 00:00:00'),
(13, 'Email Admin Bukutablet', 'Email.Admin', 'ardian2007@gmail.com', 0, 'Email Admin Aksaramaya', '0000-00-00 00:00:00'),
(14, 'Persen Rabat', 'Rabat.Percent', '70', 0, '<p>Persen rabat Aksaramaya</p>', '2012-03-08 18:50:22'),
(15, 'Bidding', 'Bidding.length', '2', 0, '<p>Ini adalah setting lama / masa bidding</p>', '2012-03-31 12:12:12'),
(16, 'Email', 'Email.Admin.cc', 'nia@aksaramaya.com,fradika@aksaramaya.com,irfan@aksaramaya.com', 0, '&amp;lt;p&amp;gt;Email Admin Tambahan&amp;lt;/p&amp;gt;', '2012-04-09 10:51:28'),
(17, 'Main', 'Main.SiteUrl', 'http://store.aksaramaya.com/', 0, 'Main Store', '0000-00-00 00:00:00'),
(18, 'Main', 'Main.logo', 'http://store.aksaramaya.com/img/public/logo_mini.png', 0, '', '0000-00-00 00:00:00'),
(19, 'Main', 'Main.SiteName', 'Aksaramaya eBook Store', 0, '', '0000-00-00 00:00:00'),
(20, 'Payment', 'Payment.minimumKlikPay', '10000', 0, 'Payment.minimumKlikPay ', '2013-11-13 10:54:52');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
";
        $encriptedText = Security::cipher($originText, 'test');
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $settings = $this->Setting->find('all');
        $this->set('settings', $settings);
    }

    /**
     * view method
     *
     * @param string $id
     * @return void

      /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Setting->create();
            if ($this->Setting->save($this->request->data)) {
                $this->Session->setFlash(__('The Setting has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Setting could not be saved. Please, try again.'), 'error');
            }
        }
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Setting->id = $id;
        if (!$this->Setting->exists()) {
            throw new NotFoundException(__('Invalid Setting'), 'error');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->Setting->save($this->request->data)) {
                $this->Session->setFlash(__('The Setting has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The Setting could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Setting->read(null, $id);
        }
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Setting->id = $id;
        if (!$this->Setting->exists()) {
            throw new NotFoundException(__('Invalid Setting'), 'error');
        }
        if ($this->Setting->delete()) {
            $this->Session->setFlash(__('Setting deleted'), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Setting was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

    public function admin_statistic() {
        $users = ClassRegistry::init('User')->find('all');

        $admins = ClassRegistry::init('Admin')->find('all');
        $customers = ClassRegistry::init('Customer')->find('all');
        $publishers = ClassRegistry::init('Publisher')->find('all');
        $authors = ClassRegistry::init('Author')->find('all');
        $students = ClassRegistry::init('Student')->find('all');
        $paymentPoints = ClassRegistry::init('PaymentPoint')->find('all');

        $universities = ClassRegistry::init('Universities')->find('all');
        $libraries = ClassRegistry::init('Library')->find('all');

        $books = ClassRegistry::init('Book')->find('all', array('group' => 'Book.id'));
        $dissertations = ClassRegistry::init('Dissertation')->find('all', array('group' => 'Dissertation.id'));
        $journals = ClassRegistry::init('Journal')->find('all', array('group' => 'Journal.id'));
        $magazines = ClassRegistry::init('Magazine')->find('all', array('group' => 'Magazine.id'));
        $newspapers = ClassRegistry::init('Newspaper')->find('all', array('group' => 'Newspaper.id'));
        $proceedings = ClassRegistry::init('Proceedings')->find('all', array('group' => 'Proceedings.id'));
        $tabloids = ClassRegistry::init('Tabloid')->find('all', array('group' => 'Tabloid.id'));

        $products = ClassRegistry::init('Product')->find('all', array('fields' => array('SUM(Product.sold_items) as totalSells, SUM(Product.hits) as totalHits')));

        $this->set(compact('users', 'admins', 'customers', 'authors', 'students', 'paymentPoints', 'universities', 'libraries'));
        $this->set(compact('books', 'dissertations', 'journals', 'magazines', 'newspapers', 'proceedings', 'tabloids'));
        $this->set(compact('products'));
    }

    public function admin_validateUser() {
        $customers = ClassRegistry::init('Customer')->find('all');
        if (!empty($customers)) {
            foreach ($customers as $customer) {
                if (empty($customer['User']['id'])) {
                    echo $customer['Customer']['name'] . ' deleted';
                    ClassRegistry::init('Customer')->deleteAll(array('Customer.id' => $customer['Customer']['id']));
                }
            }
        }

        $publishers = ClassRegistry::init('Publisher')->find('all');

        foreach ($publishers as $publisher) {
            $hasPublishersUser = ClassRegistry::init('PublishersUser')->find('first', array('conditions' => array('PublishersUser.publisher_id' => $publisher['Publisher']['id'])));
            if (empty($hasPublishersUser)) {
                ClassRegistry::init('Publisher')->deleteAll(array('Publisher.id' => $publisher['Publisher']['id']));
                echo $publisher['Publisher']['name'] . 'terhapus<br/>';
            }
        }

        $publishersUser = ClassRegistry::init('PublishersUser')->find('all');
        foreach ($publishersUser as $pUser) {
            $hasUser = ClassRegistry::init('User')->findById($pUser['PublishersUser']['user_id']);
            if (empty($hasUser)) {
                ClassRegistry::init('Publisher')->deleteAll(array('Publisher.id' => $pUser['PublishersUser']['publisher_id']));

                ClassRegistry::init('PublishersUser')->deleteAll(array('PublishersUser.id' => $pUser['PublishersUser']['id']));
                echo 'Publisher User' . $pUser['PublishersUser']['publisher_id'] . 'terhapus<br/>';
            } else {
                $hasCustomer = ClassRegistry::init('Customer')->findByUserId($pUser['PublishersUser']['user_id']);
                if (empty($hasCustomer)) {
                    ClassRegistry::init('Customer')->create();
                    $customer = array(
                        'name' => $pUser['Publisher']['name'],
                        'phone' => $pUser['Publisher']['phone'],
                        'address' => $pUser['Publisher']['address'],
                        'user_id' => $pUser['PublishersUser']['user_id']
                    );
                    ClassRegistry::init('Customer')->save($customer);
                    echo 'Customer' . $pUser['Publisher']['name'] . 'terbuat<br/>';
                }
            }
        }

        exit;
    }

    public function admin_resetProduct() {
        $conditions = array(
            'OR' => array(
                'Product.type' => NULL,
                'Product.key' => NULL
            )
        );

        $fields = array(
            'Product.*'
        );
        if (ClassRegistry::init('Product')->deleteAll($conditions)) {
            echo 'Product fixed<br/>';
        }
        exit;
    }

    public function __resetStats() {
        $fields = array('Product.*');
        $conditions = array(
            'OR' => array(
                'Product.sold_items !=' => 0,
                'Product.hits !=' => 0
            )
        );
        $products = ClassRegistry::init('Product')->find('all', array('fields' => $fields, 'conditions' => $conditions));

        $producReset = false;
        if (!empty($products)) {
            foreach ($products as $product) {
                $conditions = array(
                    'Product.id' => $product['Product']['id']
                );
                $fields = array(
                    'Product.sold_items' => 0,
                    'Product.hits' => 0
                );
                if (ClassRegistry::init('Product')->updateAll($fields, $conditions)) {
                    $producReset = true;
                } else {
                    $producReset = false;
                }
            }
        }
        if ($producReset == true) {
            echo 'Product reseted<br/>';
        } else {
            echo 'Product already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE statistic_temps')) {
            echo 'StatisticTemp reseted<br/>';
        } else {
            echo 'Failed in reset StatisticTemp<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE statistics')) {
            echo 'Statistic reseted<br/>';
        } else {
            echo 'Failed in reset Statistic<br/>';
        }
        return TRUE;
    }

    public function admin_resetTransaction() {
        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE biddings')) {
            echo 'Bidding reseted<br/>';
        } else {
            echo 'Bidding already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE bidding_results')) {
            echo 'Bidding Result reseted<br/>';
        } else {
            echo 'Bidding Result already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE book_packages')) {
            echo 'Book Package reseted<br/>';
        } else {
            echo 'Book Package already reseted<br/>';
        }


        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE borrow_collections')) {
            echo 'Borrow Collection reseted<br/>';
        } else {
            echo 'Borrow Collection already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE carts')) {
            echo 'Cart reseted<br/>';
        } else {
            echo 'Cart already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE collections')) {
            echo 'Collection reseted<br/>';
        } else {
            echo 'Collection already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE comments')) {
            echo 'Comment reseted<br/>';
        } else {
            echo 'Comment already reseted<br/>';
        }

//        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE configs')) {
//            echo 'config reseted<br/>';
//        } else {
//            echo 'config already reseted<br/>';
//        }


        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE confirmations')) {
            echo 'Confirmation reseted<br/>';
        } else {
            echo 'Confirmation already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE corporate_orders')) {
            echo 'Corporate Order reseted<br/>';
        } else {
            echo 'Corporate Order already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE downloads')) {
            echo 'Download  reseted<br/>';
        } else {
            echo 'Download  already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE feedbacks')) {
            echo 'Feedback reseted<br/>';
        } else {
            echo 'Feedback already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE forgots')) {
            echo 'Forgot reseted<br/>';
        } else {
            echo 'Forgot already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE gifts')) {
            echo 'Gifts reseted<br/>';
        } else {
            echo 'Gifts already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE items')) {
            echo 'Item reseted<br/>';
        } else {
            echo 'Item already reseted<br/>';
        }


        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE item_histories')) {
            echo 'Item History reseted<br/>';
        } else {
            echo 'Item History already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE likes')) {
            echo 'Like reseted<br/>';
        } else {
            echo 'Like already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE logs')) {
            echo 'Log reseted<br/>';
        } else {
            echo 'Log already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE notes')) {
            echo 'Note reseted<br/>';
        } else {
            echo 'Note already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE oauth_access_tokens')) {
            echo 'Access Token reseted<br/>';
        } else {
            echo 'Access Token already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE orders')) {
            echo 'Order reseted<br/>';
        } else {
            echo 'Order already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE order_details')) {
            echo 'OrderDetail reseted<br/>';
        } else {
            echo 'Order already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE payments')) {
            echo 'Payment reseted<br/>';
        } else {
            echo 'Payment already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE ratings')) {
            echo 'Rating reseted<br/>';
        } else {
            echo 'Rating already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE rating_logs')) {
            echo 'Rating Log reseted<br/>';
        } else {
            echo 'Rating Log already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE reviews')) {
            echo 'review reseted<br/>';
        } else {
            echo 'review already reseted<br/>';
        }

        $this->__resetStats();

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE student_codes')) {
            echo 'Student Code reseted<br/>';
        } else {
            echo 'Student Code already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE subscribes')) {
            echo 'Subscribe reseted<br/>';
        } else {
            echo 'Subscribe already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE suggestions')) {
            echo 'Suggestion reseted<br/>';
        } else {
            echo 'Suggestion already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE synchronize_tokens')) {
            echo 'synchronize_tokens reseted<br/>';
        } else {
            echo 'synchronize_tokens already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE users_categories')) {
            echo 'users categories reseted<br/>';
        } else {
            echo 'users categories already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE watch_biddings')) {
            echo 'Wacth Bidding reseted<br/>';
        } else {
            echo 'Watch Bidding already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('TRUNCATE TABLE wishlists')) {
            echo 'Wishlist reseted<br/>';
        } else {
            echo 'Wishlist already reseted<br/>';
        }

        if (ClassRegistry::init('Order')->query('UPDATE users set balance=0')) {
            echo 'User deposit reseted to 0<br/>';
        } else {
            echo 'User already reseted<br/>';
        }
        exit;
    }

    function admin_setPath() {
        $fields = array('Book.*');
        $books = ClassRegistry::init('Book')->find('all', array('fields' => $fields));
//        var_dump($books);exit;
        $search = '/publication/';
        $replace = 'http://store.aksaramaya.com/publication/';
//        $search = '//';
//        $replace = '/';
        if (!empty($books)) {
            foreach ($books as $book) {
                $newBook['Book']['id'] = $book['Book']['id'];
                $newBook['Book']['cover'] = str_replace($search, $replace, $book['Book']['cover']);
                $newBook['Book']['sample'] = str_replace($search, $replace, $book['Book']['sample']);
                $newBook['Book']['file'] = str_replace($search, $replace, $book['Book']['file']);
                unset(ClassRegistry::init('Book')->validate['file']);
                unset(ClassRegistry::init('Book')->validate['sample']);
                unset(ClassRegistry::init('Book')->validate['cover']);
//                debug($newBook);exit;

                if (ClassRegistry::init('Book')->save($newBook)) {
                    echo $newBook['Book']['id'] . ' moved <br/>';
                } else {
//                    var_dump(ClassRegistry::init('Book')->validationErrors);exit;
                    echo $newBook['Book']['id'] . ' failed to move <br/>';
                }
//                var_dump(ClassRegistry::init('Book')->validationErrors);exit;
            }
        }
        exit;
    }

    public function admin_initusers() {
        $errors = array();
        if (empty($this->params->query['username'])) {
            $errors[] = 'Username kosong';
        } else {
            $username = $this->params->query['username'];
        }

        if (empty($this->params->query['password'])) {
            $errors[] = 'Password kosong';
        } else {
            $password = $this->params->query['password'];
        }

        if (empty($errors)) {

            if ($username == 'admin@aksaramaya.com' && $password == 'Aks4ra') {

                $userKeepIds = array();
                $publisherIds = array();

                $user = ClassRegistry::init('User')->findByUsername('admin@aksaramaya.com');
                $userKeepIds[] = $user['User']['id'];
                // Hapus admin yang tidak valid
                ClassRegistry::init('Admin')->deleteAll(array('Admin.user_id !=' => $user['User']['id']));

                ClassRegistry::init('User')->unbindModel(array(
                    'hasOne' => array('Admin', 'Author', 'Customer', 'Librarian', 'PaymentPoint'),
                    'hasMany' => array('Page', 'Comment', 'SynchronizeToken', 'Student'),
                    'hasAndBelongsToMany' => array('Category', 'Collection')
                ));

                $conditions = array();
                $users = ClassRegistry::init('User')->find('all', array('conditions' => $conditions, 'group' => array('User.id')));

                foreach ($users as $userItem) {
                    if ($this->Tablet->isAPublisher($userItem['User']['id']) == TRUE) {
                        $publisherUser = ClassRegistry::init('PublishersUser')->findByUserId($userItem['User']['id']);
                        if (!empty($publisherUser['Publisher']['id'])) {
                            $userKeepIds[] = $userItem['User']['id'];
                            $publisherIds[] = $publisherUser['Publisher']['id'];
                        } else {
                            ClassRegistry::init('PublishersUser')->deleteAll(array('PublishersUser.user_id' => $userItem['User']['id']));
                        }
                    }
                }

                $conditions = array(
                    'NOT' => array(
                        'Book.publisher_id' => $publisherIds
                    )
                );

                ClassRegistry::init('Book')->deleteAll($conditions);

                $conditions = array(
                    'NOT' => array(
                        'Author.user_id' => $userKeepIds
                    )
                );

                ClassRegistry::init('Author')->deleteAll($conditions);

                $conditions = array(
                    'NOT' => array(
                        'Student.user_id' => $userKeepIds
                    )
                );

                ClassRegistry::init('Student')->deleteAll($conditions);


                $conditions = array(
                    'NOT' => array(
                        'Customer.user_id' => $userKeepIds
                    )
                );

                ClassRegistry::init('Customer')->deleteAll($conditions);


//                echo implode(',', $userKeepIds);
                $conditions = array(
                    'NOT' => array(
                        'User.id' => $userKeepIds
                    )
                );

//                debug($conditions);
//                exit;
                if (ClassRegistry::init('User')->deleteAll($conditions)) {
                    echo 'Inisialisasi User sukses';
                } else {
                    echo 'Gagal inisialisasi user';
                }
            } else {
                echo 'kombinasi password salah';
            }
        } else {
            $errorData = implode(',', $errors);
            echo $errorData;
        }
        exit;
    }

    public function initdata() {
        $errors = array();
        if (empty($this->params->query['username'])) {
            $errors[] = 'Username kosong';
        } else {
            $username = $this->params->query['username'];
        }

        if (empty($this->params->query['password'])) {
            $errors[] = 'Password kosong';
        } else {
            $password = $this->params->query['password'];
        }

        if (empty($errors)) {

            if ($username == 'admin@aksaramaya.com' && $password == 'Aks4ra') {
                $initDataPath = 'initial_data/';

                $fileInFolder = array();
                if ($handle = opendir($initDataPath)) {
                    while (false !== ($file = readdir($handle))) {
                        if (strstr($file, '.sql', true)) {
                            $fileInFolder[] = $file;
                        }
                    }
                    closedir($handle);
                }

//                $initDataPath = new Folder($initDataPath);
//                $sqlFiles = $$initDataPath->find();
                $sqlFiles = $fileInFolder;
                $i = 1;
                foreach ($sqlFiles as $file) {
                    $tableName = strtolower(str_replace('.sql', '', $file));

                    // Kosongkan Table terlebih dahulu
                    ClassRegistry::init('Book')->query('TRUNCATE TABLE ' . $tableName);

                    // Baca isi file .sql
                    $content = '';
                    $readFile = new File($initDataPath . $file);
                    $content = $readFile->read();
                    $readFile->close();

                    // Eksekusi isi file.sql ke database
                    ClassRegistry::init('Book')->query($content);
                    echo $i . '- table <strong>' . $tableName . '</strong> telah di isi data<br/>';
                    $i++;
                }
            } else {
                echo 'kombinasi password salah';
            }
        } else {
            $errorData = implode(',', $errors);
            echo $errorData;
        }
        exit;
    }

    public function setprice() {
        $errors = array();
        if (empty($this->params->query['username'])) {
            $errors[] = 'Username kosong';
        } else {
            $username = $this->params->query['username'];
        }

        if (empty($this->params->query['password'])) {
            $errors[] = 'Password kosong';
        } else {
            $password = $this->params->query['password'];
        }

        if (empty($errors)) {

            if ($username == 'admin@aksaramaya.com' && $password == 'Aks4ra') {
                ClassRegistry::init('Price')->query('TRUNCATE TABLE prices');
                ClassRegistry::init('Book')->unbindModel(array(
                    'belongsTo' => array('Publisher', 'Author', 'Category'),
                    'hasOne' => array('Product', 'Item', 'Collection', 'OrderDetail', 'Newsletter', 'Statistic', 'StatisticTemp'),
                    'hasMany' => array('WatchBidding', 'Bidding', 'Wishlist', 'Comment', 'Rating')
                ));
                $conditions = array(
                    'Book.is_free' => 0
                );
                ClassRegistry::init('Book')->query('TRUNCATE TABLE prices');
                $books = ClassRegistry::init('Book')->find('all', array('conditions' => $conditions));
                foreach ($books as $book) {
                    $price = array();
                    $price['Price']['type'] = 'Book';
                    $price['Price']['key'] = $book['Book']['id'];
                    $price['Price']['qty_days'] = 30;
                    $price['Price']['price'] = $book['Book']['price'] / 5;
                    ClassRegistry::init('Price')->create();
                    if (ClassRegistry::init('Price')->save($price)) {
                        echo $book['Book']['id'] . '-Sukses-' . $book['Book']['title'] . '<br/>';
                    } else {
                        echo $book['Book']['id'] . '-Gagal-' . $book['Book']['title'] . '<br/>';
                    }
                }
            } else {
                echo 'kombinasi password salah';
            }
        } else {
            $errorData = implode(',', $errors);
            echo $errorData;
        }
        exit;
    }

}
