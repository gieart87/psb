<?php

App::uses('AppController', 'Controller');

/**
 * Categories Controller
 *
 * @property Category $Category
 */
class CategoriesController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->set('status', $this->Category->status);
        $this->AclFilter->protection();
        $this->activeMenu = 'article';
        $this->set('activeMenu', $this->activeMenu);
    }

    public function beforeRender() {
        parent::beforeRender();
//        $this->set('breadcrumb', $this->Breadcrumb->setBreadcrumb($this->params->url));
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $this->Category->recursive = 0;
        $this->set('categories', $this->paginate());
    }

    /**
     * view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->set('category', $this->Category->read(null, $id));
    }

    /**
     * add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Category->create();
//            $this->request->data['Category']['permalink'] = $this->General->permalink($this->request->data['Category']['name']);
//            $this->request->data['Category']['permalink_en'] = $this->General->permalink($this->request->data['Category']['name_en']);

            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'error');
            }
        }

        $this->set(compact('types'));
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'), 'error');
        }
        if ($this->request->is('post') || $this->request->is('put')) {
//            $this->request->data['Category']['permalink'] = $this->General->permalink($this->request->data['Category']['name']);
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Category->read(null, $id);
        }

        $this->set(compact('types'));
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'), 'error');
        }
        $articlesInCategory = ClassRegistry::init('Article')->find('all', array('conditions' => array('Article.category_id' => $id)));
        if (empty($articlesInCategory)) {
            if ($this->Category->delete()) {
                $this->Session->setFlash(__('Category deleted'), 'success');
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->Session->setFlash(__('Could not delete this category cause contain articles'), 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Category was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

}
