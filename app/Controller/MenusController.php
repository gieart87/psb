<?php

App::uses('AppController', 'Controller');

/**
 * Menus Controller
 *
 * @property Menu $Menu
 */
class MenusController extends AppController {

    public function beforeFilter() {
        parent::beforeFilter();
        $this->set('status', $this->Menu->status);
        $this->AclFilter->protection();
    }

    public function beforeRender() {
        parent::beforeRender();
    }

    /**
     * index method
     *
     * @return void
     */
    public function admin_index() {
        $conditions = array();
        $menus = $this->Menu->find('all', array('conditions' => $conditions, 'order' => array('Menu.position' => 'ASC')));
        $this->set('menus', $menus);
    }

    /**
     * add method
     *
     * @return void
     */
    public function admin_add() {

        if (!empty($this->params->query)) {
            $this->request->data['Menu']['name'] = $this->params->query['name'];
            $this->request->data['Menu']['url'] = $this->params->query['url'];
            $this->request->data['Menu']['name_en'] = $this->params->query['name_en'];
            $this->request->data['Menu']['url_en'] = $this->params->query['url_en'];
        }
        if ($this->request->is('post')) {
            $this->Menu->create();
            if ($this->request->data['Menu']['parent_id'] == '') {
                $this->request->data['Menu']['parent_id'] = 0;
            }

            if ($this->Menu->save($this->request->data)) {
                $fields = array(
                    'Menu.position' => $this->Menu->id
                );
                $conditions = array(
                    'Menu.id' => $this->Menu->id
                );
                $this->Menu->updateAll($fields, $conditions);
                $this->Session->setFlash(__('The menu has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The menu could not be saved. Please, try again.'), 'error');
            }
        }
        $parentMenus = $this->Menu->find('threaded', array('order' => array('ParentMenu.name' => 'ASC')));

        $this->set(compact('parentMenus'));


        $pages = ClassRegistry::init('Page')->find('all');
        $this->set('pages', $pages);

        $categories = ClassRegistry::init('Category')->find('all');
        $this->set('categories', $categories);

        $steps = ClassRegistry::init('Step')->find('all');
        $this->set('steps', $steps);

        $documentCategories = ClassRegistry::init('DocumentCategory')->find('all');
        $this->set('documentCategories', $documentCategories);
    }

    /**
     * edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Menu->id = $id;
        if (!$this->Menu->exists()) {
            throw new NotFoundException(__('Invalid menu'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->request->data['Menu']['parent_id'] == '') {
                $this->request->data['Menu']['parent_id'] = 0;
            }
            if ($this->Menu->save($this->request->data)) {
                $this->Session->setFlash(__('The menu has been saved'), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The menu could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Menu->read(null, $id);
        }
        $parentMenus = $this->Menu->find('threaded', array('order' => array('ParentMenu.name' => 'ASC')));
        $this->set(compact('parentMenus'));
    }

    /**
     * delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Menu->id = $id;
        if (!$this->Menu->exists()) {
            throw new NotFoundException(__('Invalid menu'), 'error');
        }

        if ($this->Menu->hasChild($id)) {
            $this->Session->setFlash(__('This menu has child, could not be deleted!'), 'error');
            $this->redirect(array('action' => 'index'));
        } else {
            if ($this->Menu->delete()) {
                $this->Session->setFlash(__('Menu deleted'), 'success');
                $this->redirect(array('action' => 'index'));
            }
        }
        $this->Session->setFlash(__('Menu was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

    function admin_up($position = null) {
        if ($position == null) {
            $this->Session->setFlash(__('Invalid Menu'));
            $this->redirect(array('action' => 'index'));
        } else {
            $currentMenu = $this->Menu->findByPosition($position);

            $prevMenu = $this->Menu->getPrevMenu($position);

            if (!empty($prevMenu)) {
                $currentMenuId = $currentMenu['Menu']['id'];
                $currentMenuNewPosition = $prevMenu['Menu']['position'];

                $this->Menu->setPosition($currentMenuId, $currentMenuNewPosition);

                $prevMenuId = $prevMenu['Menu']['id'];
                $prevMenuNewPosition = $currentMenu['Menu']['position'];
                $this->Menu->setPosition($prevMenuId, $prevMenuNewPosition);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Prev Menu not Exist'), 'error');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

    function admin_down($position = null) {
        if ($position == null) {
            $this->Session->setFlash(__('Invalid Menu'));
            $this->redirect(array('action' => 'index'));
        } else {
            $currentMenu = $this->Menu->findByPosition($position);

            $nextMenu = $this->Menu->getNextMenu($position);

            if (!empty($nextMenu)) {
                $currentMenuId = $currentMenu['Menu']['id'];
                $currentMenuNewPosition = $nextMenu['Menu']['position'];
                $this->Menu->setPosition($currentMenuId, $currentMenuNewPosition);

                $nextMenuId = $nextMenu['Menu']['id'];
                $nextMenuNewPosition = $currentMenu['Menu']['position'];
                $this->Menu->setPosition($nextMenuId, $nextMenuNewPosition);
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('Next Menu not Exist'), 'errorF');
                $this->redirect(array('action' => 'index'));
            }
        }
    }

}
