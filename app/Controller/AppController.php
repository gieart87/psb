<?php

App::uses('Controller', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::uses('File', 'Utility');
App::uses('Folder', 'Utility');
App::import('Vendor', 'bing-translator/MicrosoftTranslator');
App::import('Vendor', 'rest/response');



// Bing Translator Constanta
if (!defined('ACCOUNT_KEY')) {
    define('ACCOUNT_KEY', 'cU1b1wLVcndwX6AdraTLhOK5il8QYxRA7AMNxjYM1OE');
}
if (!defined('CACHE_DIRECTORY')) {
    define('CACHE_DIRECTORY', Router::url('/') . 'cache/');
}
if (!defined('LANG_CACHE_FILE')) {
    define('LANG_CACHE_FILE', 'lang.cache');
}
if (!defined('ENABLE_CACHE')) {
    define('ENABLE_CACHE', true);
}
if (!defined('UNEXPECTED_ERROR')) {
    define('UNEXPECTED_ERROR', 'There is some un expected error . please check the code');
}
if (!defined('MISSING_ERROR')) {
    define('MISSING_ERROR', 'Missing Required Parameters ( Language or Text) in Request');
}

// End Bing Config

class AppController extends Controller {

    public $components = array(
        'General',
        'CakeMailer',
        'Session',
        'Auth',
        'Acl',
        'Acl.AclFilter',
        'RequestHandler',
//        'Breadcrumb'
    );
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Form',
        'General',
        'Tree',
        'SocialSignIn.Facebook',
    );
    public $layout = 'public';
    public $siteLogo = '';
    public $domain = '';
    public $accessApi = '';
    public $widgets = array();
    public $sessionId = '';
    public $currentLang = '';
    public $activeMenu = '';
    public $response = null;

    function beforeFilter() {
        $fullUrl = Router::url(null, true);
        $url = explode('/', $fullUrl);
        $this->domain = $url[2];
        $this->set('domain', $this->domain);
        #========================================LAYOUT_INIT==========================#
        if (!empty($this->params->params['prefix']) && in_array($this->params->params['prefix'], array('admin', 'publisher', 'author', 'librarian', 'payment'))):
            $this->layout = 'admin';
        endif;

        $this->set('prefix', isset($this->params->params['prefix']) ? $this->params->params['prefix'] : '');
        #========================================LAYOUT_INIT==========================#
        //Configure AuthComponent
        $this->Auth->loginAction = array('admin' => false, 'librarian' => false, 'payment' => false, 'controller' => 'pages', 'action' => 'display');

        #========================================UPLOADER_INIT==========================#
        CakePlugin::load('Uploader');
        App::import('Vendor', 'Uploader.Uploader');


        $this->AclFilter->auth();

        // Set Default Timezone
        date_default_timezone_set('Asia/Jakarta');

        $this->accessApi = new HttpSocket();
        /*
         * End Set Client ID
         */

        $this->set('siteLogo', $this->siteLogo);

        $this->setSession();


        //Set Default Widgets
        $this->widgets[] = 'search';
        $this->set('widgets', $this->widgets);

        //set language
        $this->currentLang = $this->Session->read('lang');

        if (empty($this->currentLang)) {
            $lang = 'ID';
            $this->Session->write('lang', $lang);
        } else {
            if (!empty($this->params->query['lang']) && in_array($this->params->query['lang'], array('ID', 'EN'))) {
                $lang = $this->params->query['lang'];
                $this->Session->write('lang', $lang);
            }
        }
//        debug($currentLang);exit;
        $this->set('activeMenu', $this->activeMenu);
    }

    public function setSession() {
        #Get Session Id
        $data = $this->Session->read();
        $this->sessionId = $data['Config']['userAgent'];
        $this->set('sessionId', $this->sessionId);
    }

    /**
     * 
     * @param type $errors
     * @return string
     * Fungsi utk formating error message default dari CakePHP ke array standar
     */
    public function reformatError($errors) {
        $error_message = '';
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $error_message .= $key . ' : ' . $value[0] . ' ';
            }
        }
        return $error_message;
    }

    /**
     * Array Pagination Function.
     * By Sergey Gurevich.
     * 
     * Input: 
     * 1 - Target Array.
     * 2 - Page Number.
     * 3 - Link prefix (example: "?page=").
     * 4 - Link suffix.
     * 5 - Results per page.
     * 6 - Number of pages displayed in the page link panel.
     * 
     * Output:
     * - $output['panel'] - Link Panel HTML source.
     * - $output['offset'] - Current page number.
     * - $output['limit'] - Number of resuts per page.
     * - $output['array'] = - Array of current page results.
     * 
     * Will return FALSE if no pagination was done.
     */
    function pagination_array($array, $page = 1, $link_prefix = false, $link_suffix = false, $limit_page = 20, $limit_number = 10) {
        if (empty($page) or !$limit_page)
            $page = 1;

        $num_rows = count($array);
//        -- Modified By GieArt
//        if (!$num_rows or $limit_page >= $num_rows) 
//            return false;
//        -- Modified by Gie-art
        $num_pages = ceil($num_rows / $limit_page);
        $page_offset = ($page - 1) * $limit_page;

        //Calculating the first number to show.
        if ($limit_number) {
            $limit_number_start = $page - ceil($limit_number / 2);
            $limit_number_end = ceil($page + $limit_number / 2) - 1;
            if ($limit_number_start < 1)
                $limit_number_start = 1;
            //In case if the current page is at the beginning.
            $dif = ($limit_number_end - $limit_number_start);
            if ($dif < $limit_number)
                $limit_number_end = $limit_number_end + ($limit_number - ($dif + 1));
            if ($limit_number_end > $num_pages)
                $limit_number_end = $num_pages;
            //In case if the current page is at the ending.
            $dif = ($limit_number_end - $limit_number_start);
            if ($limit_number_start < 1)
                $limit_number_start = 1;
        }
        else {
            $limit_number_start = 1;
            $limit_number_end = $num_pages;
        }
        //Generating page links.
        $panel = '';
        for ($i = $limit_number_start; $i <= $limit_number_end; $i++) {
            $page_cur = "<a href='$link_prefix$i$link_suffix'>$i</a>";
            if ($page == $i)
                $page_cur = "<b>$i</b>";
            else
                $page_cur = "<a href='$link_prefix$i$link_suffix'>$i</a>";

            $panel .= " <span>$page_cur</span>";
        }

        $panel = trim($panel);
        //Navigation arrows.
        if ($limit_number_start > 1)
            $panel = "<b><a href='$link_prefix" . (1) . "$link_suffix'>&lt;&lt;</a>  <a href='$link_prefix" . ($page - 1) . "$link_suffix'>&lt;</a></b> $panel";
        if ($limit_number_end < $num_pages)
            $panel = "$panel <b><a href='$link_prefix" . ($page + 1) . "$link_suffix'>&gt;</a> <a href='$link_prefix$num_pages$link_suffix'>&gt;&gt;</a></b>";

//        $output['panel'] = $panel; //Panel HTML source.
        $output['offset'] = $page_offset; //Current page number.
        $output['limit'] = $limit_page; //Number of resuts per page.
        $output['data'] = array_slice($array, $page_offset, $limit_page, true); //Array of current page results.

        return $output;
    }

    function getHasedTime($time) {
        $hash = intval(crc32($time));
        $code = str_replace('-', '', $hash);
        return $code;
    }

}

?>
