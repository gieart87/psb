<?php

/*
 * Controller/SekolahsController.php
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */

class SekolahsController extends AppController {

    var $paginate = array(
        'limit' => 15
    );

    function beforeFilter() {
        parent::beforeFilter();
        $this->AclFilter->protection();
        // $this->response = new Response();

        $this->set('status', $this->Sekolah->status);
        $this->activeMenu = 'event';
        $this->set('activeMenu', $this->activeMenu);
        $kelamins = array(
            1 => 'laki-laki',
            2 => 'perempuan'
        );

        $this->set('kelamins', $kelamins);
    }

    function admin_index() {

        $this->set('sekolahs', $this->Sekolah->find('all'));
    }

    function admin_add() {
        if ($this->request->is('ajax')) {
            $sekolah['Sekolah']['nama'] = $this->request->data['nama'];
            if ($savedData = ClassRegistry::init('Sekolah')->save($sekolah)) {
                $this->response->ok($savedData);
            } else {
                $this->response->bad_request('Tambah sekolah gagal');
            }
        }
        if (!empty($this->data)) {
            $this->Sekolah->create();
            if ($this->Sekolah->save($this->data)) {
                $this->Session->setFlash(__('The event has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.', true), 'error');
            }
        }
        $jurusans = ClassRegistry::init('Jurusan')->find('list');
        $this->set('jurusans', $jurusans);
    }

    function admin_edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid event', true), 'error');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            if ($this->Sekolah->save($this->data)) {
                $this->Session->setFlash(__('The event has been saved', true), 'success');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The event could not be saved. Please, try again.', true), 'error');
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Sekolah->read(null, $id);
        }

    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for event', true), 'error');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->Sekolah->delete($id)) {
            $this->Session->setFlash(__('Sekolah deleted', true), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Sekolah was not deleted', true), 'error');
        $this->redirect(array('action' => 'index'));
    }

    // The feed action is called from "webroot/js/ready.js" to get the list of siswas (JSON)
    function feed($id = null) {
        $this->layout = "ajax";
        $vars = $this->params['url'];
        $conditions = array('conditions' => array('UNIX_TIMESTAMP(start) >=' => $vars['start'], 'UNIX_TIMESTAMP(start) <=' => $vars['end']));
        $siswas = $this->Sekolah->find('all', $conditions);
        foreach ($siswas as $event) {
            if ($event['Sekolah']['all_day'] == 1) {
                $allday = true;
                $end = $event['Sekolah']['start'];
            } else {
                $allday = false;
                $end = $event['Sekolah']['end'];
            }
            $data[] = array(
                'id' => $event['Sekolah']['id'],
                'title' => $event['Sekolah']['title'],
                'start' => $event['Sekolah']['start'],
                'end' => $end,
                'allDay' => $allday,
                'url' => Router::url('/') . 'full_calendar/siswas/view/' . $event['Sekolah']['id'],
                'details' => $event['Sekolah']['details'],
                'className' => $event['SekolahType']['color']
            );
        }
        $this->set("json", json_encode($data));
    }

    // The update action is called from "webroot/js/ready.js" to update date/time when an event is dragged or resized
    function update() {
        $vars = $this->params['url'];
        $this->Sekolah->id = $vars['id'];
        $this->Sekolah->saveField('start', $vars['start']);
        $this->Sekolah->saveField('end', $vars['end']);
        $this->Sekolah->saveField('all_day', $vars['allday']);
    }

}

?>
