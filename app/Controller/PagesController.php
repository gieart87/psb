<?php

App::uses('AppController', 'Controller');

/**
 * Pages Controller
 *
 * @property Page $Page
 */
class PagesController extends AppController {

    public $components = array('Recaptcha.Recaptcha');
    public $helpers = array('Recaptcha.Recaptcha');

    /**
     * index method
     *
     * @return void
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array_merge($this->Auth->allowedActions, array(
            'index',
            'read',
            'structure',
            'complaint'
                )
        );
        $this->AclFilter->protection();
        $this->set('status', $this->Page->status);
        $this->set('themes', $this->Page->themes);
        $this->activeMenu = 'page';
        $this->set('activeMenu', $this->activeMenu);
    }

    public function index() {
        $options = array();
        $options['conditions'] = array(
            'Siswa.pilihan_satu' => 1
        );
        $options['fields'] = array('Siswa.*,((4*Siswa.nilai_matematika)+(2*Siswa.nilai_b_inggris)+(3*Siswa.nilai_ipa) + (1*Siswa.nilai_b_indonesia)) as bobot');
        $options['order'] = array('bobot' => 'DESC');
        $audioVideoSiswas = ClassRegistry::init('Siswa')->find('all', $options);
        $this->set('audioVideoSiswas', $audioVideoSiswas);

        $options = array();
        $options['conditions'] = array(
            'Siswa.pilihan_satu' => 2
        );
        $options['fields'] = array('Siswa.*,((4*Siswa.nilai_matematika)+(2*Siswa.nilai_b_inggris)+(3*Siswa.nilai_ipa) + (1*Siswa.nilai_b_indonesia)) as bobot');
        $options['order'] = array('bobot' => 'DESC');
        $otomotifSiswas = ClassRegistry::init('Siswa')->find('all', $options);
        $this->set('otomotifSiswas', $otomotifSiswas);

        $options = array();
        $options['conditions'] = array(
            'Siswa.pilihan_satu' => 3
        );
        $options['fields'] = array('Siswa.*,((4*Siswa.nilai_matematika)+(2*Siswa.nilai_b_inggris)+(3*Siswa.nilai_ipa) + (1*Siswa.nilai_b_indonesia)) as bobot');
        $options['order'] = array('bobot' => 'DESC');
        $multimediaSiswas = ClassRegistry::init('Siswa')->find('all', $options);
        $this->set('multimediaSiswas', $multimediaSiswas);


        // all 
        $options = array();
        $options['fields'] = array('Siswa.*,((4*Siswa.nilai_matematika)+(2*Siswa.nilai_b_inggris)+(3*Siswa.nilai_ipa) + (1*Siswa.nilai_b_indonesia)) as bobot');
        $options['order'] = array('bobot' => 'DESC');
        $all = ClassRegistry::init('Siswa')->find('count');

        $this->set('all', $all);

        // tertinggi
        $options = array();
        $options['fields'] = array('Siswa.*,((4*Siswa.nilai_matematika)+(2*Siswa.nilai_b_inggris)+(3*Siswa.nilai_ipa) + (1*Siswa.nilai_b_indonesia)) as bobot');
        $options['order'] = array('bobot' => 'DESC');
        $options['limit'] = 1;
        $highest = ClassRegistry::init('Siswa')->find('first', $options);
        if(!empty($highest['0']['bobot'])){
        $this->set('highest', $highest['0']['bobot']);
        }
        //terendah 

        $options = array();
        $options['fields'] = array('Siswa.*,((4*Siswa.nilai_matematika)+(2*Siswa.nilai_b_inggris)+(3*Siswa.nilai_ipa) + (1*Siswa.nilai_b_indonesia)) as bobot');
        $options['order'] = array('bobot' => 'ASC');
        $options['limit'] = 1;
        $lowest = ClassRegistry::init('Siswa')->find('first', $options);
        if(!empty($lowest['0']['bobot'])){
        $this->set('lowest', $lowest['0']['bobot']);
        }

        $this->set('home', 1);
    }

    public function read($permalink) {
        $options['conditions'] = array(
            'OR' => array(
                'Page.permalink' => $permalink,
                'Page.permalink_en' => $permalink
            )
        );
        $page = ClassRegistry::init('Page')->find('first', $options);

        if ($this->currentLang == 'EN') {
            $page['Page']['title'] = $page['Page']['title_en'];
            $page['Page']['permalink'] = $page['Page']['permalink_en'];
            $page['Page']['body'] = $page['Page']['body_en'];
        }

        if (!empty($page)) {
            $this->set('page', $page);

            $options = array();
            $options['conditions'] = array(
                'ContentTab.type' => 'Page',
                'ContentTab.key' => $page['Page']['id'],
                'ContentTab.status' => 1
            );
            $options['order'] = array('ContentTab.created' => 'ASC');
            $contentTabs = ClassRegistry::init('ContentTab')->find('all', $options);
            if ($this->currentLang == 'EN') {
                foreach ($contentTabs as $key => $val) {
                    $contentTabs[$key]['ContentTab']['title'] = $val['ContentTab']['title_en'];
                    $contentTabs[$key]['ContentTab']['body'] = $val['ContentTab']['body_en'];
                }
            }
            $this->set('contentTabs', $contentTabs);
        } else {
            $this->Session->setFlash(__('Invalid page'), 'error');
            $this->redirect(array('action' => 'index'));
        }
        $this->set('widgets', array_merge($this->widgets, array('other_pages')));
    }

    public function structure() {
        
    }

    public function complaint() {

        if ($this->request->is('POST')) {
            $complaint['Complaint'] = $this->request->data['Page'];
            if (ClassRegistry::init('Complaint')->save($complaint)) {
                $this->Session->setFlash(__('Complaint sent successfully'), 'success');
            } else {
                $this->Session->setFlash(__('Failed, try again'), 'error');
            }
            $this->redirect(array('action' => 'complaint'));
        }
        $options = array();
        $options['fields'] = array('ComplaintTopic.id', 'ComplaintTopic.topic');
        $options['order'] = array('ComplaintTopic.topic' => 'ASC');
        $complaintTopics = ClassRegistry::init('ComplaintTopic')->find('list', $options);
        $this->set('complaintTopics', $complaintTopics);
    }

    public function admin_dashboard() {
        
    }

    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index() {
        $options = array();
//        $options['fields'] = array(
//            'Page.id,Page.title,Page.status,Page.created,Page.user_id,User.name,User.id'
//        );
        $options['order'] = array(
            'Page.created' => 'DESC'
        );
        $pages = ClassRegistry::init('Page')->find('all', $options);
        $this->set('pages', $pages);
    }

    /**
     * admin_view method
     *
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'));
        }
        $this->set('page', $this->Page->read(null, $id));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {

        if ($this->request->is('post')) {

            $this->Page->create();

            $this->request->data['Page']['user_id'] = $this->Session->read('Auth.User.id');

            if ($this->Page->save($this->request->data)) {
                if (!empty($this->request->data['Page']['image'])) {
                    if ($this->request->data['Page']['image']['error'] != 4) {
                        $image['Image']['type'] = $this->Page->alias;
                        $image['Image']['key'] = $this->Page->id;
                        $image['Image']['description'] = $this->request->data['Page']['title'];
                        $image['Image']['image'] = $this->request->data['Page']['image'];
                        $image['Image']['mime'] = $this->request->data['Page']['image']['type'];
                        ClassRegistry::init('Image')->save($image);
                    }
                }
                $this->Session->setFlash(__('The page has been saved'), 'success');
                $this->redirect(array('action' => 'edit', $this->Page->id));
            } else {
                $this->Session->setFlash(__('The page could not be saved. Please, try again.'), 'error');
            }
        }
        $options = array();
        $options['order'] = array('PageCategory.name' => 'ASC');
        $pageCategories = ClassRegistry::init('PageCategory')->find('list', $options);
        $users = $this->Page->User->find('list');

        $this->set(compact('users', 'pageCategories'));
    }

    /**
     * admin_edit method
     *
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        $this->Page->id = $id;

        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'), 'error');
        }
        if ($this->request->is('post') || $this->request->is('put')) {


            if ($this->Page->save($this->request->data)) {
                if (!empty($this->request->data['Page']['image'])) {
                    if ($this->request->data['Page']['image']['error'] != 4) {
                        //Delete exist image
                        $conditions = array(
                            'Image.type' => $this->Page->alias,
                            'Image.key' => $this->Page->id
                        );
                        $images = ClassRegistry::init('Image')->find('all', array('conditions' => $conditions));
                        if (!empty($images)) {
                            ClassRegistry::init('Image')->deleteAll($conditions);
                        }
                        //------------------------
                        $image['Image']['type'] = $this->Page->alias;
                        $image['Image']['key'] = $this->Page->id;
                        $image['Image']['description'] = $this->request->data['Page']['title'];
                        $image['Image']['image'] = $this->request->data['Page']['image'];
                        $image['Image']['mime'] = $this->request->data['Page']['image']['type'];

                        ClassRegistry::init('Image')->save($image);
                    }
                }

                $this->Session->setFlash(__('The page has been saved'), 'success');
                $this->redirect(array('action' => 'edit', $this->Page->id));
            } else {
                $this->Session->setFlash(__('The page could not be saved. Please, try again.'), 'error');
            }
        } else {
            $this->request->data = $this->Page->read(null, $id);
        }

        $options = array();
        $options['order'] = array('PageCategory.name' => 'ASC');
        $pageCategories = ClassRegistry::init('PageCategory')->find('list', $options);
        $users = $this->Page->User->find('list');

        $options = array();
        $options['conditions'] = array(
            'ContentTab.type' => $this->Page->alias,
            'ContentTab.key' => $this->Page->id
        );
        $contentTabs = ClassRegistry::init('ContentTab')->find('all', $options);

        $this->set(compact('users', 'pageCategories', 'contentTabs'));
    }

    /**
     * admin_delete method
     *
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        $this->Page->id = $id;
        if (!$this->Page->exists()) {
            throw new NotFoundException(__('Invalid page'), 'error');
        }
        if ($this->Page->delete()) {
            $this->Session->setFlash(__('Page deleted'), 'success');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('Page was not deleted'), 'error');
        $this->redirect(array('action' => 'index'));
    }

}
