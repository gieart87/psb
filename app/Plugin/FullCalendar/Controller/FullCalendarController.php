<?php

/*
 * Controller/FullCalendarController.php
 * CakePHP Full Calendar Plugin
 *
 * Copyright (c) 2010 Silas Montgomery
 * http://silasmontgomery.com
 *
 * Licensed under MIT
 * http://www.opensource.org/licenses/mit-license.php
 */

class FullCalendarController extends FullCalendarAppController {

    var $name = 'FullCalendar';

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allowedActions = array_merge($this->Auth->allowedActions, array('index', 'view'));
        $this->AclFilter->protection();
    }

    function index() {
        
    }

}

?>
