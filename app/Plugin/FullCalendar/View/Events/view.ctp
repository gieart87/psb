<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left"><?php echo $this->General->lang('event_detail'); ?></h1>
        <!--        <ul class="pull-right breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Features</a></li>
                    <li><a href="">Events Archive</a></li>
                    <li class="active">Jawa Ipsum</li>
                </ul>-->
    </div>
</div>

<div class="container content">		
    <div class="row blog-page">    
        <!-- Event Detail -->
        <div class="col-md-9 md-margin-bottom-60">
            <!--Blog Post-->        
            <div class="blog margin-bottom-40">
                <h2><?php echo $event['Event']['title']; ?></h2>

                <div class="blog-post-tags">
                    <ul class="list-unstyled list-inline blog-info">
                        <li><i class="fa fa-calendar"></i> <?php echo $this->General->humanDate2($event['Event']['created']); ?></li>
                    </ul>

                </div>
                <div class="blog-img">
                    <?php $image = $this->General->getSingleImage('Event', $event['Event']['id']); ?>
                    <?php if (!empty($image)): ?>
                        <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=350&w=825', array('class' => 'img-responsive')); ?>
                        <br/>                   
                    <?php endif; ?>
                </div>

                <table class="table table-bordered" style="width: 40%">
                    <tr>
                        <td>Type</td><td><?php echo $event['EventType']['name']; ?></td>
                    </tr>
                    <tr>
                        <td>Start</td><td><?php echo $this->General->humanDate($event['Event']['start']); ?></td>
                    </tr>
                    <tr>
                        <td>End</td>
                        <td>
                            <?php
                            if ($event['Event']['all_day'] != 1) {
                                echo $this->General->humanDate($event['Event']['end']);
                            } else {
                                echo "N/A";
                            }
                            ?>    
                        </td>
                    </tr>
                    <tr>
                        <td>Status</td><td><?php echo $event['Event']['status']; ?></td>
                    </tr>
                    <tr>
                        <td>All days</td><td>
                            <?php
                            if ($event['Event']['all_day'] == 1) {
                                echo "Yes";
                            } else {
                                echo "No";
                            }
                            ?>
                        </td>
                    </tr>
                </table>
                <?php echo $event['Event']['details']; ?>
            </div>
            <!--End Blog Post-->        

            <!-- Disqus System -->
            <div id="disqus_thread"></div>
            <script type="text/javascript">
                /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
                var disqus_shortname = 'imigrasijogja'; // required: replace example with your forum shortname
                var disqus_title = '<?php echo $event['Event']['title']; ?>';
                var disqus_url = '<?php echo $this->General->getSetting('Main.SiteUrl') . 'articles/read/' . $event['Event']['permalink'] ?>';

                /* * * DON'T EDIT BELOW THIS LINE * * */
                (function() {
                    var dsq = document.createElement('script');
                    dsq.type = 'text/javascript';
                    dsq.async = true;
                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('details')[0]).appendChild(dsq);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
            <a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>

        </div>
        <?php echo $this->element('public/right_sidebar'); ?>
    </div><!--/row-->        
</div>


