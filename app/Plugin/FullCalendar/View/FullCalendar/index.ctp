<script type="text/javascript">
    plgFcRoot = '<?php echo $this->Html->url('/'); ?>' + "full_calendar";
</script>
<?php
echo $this->Html->script(array('/full_calendar/js/jquery-1.5.min', '/full_calendar/js/jquery-ui-1.8.9.custom.min', '/full_calendar/js/fullcalendar.min', '/full_calendar/js/jquery.qtip-1.0.0-rc3.min', '/full_calendar/js/ready'), array('inline' => 'false'));
echo $this->Html->css('/full_calendar/css/fullcalendar', null, array('inline' => false));
?>
<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left"><?php echo $this->General->lang('event_calendar'); ?></h1>
        <!--        <ul class="pull-right breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Features</a></li>
                    <li><a href="">Events Archive</a></li>
                    <li class="active">Jawa Ipsum</li>
                </ul>-->
    </div>
</div>

<div class="container content">		
    <div class="row blog-page">    
        <!-- Event Detail -->
        <div class="col-md-12 md-margin-bottom-60">
            <div id="calendar"></div>
        </div>
    </div>
</div>