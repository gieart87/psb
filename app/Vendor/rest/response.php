<?php

/**
 * Description of response
 *
 * @author tediscript
 */
class Response {

    public function not_found($str = "") {
        $response["meta"]["code"] = 404;
        if ($str) {
            $response["meta"]["error_message"] = $str;
        } else {
            $response["meta"]["error_message"] = "Not Found";
        }
        $json = json_encode($response);
        header("Status: HTTP/1.1 404 Not Found");
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json");
        echo $json;
        exit;
    }

    public function unauthorized($str = "") {
        $response["meta"]["code"] = 401;
        if ($str) {
            $response["meta"]["error_message"] = $str;
        } else {
            $response["meta"]["error_message"] = "Unauthorized";
        }
        $json = json_encode($response);
        header("Status: HTTP/1.1 401 Unauthorized");
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json");
        echo $json;
        exit;
    }

    public function bad_request($str = "", $error_code = null) {

        $response["meta"]["code"] = 400;
        if ($str) {
            $response["meta"]["error_message"] = $str;
        } else {
            $response["meta"]["error_message"] = "Bad Request";
        }

        if (!empty($error_code)) {
            $response["meta"]["error_code"] = $error_code;
        }

        $json = json_encode($response);
        header("Status: HTTP/1.1 400 Bad Request");
        header("Access-Control-Allow-Origin: *");
        header("Content-type: application/json");
        echo $json;
        exit;
    }

    public function ok($data = array(), $confirm = 'success', $forceJsonObject = false) {
        $response["meta"]["code"] = 200;
        $response["meta"]["confirm"] = $confirm;
        if (!empty($data)) {
            $response["data"] = $data;
        }
        if ($forceJsonObject == true) {
            $json = json_encode($response, JSON_FORCE_OBJECT);
        } else {
            $json = json_encode($response);
        }
        header("Status: HTTP/1.1 200 OK");
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        echo $json;
        exit;
    }

}

?>
