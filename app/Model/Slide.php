<?php

App::uses('AppModel', 'Model');

/**
 * Photo Model
 *
  <<<<<<< HEAD
 * @property User $User
 * @property Category $Category
  =======
 * @property Gallery $Gallery
 * @property User $User
  >>>>>>> f995fe369593ba6fc4c364b725a6687f8bce0f98
 */
class Slide extends AppModel {

    public $status = array(
        0 => 'draft',
        1 => 'published'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'title' => array(
            'notempty' => array(
                'rule' => array('isUnique'),
                'message' => 'This name already exist'
            )
        ),
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $hasOne = array(
        'Image' => array(
            'className' => 'Image',
            'foreignKey' => 'key',
            'conditions' => array('Image.type' => 'Slide'),
            'dependent' => TRUE
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    function beforeFind($queryData) {
        $queryData['order'][$this->alias . '.id'] = 'DESC';
        return $queryData;
    }

    function beforeSave($options = array()) {
        parent::beforeSave($options);
        $this->data['Slide']['description'] = $this->cleanHtml($this->data['Slide']['description']);
        $this->data['Slide']['description_en'] = $this->cleanHtml($this->data['Slide']['description_en']);
    }

}
