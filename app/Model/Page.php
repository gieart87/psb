<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 * @property User $User
 */
class Page extends AppModel {

    public $status = array(
        0 => 'Pending',
        1 => 'Active',
        2 => 'Trash',
    );
    public $themes = array(
        0 => 'Text Default',
        1 => 'PDF Viewer'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'title' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'permalink' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        $this->data['Page']['permalink'] = $this->urlTitle($this->data['Page']['title']);
        $this->data['Page']['permalink_en'] = $this->urlTitle($this->data['Page']['title_en']);
        $this->data['Page']['body'] = $this->cleanHtml($this->data['Page']['body']);
        $this->data['Page']['body_en'] = $this->cleanHtml($this->data['Page']['body_en']);
    }

    public function findPagesByPermalinkCategory($permalink) {
        $this->bindModel(array(
            'belongsTo' => array('PageCategory')
        ));
        $options = array();
        $options['conditions'] = array(
            'OR' => array(
                'PageCategory.permalink' => $permalink,
                'PageCategory.permalink_en' => $permalink
            )
        );
        $pages = $this->find('all', $options);
        return $pages;
    }

}
