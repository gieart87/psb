<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 * @property Type $Type
 * @property Book $Book
 * @property Magazine $Magazine
 * @property Newspaper $Newspaper
 * @property Tabloid $Tabloid
 * @property ZPosDetail $ZPosDetail
 * @property ZPublishing $ZPublishing
 */
class Setting extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     * 
     * 
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    /**
     * hasMany associations
     *
     * @var array
     */
}
