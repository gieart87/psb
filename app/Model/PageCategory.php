<?php

App::uses('AppModel', 'Model');

/**
 * PageCategory Model
 *
 * @property Type $Type
 * @property Book $Book
 * @property Magazine $Magazine
 * @property Newspaper $Newspaper
 * @property Tabloid $Tabloid
 * @property ZPosDetail $ZPosDetail
 * @property ZPublishing $ZPublishing
 */
class PageCategory extends AppModel {

    public $status = array(
        0 => 'Draft',
        1 => 'Published',
    );

    /**
     * Validation rules
     *
     * @var array
     * 
     * 
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Kategori sudah ada',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */

    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        $this->data['PageCategory']['permalink'] = $this->urlTitle($this->data['PageCategory']['name']);
        $this->data['PageCategory']['permalink_en'] = $this->urlTitle($this->data['PageCategory']['name_en']);
    }
    
    

}
