<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 * @property User $User
 */
class Sekolah extends AppModel {

    public $status = array(
        0 => 'Pending',
        1 => 'Active',
        2 => 'Trash',
    );
    public $displayField = 'nama';

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'nama' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Kategori sudah ada'
            )
        ),
    );

}
