<?php

App::uses('AppModel', 'Model');

/**
 * Article Model
 *
 * @property User $User
 * @property Category $Category
 */
class Article extends AppModel {

    public $status = array(
        0 => 'draft',
        1 => 'published'
    );
    public $actsAs = array('Search.Searchable', 'Tags.Taggable');
    public $hasAndBelongsToMany = array('Tag' => array('with' => 'Tagged'));
    public $filterArgs = array(
        'title' => array('type' => 'like'),
        'search' => array('type' => 'like', 'field' => 'Article.description'),
        'tags' => array('type' => 'subquery', 'method' => 'findByTags', 'field' => 'Article.id'),
        'filter' => array('type' => 'query', 'method' => 'orConditions'),
//        'enhanced_search' => array('type' => 'like', 'encode' => true, 'before' => false, 'after' => false, 'field' => array('Category.name', 'User.name')),
    );

    public function findByTags($data = array()) {
        $this->Tagged->Behaviors->attach('Containable', array('autoFields' => false));
        $this->Tagged->Behaviors->attach('Search.Searchable');
        $query = $this->Tagged->getQuery('all', array(
            'conditions' => array('Tag.name' => $data['tags']),
            'fields' => array('foreign_key'),
            'contain' => array('Tag')
        ));
        return $query;
    }

    public function orConditions($data = array()) {
        $filter = $data['filter'];
        $cond = array(
            'OR' => array(
                $this->alias . '.title LIKE' => '%' . $filter . '%',
                $this->alias . '.body LIKE' => '%' . $filter . '%',
        ));
        return $cond;
    }

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'title' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'This title already exist'
            )
        ),
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $hasOne = array(
        'Image' => array(
            'className' => 'Image',
            'foreignKey' => 'key',
            'conditions' => array('Image.type' => 'Article'),
            'dependent' => TRUE
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

//    public $hasAndBelongsToMany = array(
//        'Category' => array(
//            'className' => 'Category',
//            'foreignKey' => 'article_id',
//            'conditions' => '',
//            'fields' => '',
//            'order' => ''
//        )
//    );

    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        $this->data['Article']['permalink'] = $this->urlTitle($this->data['Article']['title']);
        $this->data['Article']['permalink_en'] = $this->urlTitle($this->data['Article']['title_en']);
        $this->data['Article']['body'] = $this->cleanHtml($this->data['Article']['body']);
        $this->data['Article']['body_en'] = $this->cleanHtml($this->data['Article']['body_en']);
    }

    function beforeFind($queryData) {
        $queryData['order'][$this->alias . '.id'] = 'DESC';
        return $queryData;
    }

    function findByCategory($limit = null, $categoryId = null) {
        $conditions = array(
            'Article.status' => 1,
        );

//        if ($categoryId != null) {
//            $conditions['Category.id'] = $categoryId;
//        }
        $articles = ClassRegistry::init('Article')->find('all', array('conditions' => $conditions, 'limit' => $limit, 'order' => array('Article.id' => 'DESC')));

        $data = array();
        if (!empty($articles)) {

            foreach ($articles as $article) {

                if (!empty($categoryId)) {
                    $categoryIds = array();
                    if (!empty($article['Category'])) {
                        foreach ($article['Category'] as $category) {
                            $categoryIds[] = $category['id'];
                        }
                    }

                    if (in_array($categoryId, $categoryIds)) {
                        $data[] = $article;
                    }
                } else {
                    $data[] = $article;
                }
            }
        }
        return $data;
    }

}
