<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 * @property User $User
 */
class Siswa extends AppModel {

    public $status = array(
        0 => 'Terdaftar',
        1 => 'Diterima',
        2 => 'Cadangan',
        3 => 'Tidak Diterima',
        4 => 'Batal',
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'no_pendaftaran' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'nama' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'tempat_lahir' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'alamat' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'nilai_b_indonesia' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'nilai_b_inggris' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'nilai_matematika' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'nilai_ipa' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //    'message' => 'no pendaftaran harus diisi',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        )
    );

    public function rata2Mapel($mapel) {
//        $options = array();
//        if($)
    }

}
