<?php

App::uses('AppModel', 'Model');

/**
 * Photo Model
 *
<<<<<<< HEAD
 * @property User $User
 * @property Category $Category
=======
 * @property Gallery $Gallery
 * @property User $User
>>>>>>> f995fe369593ba6fc4c364b725a6687f8bce0f98
 */
class Photo extends AppModel {

    public $status = array(

        0 => 'draft',
        1 => 'published'

    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
             'rule' => array('isUnique'),
                'message' => 'This name already exist'
            )
        ),
        'status' => array(
        ),
        'gallery_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'user_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
      
    );
    //The Associations below have been created with all possible keys, those that are not needed can be removed

    public $hasOne = array(
        'Image' => array(
            'className' => 'Image',
            'foreignKey' => 'key',
            'conditions' => array('Image.type' => 'Photo'),
            'dependent' => TRUE
        ),
    );

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(

        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Gallery' => array(
            'className' => 'Gallery',
            'foreignKey' => 'gallery_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),

    );    
    
    function beforeFind($queryData) {
        $queryData['order'][$this->alias . '.id'] = 'DESC';
        return $queryData;
    }

    function findByGallery($limit = null, $galleryId = null) {
        $conditions = array(
            'Photo.status' => 1,
        );

//        if ($galleryId != null) {
//            $conditions['Category.id'] = $galleryId;
//        }
        $galleries = ClassRegistry::init('Photo')->find('all', array('conditions' => $conditions, 'limit' => $limit, 'order' => array('Photo.id' => 'DESC')));

        $data = array();
        if (!empty($galleries)) {

            foreach ($galleries as $gallery) {

                if (!empty($galleryId)) {
                    $galleryIds = array();
                    if (!empty($gallery['Category'])) {
                        foreach ($gallery['Category'] as $category) {
                            $galleryIds[] = $category['id'];
                        }
                    }

                    if (in_array($galleryId, $galleryIds)) {
                        $data[] = $gallery;
                    }
                } else {
                    $data[] = $gallery;
                }
            }
        }
        return $data;
    }

}
