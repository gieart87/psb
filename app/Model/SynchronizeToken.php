<?php

App::uses('AppModel', 'Model');

/**
 * Page Model
 *
 * @property User $User
 */
class SynchronizeToken extends AppModel {
    /**
     * Validation rules
     *
     * @var array
     */

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
