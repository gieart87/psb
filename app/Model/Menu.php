<?php

App::uses('AppModel', 'Model');

/**
 * Menu Model
 *
 * @property Menu $ParentMenu
 * @property Menu $ChildMenu
 */
class Menu extends AppModel {

    public $status = array(
        0 => 'Draft',
        1 => 'Published'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'url' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'status' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'position' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'ParentMenu' => array(
            'className' => 'Menu',
            'foreignKey' => 'parent_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'ChildMenu' => array(
            'className' => 'Menu',
            'foreignKey' => 'parent_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    function getMenu() {
        $conditions = array(
            'Menu.status' => 1
        );
        $results = $this->find('threaded', array('conditions' => $conditions, 'order' => array('Menu.position' => 'ASC')));
        return $results;
    }

    function setPosition($menuId, $position) {

        $fields = array(
            'Menu.position' => $position
        );

        $conditions = array(
            'Menu.id' => $menuId
        );
        if ($this->updateAll($fields, $conditions)) {
            return true;
        } else {
            return false;
        }
    }

    function getPrevMenu($position) {
        $conditions = array(
            'Menu.position <' => $position
        );
        $menu = $this->find('first', array('conditions' => $conditions, 'order' => array('Menu.position' => 'DESC')));
        return $menu;
    }

    function getNextMenu($position) {

        $conditions = array(
            'Menu.position >' => $position
        );
        $menu = $this->find('first', array('conditions' => $conditions, 'order' => array('Menu.position' => 'ASC')));
        return $menu;
    }

    function hasChild($id) {
        $conditions = array(
            'Menu.parent_id' => $id
        );
        $menus = $this->find('all', array('conditions' => $conditions));
        if (!empty($menus)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
