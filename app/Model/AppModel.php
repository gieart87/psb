<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses("Sanitize", "Utility");

class AppModel extends Model {

//    function find($conditions = null, $fields = array(), $order = null, $recursive = null) {
//        $doQuery = true;
//        // check if we want the cache
//        if (!empty($fields['cache'])) {
//            $cacheConfig = null;
//            // check if we have specified a custom config, e.g. different expiry time
//            if (!empty($fields['cacheConfig']))
//                $cacheConfig = $fields['cacheConfig'];
//
//            $cacheName = $this->name . '-' . $fields['cache'];
//            // if so, check if the cache exists
//            if (($data = Cache::read($cacheName, $cacheConfig)) === false) {
//                $data = parent::find($conditions, $fields, $order, $recursive);
//                Cache::write($cacheName, $data, $cacheConfig);
//            }
//            $doQuery = false;
//        }
//        if ($doQuery)
//            $data = parent::find($conditions, $fields, $order, $recursive);
//
//        return $data;
//    }

    /**
     * Unbinds validation rules and optionally sets the remaining rules to required.
     * 
     * @param string $type 'Remove' = removes $fields from $this->validate
     *                       'Keep' = removes everything EXCEPT $fields from $this->validate
     * @param array $fields
     * @param bool $require Whether to set 'required'=>true on remaining fields after unbind
     * @return null
     * @access public
     */
    function unbindValidation($type, $fields, $require = false) {
        if ($type === 'remove') {
            $this->validate = array_diff_key($this->validate, array_flip($fields));
        } else
        if ($type === 'keep') {
            $this->validate = array_intersect_key($this->validate, array_flip($fields));
        }

        if ($require === true) {
            foreach ($this->validate as $field => $rules) {
                if (is_array($rules)) {
                    $rule = key($rules);

                    $this->validate[$field][$rule]['required'] = true;
                } else {
                    $ruleName = (ctype_alpha($rules)) ? $rules : 'required';

                    $this->validate[$field] = array($ruleName => array('rule' => $rules, 'required' => true));
                }
            }
        }
    }

    function generateRandom($length) {
        $random = "";
        srand((double) microtime() * 1000000);
        $strset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $strset .= "abcdefghijklmnopqrstuvwxyz";
        $strset .= "1234567890";
        // Add the special characters to $char_list if needed

        for ($i = 0; $i < $length; $i++) {
            $random .= substr($strset, (rand() % (strlen($strset))), 1);
        }
        return $random;
    }

    function checkUnique($data, $fields) {
        if (!is_array($fields)) {
            $fields = array($fields);
        }
        foreach ($fields as $key) {
            $tmp[$key] = $this->data[$this->name][$key];
        }
        if (isset($this->data[$this->name][$this->primaryKey])) {
            $tmp[$this->primaryKey] = "<>" . $this->data[$this->name][$this->primaryKey];
        }
        return $this->isUnique($tmp, false);
    }

    function getSetting($key) {
        $setting = ClassRegistry::init('Setting')->findByKey($key);
        return $setting['Setting']['value'];
    }

    function cleanHtml($string) {
        $cleanHtml = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $string);
        $cleanHtml = preg_replace("/&#?[a-z0-9]{2,8};/i", "", $cleanHtml);
        $cleanHtml = preg_replace('/ .*".*"/', '', $cleanHtml);
        $cleanHtml = strip_tags($cleanHtml, '<p><br/><a><table><img><ul><li><strong><ol>');
        return $cleanHtml;
    }

    /**
     * menghitung common datetime 
     * @access public
     * 
     * @param $datetime datetime (YYYY-mm-dd H:i:s)
     * 
     * @return String CommontDateTime (ex 2 minutes ago)
     */
    public function getElapsedTimeAgo($datetime) {
        if (empty($datetime)) {
            $datetime = date('Y-m-d H:i:s');
        }

        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        $now = time();
        $unix_date = strtotime($datetime);

        // check validity of date
        if (empty($unix_date)) {
            return "Invalid Unix date system";
        }

        // is it future date or past date
        if ($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = "ago";
        } else {
            $difference = $unix_date - $now;
            $tense = "from now";
        }

        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if ($difference != 1) {
            $periods[$j].= "s";
        }

        return "$difference $periods[$j] {$tense}";
    }

    function urlTitle($str, $separator = 'dash', $lowercase = TRUE) {
        if ($separator == 'dash') {
            $search = '_';
            $replace = '-';
        } else {
            $search = '-';
            $replace = '_';
        }

        $trans = array(
            '&\#\d+?;' => '',
            '&\S+?;' => '',
            '\s+' => $replace,
            '[^a-z0-9\-\._]' => '',
            $replace . '+' => $replace,
            $replace . '$' => $replace,
            '^' . $replace => $replace,
            '\.+$' => ''
        );

        $str = strip_tags($str);

        foreach ($trans as $key => $val) {
            $str = preg_replace("#" . $key . "#i", $val, $str);
        }

        if ($lowercase === TRUE) {
            $str = strtolower($str);
        }

        return trim(stripslashes($str));
    }

}

?>
