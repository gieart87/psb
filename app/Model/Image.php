<?php

App::uses('AppModel', 'Model');

/**
 * Image Model
 *
 */
class Image extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'type' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'key' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'description' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    public $belongsTo = array(
        'Photo' => array(
            'conditions' => array('Image.type' => 'Photo'),
            'foreignKey' => 'key'
        ),
    );
    public $actsAs = array(
        'Uploader.Attachment' => array(
            'image' => array(
                'name' => 'formatFileName', // Name of the function to use to format filenames
                'baseDir' => '', // See UploaderComponent::$baseDir
                'uploadDir' => 'img/images/', // See UploaderComponent::$uploadDir
                'dbColumn' => 'large', // The database column name to save the path to
                'importFrom' => '', // Path or URL to import file
                'defaultPath' => '', // Default file path if no upload present
                'maxNameLength' => 30, // Max file name length
                'overwrite' => true, // Overwrite file with same name if it exists
                'stopSave' => true, // Stop the model save() if upload fails
                'transforms' => array(
//                    array('method' => 'scale', 'percent' => .3, 'dbColumn' => 'path_scaled'),
                //array('method' => 'resize', 'width' => 600, 'height' => 250, 'dbColumn' => 'large'), // Removes original image and uses this one instead
                //array('method' => 'resize', 'width' => 200, 'height' => 200, 'dbColumn' => 'medium'),
                // array('method' => 'resize', 'width' => 100, 'height' => 100, 'dbColumn' => 'small')
                ), // What transformations to do on images: scale, resize, etc
                's3' => array(
                    'accessKey' => '',
                    'secretKey' => '',
                    'ssl' => true,
                    'bucket' => '',
                    'path' => ''
                ), // Array of Amazon S3 settings
                'metaColumns' => array(// Mapping of meta data to database fields
                    'ext' => '',
                    'type' => '',
                    'size' => '',
                    'group' => '',
                    'width' => '',
                    'height' => '',
                    'filesize' => ''
                )
            )
        )
    );

    public function afterFind($results, $primary = false) {
        foreach ($results as $key => $val) {
            $results[$key]['Image']['large'] = str_replace('/img/', '', $val['Image']['large']);
        }
        return $results;
    }

}
