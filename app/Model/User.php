<?php

App::uses('AppModel', 'Model');

/**
 * User Model
 *
 */
class User extends AppModel {

    public $status = array(
        0 => 'Pending',
        1 => 'Active',
        2 => 'Bloked',
        3 => 'Deactivated'
    );
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'name is required',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            )
        ),
        'username' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'email is required',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'email' => array(
                'rule' => array('email', true),
                'message' => 'invalid email'
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'email account has already taken'
            )
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'password is required',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'identicalFieldValues' => array(
                'rule' => array('identicalFieldValues', 'confirm_password'),
                'message' => 'password confirm doest not match'
            )
        ),
//        'avatar' => array(
//            'extension' => array(
//                'rule' => array('extension', array('jpeg', 'jpg', 'gif', 'png')),
//                'message' => 'You must supply a GIF, PNG, or JPG file.',
//                'required' => false,
//                'on' => 'update'
//            ),
//            'extension2' => array(
//                'rule' => array('extension', array('jpeg', 'jpg', 'gif', 'png')),
//                'message' => 'You must supply a GIF, PNG, or JPG file.',
//                'required' => true,
//                'on' => 'create'
//            ),
//        )
    );

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'username';
    var $hasMany = array(
//        'Page' => array(
//            'className' => 'Page',
//            'foreignKey' => 'user_id',
//            'dependent' => true,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        ),
//        'SynchronizeToken' => array(
//            'className' => 'SynchronizeToken',
//            'foreignKey' => 'user_id',
//            'dependent' => true,
//            'conditions' => '',
//            'fields' => '',
//            'order' => '',
//            'limit' => '',
//            'offset' => '',
//            'exclusive' => '',
//            'finderQuery' => '',
//            'counterQuery' => ''
//        )
    );

    function identicalFieldValues($field = array(), $compare_field = null) {
        foreach ($field as $key => $value) {
            $v1 = $value;
            $v2 = $this->data[$this->name][$compare_field];

            if ($v1 !== md5($v2)) {
                return FALSE;
            } else {

                continue;
            }
        }
        return TRUE;
    }

}
