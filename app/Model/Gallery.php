<?php

App::uses('AppModel', 'Model');

/**
 * Gallery Model
 *
 * @property Type $Type
 * @property Book $Book
 * @property Magazine $Magazine
 * @property Newspaper $Newspaper
 * @property Tabloid $Tabloid
 * @property ZPosDetail $ZPosDetail
 * @property ZPublishing $ZPublishing
 */
class Gallery extends AppModel {

    public $status = array(
        0 => 'Draft',
        1 => 'Published',
    );

    /**
     * Validation rules
     *
     * @var array
     * 
     * 
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'Kategori sudah ada',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'permalink' => array(
            'notempty' => array(
                'rule' => array('notempty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'isUnique' => array(
                'rule' => array('isUnique'),
                'message' => 'This permalink already exist'
            )
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $hasMany = array(
        'Photo' => array(
            'className' => 'Photo',
            'foreignKey' => 'gallery_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        $this->data['Gallery']['permalink'] = $this->urlTitle($this->data['Gallery']['name']);
        $this->data['Gallery']['permalink_en'] = $this->urlTitle($this->data['Gallery']['name_en']);
        $this->data['Gallery']['description'] = $this->cleanHtml($this->data['Gallery']['description']);
    }

}
