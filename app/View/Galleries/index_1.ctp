<div class="one-col">

    <div class="primary">
        <h2 class="title"><?php echo $title; ?></h2>
        <div class="post-detail clearfix">
            <?php if (!empty($photos)): ?>
                <ul class="gallery clearfix">
                    <?php foreach ($photos as $photo): ?>
                        <?php $image = $this->General->getSingleImage('Photo', $photo['Photo']['id']); ?>
                        <li>
                            <a href="<?php echo Router::url('/') . 'img/' . $image['Image']['large']; ?>" class="lightbox" title="<?php echo $image['Image']['description']; ?>">
        <!--                        <img class="thumb-small" src="images/gale-img1.jpg" alt="" />-->
                                <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=100&w=100'); ?>

                            </a>
                        </li>
                    <?php endforeach; ?>


                </ul>
            <?php endif; ?>
        </div>

        <?php if (!empty($galleries)): ?>

            <h3>Galeri Lainya</h3>
            <div class="row20">
                <ul class="gallery clearfix">
                    <?php foreach ($galleries as $gallery): ?>
                        <?php if (!empty($gallery['Photo'])): ?>
                            <?php $photo = $this->General->getSingleImage('Photo', $gallery['Photo'][0]['id']); ?>

                            <li>
                                <a href="<?php echo $this->Html->url(array('controller' => 'galleries', 'action' => 'index', $gallery['Gallery']['permalink'])) ?>">
            <!--                                    <img class="thumb-small" src="images/gale-img4.jpg" alt="" />-->
                                    <?php echo $this->Html->image('timthumb.php?src=' . $photo['Image']['large'] . '&h=100&w=100'); ?>
                                </a>
                                <a href="<?php echo $this->Html->url(array('controller' => 'galleries', 'action' => 'index', $gallery['Gallery']['permalink'])) ?>">
                                    <?php echo $gallery['Gallery']['name'] ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
        <div class="fb-like" data-href="<?php echo $mainUrl?><?php echo $this->Html->url(array('controller' => 'galleries', 'action' => 'index')) ?>" data-send="true" data-width="700" data-show-faces="true"></div>
        <div class="fb-comments" data-href="<?php echo $mainUrl?><?php echo $this->Html->url(array('controller' => 'galleries', 'action' => 'index')) ?>" data-num-posts="5" data-width="700"></div>
        <br/>
    </div>

</div> <!-- two-col -->
<?php echo $this->element('public/right'); ?>