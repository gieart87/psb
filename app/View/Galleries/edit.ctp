<div class="categories form">
<?php echo $this->Form->create('Category');?>
	<fieldset>
		<legend><?php echo __('Edit Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('status');
		echo $this->Form->input('created_date');
		echo $this->Form->input('last_edit');
		echo $this->Form->input('type');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Category.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Category.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Categories'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Books'), array('controller' => 'books', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Book'), array('controller' => 'books', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Magazines'), array('controller' => 'magazines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Magazine'), array('controller' => 'magazines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Newspapers'), array('controller' => 'newspapers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Newspaper'), array('controller' => 'newspapers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pos Details'), array('controller' => 'pos_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pos Detail'), array('controller' => 'pos_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Publishings'), array('controller' => 'publishings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Publishing'), array('controller' => 'publishings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tabloids'), array('controller' => 'tabloids', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tabloid'), array('controller' => 'tabloids', 'action' => 'add')); ?> </li>
	</ul>
</div>
