
<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left"><?php echo $title ?></h1>
        <ul class="pull-right breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">Gallery</li>
        </ul>
    </div>
</div>

<!-- Page Detail -->
<div class="container content">   
    <div class="row"> 
        <div class="col-md-9">
            <div class="row">
                <?php if (!empty($photos)): ?>

                    <?php foreach ($photos as $photo): ?>
                        <div class="col-md-4">
                            <div class="view view-tenth">
                                <?php if (!empty($photo) && file_exists('img/' . $photo['Image']['large'])): ?>
                                    <?php echo $this->Html->image('timthumb.php?src=' . $photo['Image']['large'] . '&h=200&w=250', array('class' => 'img-responsive')); ?>
                                <?php else: ?>
                                    <img src="http://placehold.it/250x200" class="img-responsive"/>
                                <?php endif; ?>
                                <div class="mask">
                                    <h2><?php echo $photo['Photo']['name'] ?></h2>
                                    <a href="<?php echo $this->Html->url(array('controller' => 'photos', 'action' => 'detail', $photo['Photo']['id'])); ?>" class="info">Read More</a>
                                </div>                
                            </div>
                        </div>
                    <?php endforeach; ?>

                <?php endif; ?>
            </div>
        </div>
        <?php echo $this->element('public/right_sidebar'); ?>
    </div><!--/row-->
</div>

