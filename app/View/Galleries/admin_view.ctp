
<div class="galleries view">
<h2><?php  echo __('Gallery');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($gallery['Gallery']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($gallery['Gallery']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Permalink'); ?></dt>
		<dd>
			<?php echo h($gallery['Gallery']['permalink']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($gallery['Gallery']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($gallery['Gallery']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Edit Category'), array('action' => 'edit', $category['Category']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Category'), array('action' => 'delete', $category['Category']['id']), null, __('Are you sure you want to delete # %s?', $category['Category']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Books'), array('controller' => 'books', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Book'), array('controller' => 'books', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Magazines'), array('controller' => 'magazines', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Magazine'), array('controller' => 'magazines', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Newspapers'), array('controller' => 'newspapers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Newspaper'), array('controller' => 'newspapers', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Pos Details'), array('controller' => 'pos_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pos Detail'), array('controller' => 'pos_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Publishings'), array('controller' => 'publishings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Publishing'), array('controller' => 'publishings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tabloids'), array('controller' => 'tabloids', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tabloid'), array('controller' => 'tabloids', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Books');?></h3>
	<?php if (!empty($category['Book'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Isbn'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Authors'); ?></th>
		<th><?php echo __('Num Pages'); ?></th>
		<th><?php echo __('Cover'); ?></th>
		<th><?php echo __('File'); ?></th>
		<th><?php echo __('Version'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Publisher Id'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Unlimited'); ?></th>
		<th><?php echo __('Quota'); ?></th>
		<th><?php echo __('Size'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Created Date'); ?></th>
		<th><?php echo __('Last Edit'); ?></th>
		<th><?php echo __('Extension'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($category['Book'] as $book): ?>
		<tr>
			<td><?php echo $book['id'];?></td>
			<td><?php echo $book['isbn'];?></td>
			<td><?php echo $book['title'];?></td>
			<td><?php echo $book['description'];?></td>
			<td><?php echo $book['authors'];?></td>
			<td><?php echo $book['num_pages'];?></td>
			<td><?php echo $book['cover'];?></td>
			<td><?php echo $book['file'];?></td>
			<td><?php echo $book['version'];?></td>
			<td><?php echo $book['price'];?></td>
			<td><?php echo $book['publisher_id'];?></td>
			<td><?php echo $book['category_id'];?></td>
			<td><?php echo $book['unlimited'];?></td>
			<td><?php echo $book['quota'];?></td>
			<td><?php echo $book['size'];?></td>
			<td><?php echo $book['status'];?></td>
			<td><?php echo $book['created_date'];?></td>
			<td><?php echo $book['last_edit'];?></td>
			<td><?php echo $book['extension'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'books', 'action' => 'view', $book['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'books', 'action' => 'edit', $book['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'books', 'action' => 'delete', $book['id']), null, __('Are you sure you want to delete # %s?', $book['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Book'), array('controller' => 'books', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Magazines');?></h3>
	<?php if (!empty($category['Magazine'])):?>
        <ul>
		<li><?php echo $this->Html->link(__('Edit Gallery'), array('action' => 'edit', $gallery['Gallery']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Gallery'), array('action' => 'delete', $gallery['Gallery']['id']), null, __('Are you sure you want to delete # %s?', $gallery['Gallery']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Galleries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Gallery'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Photos'), array('controller' => 'photos', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Photo'), array('controller' => 'photos', 'action' => 'add')); ?> </li>
	</ul>
        <?php endif;?>
</div>
<div class="related">
	<h3><?php echo __('Related Photos');?></h3>
	<?php if (!empty($gallery['Photo'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Issn'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Publisher Id'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($category['Magazine'] as $magazine): ?>
		<tr>
			<td><?php echo $magazine['id'];?></td>
			<td><?php echo $magazine['name'];?></td>
			<td><?php echo $magazine['issn'];?></td>
			<td><?php echo $magazine['category_id'];?></td>
			<td><?php echo $magazine['publisher_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'magazines', 'action' => 'view', $magazine['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'magazines', 'action' => 'edit', $magazine['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'magazines', 'action' => 'delete', $magazine['id']), null, __('Are you sure you want to delete # %s?', $magazine['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Magazine'), array('controller' => 'magazines', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Newspapers');?></h3>
	<?php if (!empty($category['Newspaper'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Issn'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Publisher Id'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($category['Newspaper'] as $newspaper): ?>
		<tr>
			<td><?php echo $newspaper['id'];?></td>
			<td><?php echo $newspaper['name'];?></td>
			<td><?php echo $newspaper['issn'];?></td>
			<td><?php echo $newspaper['category_id'];?></td>
			<td><?php echo $newspaper['publisher_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'newspapers', 'action' => 'view', $newspaper['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'newspapers', 'action' => 'edit', $newspaper['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'newspapers', 'action' => 'delete', $newspaper['id']), null, __('Are you sure you want to delete # %s?', $newspaper['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Newspaper'), array('controller' => 'newspapers', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Pos Details');?></h3>
	<?php if (!empty($category['PosDetail'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Pos Id'); ?></th>
		<th><?php echo __('Book Id'); ?></th>
		<th><?php echo __('Book Title'); ?></th>
		<th><?php echo __('Book Authors'); ?></th>
		<th><?php echo __('Book Price'); ?></th>
		<th><?php echo __('Publisher Id'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Packet Type'); ?></th>
		<th><?php echo __('Sub Total'); ?></th>
		<th><?php echo __('Created Date'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Last Edit'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($category['PosDetail'] as $posDetail): ?>
		<tr>
			<td><?php echo $posDetail['id'];?></td>
			<td><?php echo $posDetail['pos_id'];?></td>
			<td><?php echo $posDetail['book_id'];?></td>
			<td><?php echo $posDetail['book_title'];?></td>
			<td><?php echo $posDetail['book_authors'];?></td>
			<td><?php echo $posDetail['book_price'];?></td>
			<td><?php echo $posDetail['publisher_id'];?></td>
			<td><?php echo $posDetail['category_id'];?></td>
			<td><?php echo $posDetail['packet_type'];?></td>
			<td><?php echo $posDetail['sub_total'];?></td>
			<td><?php echo $posDetail['created_date'];?></td>
			<td><?php echo $posDetail['status'];?></td>
			<td><?php echo $posDetail['last_edit'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'pos_details', 'action' => 'view', $posDetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'pos_details', 'action' => 'edit', $posDetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'pos_details', 'action' => 'delete', $posDetail['id']), null, __('Are you sure you want to delete # %s?', $posDetail['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pos Detail'), array('controller' => 'pos_details', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Publishings');?></h3>
	<?php if (!empty($category['Publishing'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Isbn'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Authors'); ?></th>
		<th><?php echo __('Num Pages'); ?></th>
		<th><?php echo __('Cover'); ?></th>
		<th><?php echo __('File'); ?></th>
		<th><?php echo __('Version'); ?></th>
		<th><?php echo __('Price'); ?></th>
		<th><?php echo __('Publisher Id'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Created Date'); ?></th>
		<th><?php echo __('Last Edit'); ?></th>
		<th><?php echo __('Quota'); ?></th>
		<th><?php echo __('Unlimited'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($category['Publishing'] as $publishing): ?>
		<tr>
			<td><?php echo $publishing['id'];?></td>
			<td><?php echo $publishing['isbn'];?></td>
			<td><?php echo $publishing['title'];?></td>
			<td><?php echo $publishing['description'];?></td>
			<td><?php echo $publishing['authors'];?></td>
			<td><?php echo $publishing['num_pages'];?></td>
			<td><?php echo $publishing['cover'];?></td>
			<td><?php echo $publishing['file'];?></td>
			<td><?php echo $publishing['version'];?></td>
			<td><?php echo $publishing['price'];?></td>
			<td><?php echo $publishing['publisher_id'];?></td>
			<td><?php echo $publishing['category_id'];?></td>
			<td><?php echo $publishing['status'];?></td>
			<td><?php echo $publishing['created_date'];?></td>
			<td><?php echo $publishing['last_edit'];?></td>
			<td><?php echo $publishing['quota'];?></td>
			<td><?php echo $publishing['unlimited'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'publishings', 'action' => 'view', $publishing['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'publishings', 'action' => 'edit', $publishing['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'publishings', 'action' => 'delete', $publishing['id']), null, __('Are you sure you want to delete # %s?', $publishing['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Publishing'), array('controller' => 'publishings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Tabloids');?></h3>
	<?php if (!empty($category['Tabloid'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Issn'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Publisher Id'); ?></th>
		<th><?php echo __('Gallery Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;

		foreach ($category['Tabloid'] as $tabloid): ?>
		<tr>
			<td><?php echo $tabloid['id'];?></td>
			<td><?php echo $tabloid['name'];?></td>
			<td><?php echo $tabloid['issn'];?></td>
			<td><?php echo $tabloid['category_id'];?></td>
			<td><?php echo $tabloid['publisher_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tabloids', 'action' => 'view', $tabloid['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tabloids', 'action' => 'edit', $tabloid['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tabloids', 'action' => 'delete', $tabloid['id']), null, __('Are you sure you want to delete # %s?', $tabloid['id'])); ?>
                        </td>
                </tr>        
            <?php endforeach; ?>
         <?php  foreach ($gallery['Photo'] as $photo): ?>
		<tr>
			<td><?php echo $photo['id'];?></td>
			<td><?php echo $photo['name'];?></td>
			<td><?php echo $photo['gallery_id'];?></td>
			<td><?php echo $photo['user_id'];?></td>
			<td><?php echo $photo['status'];?></td>
			<td><?php echo $photo['created'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'photos', 'action' => 'view', $photo['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'photos', 'action' => 'edit', $photo['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'photos', 'action' => 'delete', $photo['id']), null, __('Are you sure you want to delete # %s?', $photo['id'])); ?>

			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tabloid'), array('controller' => 'tabloids', 'action' => 'add'));?> </li>

			<li><?php echo $this->Html->link(__('New Photo'), array('controller' => 'photos', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
