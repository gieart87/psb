<div class="row">
    <div class="container">
        <div class="row">
            <div class="span3">
                <div class="box">
                    <div class="box-title">
                        <h3>Galeri</h3>
                    </div>
                    <div class="box-content">
                        <?php $galleries = $this->General->getGalleries(); ?>
                        <?php if (!empty($galleries)): ?>
                            <ul class="menu-list">
                                <?php foreach ($galleries as $gallery): ?>
                                    <li><a href="<?php echo $this->Html->url(array('controller' => 'galleries', 'action' => 'index', $gallery['Gallery']['permalink'])) ?>"><?php echo $gallery['Gallery']['name']; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="span9">
                <div class="page-header">
                    <h2><?php echo $title ?></h2>
                </div>
                <?php if (!empty($photos)): ?>
                    <!--Portfolio Items-->
                    <ul class="galleries isotope" style="position: relative; overflow: hidden; height: 552px;">
                        <?php foreach ($photos as $photo): ?>
                            <li class="span5 cat-<?php echo $photo['Gallery']['id'] ?> isotope-item isotope-hidden" style="position: absolute; left: 0px; top: 0px; transform: translate(0px, 5px) scale(0.001); opacity: 0;">
                                <div class="thumbnail">
                                    <a href="<?php echo $photo['Image']['large'] ?>" title="<?php echo $photo['Photo']['name'] ?>" rel="album" class="js-fancybox"><?php echo $this->Html->image('timthumb.php?src=' . $photo['Image']['large'] . '&h=200&w=300'); ?></a>
                                    <h5><?php echo $photo['Photo']['name'] ?></h5>
                                    <!--<p>Description</p>-->
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>



    </div>
</div>