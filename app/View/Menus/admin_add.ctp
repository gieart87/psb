<section id="widget-grid" class="">
    <!-- row -->

    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <div data-widget-custombutton="false" data-widget-editbutton="false" class="jarviswidget" style="" role="widget">
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"> 
                        <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-toggle-btn" href="#" data-original-name="Collapse"><i class="fa fa-minus "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-fullscreen-btn" href="javascript:void(0);" data-original-name="Fullscreen"><i class="fa fa-resize-full "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-delete-btn" href="javascript:void(0);" data-original-name="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a href="javascript:void(0);" class="dropdown-toggle color-box selector" data-toggle="dropdown"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span data-original-name="Green Grass" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-green" class="bg-color-green"></span></li><li><span data-original-name="Dark Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenDark" class="bg-color-greenDark"></span></li><li><span data-original-name="Light Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenLight" class="bg-color-greenLight"></span></li><li><span data-original-name="Purple" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-purple" class="bg-color-purple"></span></li><li><span data-original-name="Magenta" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-magenta" class="bg-color-magenta"></span></li><li><span data-original-name="Pink" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-pink" class="bg-color-pink"></span></li><li><span data-original-name="Fade Pink" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-pinkDark" class="bg-color-pinkDark"></span></li><li><span data-original-name="Light Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueLight" class="bg-color-blueLight"></span></li><li><span data-original-name="Teal" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-teal" class="bg-color-teal"></span></li><li><span data-original-name="Ocean Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blue" class="bg-color-blue"></span></li><li><span data-original-name="Night Sky" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueDark" class="bg-color-blueDark"></span></li><li><span data-original-name="Night" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-darken" class="bg-color-darken"></span></li><li><span data-original-name="Day Light" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-yellow" class="bg-color-yellow"></span></li><li><span data-original-name="Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orange" class="bg-color-orange"></span></li><li><span data-original-name="Dark Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orangeDark" class="bg-color-orangeDark"></span></li><li><span data-original-name="Red Rose" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-red" class="bg-color-red"></span></li><li><span data-original-name="Light Red" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-redLight" class="bg-color-redLight"></span></li><li><span data-original-name="Purity" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-white" class="bg-color-white"></span></li><li><a data-original-name="Reset widget color to default" data-placement="bottom" rel="tooltip" data-widget-setstyle="" class="jarviswidget-remove-colors" href="javascript:void(0);">Remove</a></li></ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Tambah Menu</h2>				
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="content">

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <!--<form novalidate="novalidate" class="smart-form" id="order-form">-->
                        <?php echo $this->Form->create('Menu', array('action' => 'add', 'class' => 'smart-form', 'id' => 'order-form')); ?>
                        <fieldset>
                            <div class="row">
                                <section class="col col-12 pull-right">
                                    <button type="button" class="btn btn-warning translate"><i class="fa fa-rotate-right"></i> Translate</button>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input">
                                        <?php echo $this->Form->input('name', array('placeholder' => 'name (ID)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">
                                        <?php echo $this->Form->input('name_en', array('placeholder' => 'name (EN)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="input">
                                        <?php echo $this->Form->input('url', array('placeholder' => 'url (ID)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="input">
                                        <?php echo $this->Form->input('url_en', array('placeholder' => 'url (EN)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>

                            <div class="row">
                                <section class="col col-6">
                                    <label class="select"> 										
                                        <?php echo $this->Form->input('parent_id', array('type' => 'select', 'options' => $this->Tree->indentTree($parentMenus), 'empty' => '-- Choose parent --', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="select"> 										
                                        <?php echo $this->Form->input('status', array('options' => $status, 'placeholder' => 'About (EN)', 'label' => FALSE, 'div' => FALSE)); ?>
                                    </label>
                                </section>

                            </div>

                        </fieldset>

                        <footer>
                            <button class="btn btn-primary" type="submit">
                                Save
                            </button>
                            <button class="btn btn-primary" type="button" onclick="history.back();">
                                Back
                            </button>
                        </footer>
                        <?php echo $this->Form->end; ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </article>

    </div>

    <div class="row">
        <article class="col-md-4">
            <div data-widget-custombutton="false" data-widget-editbutton="false" class="jarviswidget" style="" role="widget">
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"> 
                        <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-toggle-btn" href="#" data-original-name="Collapse"><i class="fa fa-minus "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-fullscreen-btn" href="javascript:void(0);" data-original-name="Fullscreen"><i class="fa fa-resize-full "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-delete-btn" href="javascript:void(0);" data-original-name="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a href="javascript:void(0);" class="dropdown-toggle color-box selector" data-toggle="dropdown"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span data-original-name="Green Grass" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-green" class="bg-color-green"></span></li><li><span data-original-name="Dark Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenDark" class="bg-color-greenDark"></span></li><li><span data-original-name="Light Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenLight" class="bg-color-greenLight"></span></li><li><span data-original-name="Purple" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-purple" class="bg-color-purple"></span></li><li><span data-original-name="Magenta" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-magenta" class="bg-color-magenta"></span></li><li><span data-original-name="Pink" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-pink" class="bg-color-pink"></span></li><li><span data-original-name="Fade Pink" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-pinkDark" class="bg-color-pinkDark"></span></li><li><span data-original-name="Light Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueLight" class="bg-color-blueLight"></span></li><li><span data-original-name="Teal" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-teal" class="bg-color-teal"></span></li><li><span data-original-name="Ocean Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blue" class="bg-color-blue"></span></li><li><span data-original-name="Night Sky" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueDark" class="bg-color-blueDark"></span></li><li><span data-original-name="Night" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-darken" class="bg-color-darken"></span></li><li><span data-original-name="Day Light" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-yellow" class="bg-color-yellow"></span></li><li><span data-original-name="Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orange" class="bg-color-orange"></span></li><li><span data-original-name="Dark Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orangeDark" class="bg-color-orangeDark"></span></li><li><span data-original-name="Red Rose" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-red" class="bg-color-red"></span></li><li><span data-original-name="Light Red" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-redLight" class="bg-color-redLight"></span></li><li><span data-original-name="Purity" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-white" class="bg-color-white"></span></li><li><a data-original-name="Reset widget color to default" data-placement="bottom" rel="tooltip" data-widget-setstyle="" class="jarviswidget-remove-colors" href="javascript:void(0);">Remove</a></li></ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Pages</h2>				
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                        <?php if (!empty($pages)): ?>
                            <table class="table table-striped" id="list-pages">
                                <thead>
                                <th>Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($pages as $page): ?>
                                        <tr>
                                            <td><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'edit', $page['Page']['id'])); ?>"><?php echo $page['Page']['title']; ?></td>
                                            <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="<?php echo $page['Page']['title'] ?>" menu_url="pages/read/<?php echo $page['Page']['permalink'] ?>" menu_url_en="pages/read/<?php echo $page['Page']['permalink_en'] ?>"><i class="icon-plus"></i> add</button></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </article>

        <article class="col-md-4">
            <div data-widget-custombutton="false" data-widget-editbutton="false" class="jarviswidget" style="" role="widget">
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"> 
                        <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-toggle-btn" href="#" data-original-name="Collapse"><i class="fa fa-minus "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-fullscreen-btn" href="javascript:void(0);" data-original-name="Fullscreen"><i class="fa fa-resize-full "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-delete-btn" href="javascript:void(0);" data-original-name="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a href="javascript:void(0);" class="dropdown-toggle color-box selector" data-toggle="dropdown"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span data-original-name="Green Grass" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-green" class="bg-color-green"></span></li><li><span data-original-name="Dark Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenDark" class="bg-color-greenDark"></span></li><li><span data-original-name="Light Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenLight" class="bg-color-greenLight"></span></li><li><span data-original-name="Purple" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-purple" class="bg-color-purple"></span></li><li><span data-original-name="Magenta" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-magenta" class="bg-color-magenta"></span></li><li><span data-original-name="Pink" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-pink" class="bg-color-pink"></span></li><li><span data-original-name="Fade Pink" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-pinkDark" class="bg-color-pinkDark"></span></li><li><span data-original-name="Light Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueLight" class="bg-color-blueLight"></span></li><li><span data-original-name="Teal" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-teal" class="bg-color-teal"></span></li><li><span data-original-name="Ocean Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blue" class="bg-color-blue"></span></li><li><span data-original-name="Night Sky" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueDark" class="bg-color-blueDark"></span></li><li><span data-original-name="Night" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-darken" class="bg-color-darken"></span></li><li><span data-original-name="Day Light" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-yellow" class="bg-color-yellow"></span></li><li><span data-original-name="Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orange" class="bg-color-orange"></span></li><li><span data-original-name="Dark Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orangeDark" class="bg-color-orangeDark"></span></li><li><span data-original-name="Red Rose" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-red" class="bg-color-red"></span></li><li><span data-original-name="Light Red" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-redLight" class="bg-color-redLight"></span></li><li><span data-original-name="Purity" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-white" class="bg-color-white"></span></li><li><a data-original-name="Reset widget color to default" data-placement="bottom" rel="tooltip" data-widget-setstyle="" class="jarviswidget-remove-colors" href="javascript:void(0);">Remove</a></li></ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Categories</h2>				
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                        <?php if (!empty($categories)): ?>
                            <table class="table table-striped" id="list-categories">
                                <thead>
                                <th>Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($categories as $category): ?>
                                        <tr>
                                            <td><a href="<?php echo $this->Html->url(array('controller' => 'categories', 'action' => 'edit', $category['Category']['id'])); ?>"><?php echo $category['Category']['name']; ?></td>
                                            <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="<?php echo $category['Category']['name'] ?>" menu_url="articles/index/<?php echo $category['Category']['permalink'] ?>" menu_url_en="articles/index/<?php echo $category['Category']['permalink_en'] ?>"><i class="icon-plus"></i> add</button></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </article>
        <article class="col-md-4">
            <div data-widget-custombutton="false" data-widget-editbutton="false" class="jarviswidget" style="" role="widget">
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"> 
                        <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-toggle-btn" href="#" data-original-name="Collapse"><i class="fa fa-minus "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-fullscreen-btn" href="javascript:void(0);" data-original-name="Fullscreen"><i class="fa fa-resize-full "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-delete-btn" href="javascript:void(0);" data-original-name="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a href="javascript:void(0);" class="dropdown-toggle color-box selector" data-toggle="dropdown"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span data-original-name="Green Grass" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-green" class="bg-color-green"></span></li><li><span data-original-name="Dark Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenDark" class="bg-color-greenDark"></span></li><li><span data-original-name="Light Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenLight" class="bg-color-greenLight"></span></li><li><span data-original-name="Purple" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-purple" class="bg-color-purple"></span></li><li><span data-original-name="Magenta" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-magenta" class="bg-color-magenta"></span></li><li><span data-original-name="Pink" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-pink" class="bg-color-pink"></span></li><li><span data-original-name="Fade Pink" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-pinkDark" class="bg-color-pinkDark"></span></li><li><span data-original-name="Light Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueLight" class="bg-color-blueLight"></span></li><li><span data-original-name="Teal" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-teal" class="bg-color-teal"></span></li><li><span data-original-name="Ocean Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blue" class="bg-color-blue"></span></li><li><span data-original-name="Night Sky" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueDark" class="bg-color-blueDark"></span></li><li><span data-original-name="Night" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-darken" class="bg-color-darken"></span></li><li><span data-original-name="Day Light" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-yellow" class="bg-color-yellow"></span></li><li><span data-original-name="Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orange" class="bg-color-orange"></span></li><li><span data-original-name="Dark Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orangeDark" class="bg-color-orangeDark"></span></li><li><span data-original-name="Red Rose" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-red" class="bg-color-red"></span></li><li><span data-original-name="Light Red" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-redLight" class="bg-color-redLight"></span></li><li><span data-original-name="Purity" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-white" class="bg-color-white"></span></li><li><a data-original-name="Reset widget color to default" data-placement="bottom" rel="tooltip" data-widget-setstyle="" class="jarviswidget-remove-colors" href="javascript:void(0);">Remove</a></li></ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Document Categories</h2>				
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                        <?php if (!empty($documentCategories)): ?>
                            <table class="table table-striped" id="list-document-categories">
                                <thead>
                                <th>Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($documentCategories as $category): ?>
                                        <tr>
                                            <td><a href="<?php echo $this->Html->url(array('controller' => 'document_categories', 'action' => 'edit', $category['DocumentCategory']['id'])); ?>"><?php echo $category['DocumentCategory']['name']; ?></td>
                                            <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="<?php echo $category['DocumentCategory']['name'] ?>" menu_url="documents/category/<?php echo $category['DocumentCategory']['id'] ?>" menu_url_en="documents/category/<?php echo $category['DocumentCategory']['id'] ?>"><i class="icon-plus"></i> add</button></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </article>
    </div>
    <div class="row">
        <article class="col-md-4">
            <div data-widget-custombutton="false" data-widget-editbutton="false" class="jarviswidget" style="" role="widget">
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"> 
                        <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-toggle-btn" href="#" data-original-name="Collapse"><i class="fa fa-minus "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-fullscreen-btn" href="javascript:void(0);" data-original-name="Fullscreen"><i class="fa fa-resize-full "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-delete-btn" href="javascript:void(0);" data-original-name="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a href="javascript:void(0);" class="dropdown-toggle color-box selector" data-toggle="dropdown"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span data-original-name="Green Grass" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-green" class="bg-color-green"></span></li><li><span data-original-name="Dark Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenDark" class="bg-color-greenDark"></span></li><li><span data-original-name="Light Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenLight" class="bg-color-greenLight"></span></li><li><span data-original-name="Purple" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-purple" class="bg-color-purple"></span></li><li><span data-original-name="Magenta" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-magenta" class="bg-color-magenta"></span></li><li><span data-original-name="Pink" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-pink" class="bg-color-pink"></span></li><li><span data-original-name="Fade Pink" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-pinkDark" class="bg-color-pinkDark"></span></li><li><span data-original-name="Light Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueLight" class="bg-color-blueLight"></span></li><li><span data-original-name="Teal" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-teal" class="bg-color-teal"></span></li><li><span data-original-name="Ocean Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blue" class="bg-color-blue"></span></li><li><span data-original-name="Night Sky" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueDark" class="bg-color-blueDark"></span></li><li><span data-original-name="Night" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-darken" class="bg-color-darken"></span></li><li><span data-original-name="Day Light" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-yellow" class="bg-color-yellow"></span></li><li><span data-original-name="Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orange" class="bg-color-orange"></span></li><li><span data-original-name="Dark Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orangeDark" class="bg-color-orangeDark"></span></li><li><span data-original-name="Red Rose" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-red" class="bg-color-red"></span></li><li><span data-original-name="Light Red" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-redLight" class="bg-color-redLight"></span></li><li><span data-original-name="Purity" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-white" class="bg-color-white"></span></li><li><a data-original-name="Reset widget color to default" data-placement="bottom" rel="tooltip" data-widget-setstyle="" class="jarviswidget-remove-colors" href="javascript:void(0);">Remove</a></li></ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Modules</h2>				
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>

                        <table class="table table-striped" id="list-document-categories">
                            <thead>
                            <th>Name</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'galleries', 'action' => 'index')); ?>">Galleries</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Galeri Foto" menu_url="galleries" menu_url_en="galleries"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'complaints', 'action' => 'index')); ?>">Pengaduan</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Pengaduan" menu_url="pages/complaint" menu_url_en="pages/complaint"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'index')); ?>">Indeks Kepuasan Masyarakan</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Indeks Kepuasan Masyarakat" menu_url="questions" menu_url_en="questions"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'head_profiles', 'action' => 'index')); ?>">Profile Kepala dari Masa ke Masa</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Profile Kepala dari Masa ke Masa" menu_url="head_profiles" menu_url_en="head_profiles"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'structures', 'action' => 'index')); ?>">Struktur Organisasi</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Struktur Organisasi Kantor Imigrasi" menu_url="pages/structure" menu_url_en="pages/structure"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'index')); ?>">Agenda</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Agenda" menu_url="full_calendar" menu_url_en="full_calendar"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'official_profiles', 'action' => 'index')); ?>">Profil Pejabat Struktural</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Profil Pejabat Struktural" menu_url="official_profiles/structural" menu_url_en="official_profiles/structural"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'official_profiles', 'action' => 'index')); ?>">Profil Pejabat Imigrasi</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Profil Pejabat Imigrasi" menu_url="official_profiles/imigrasi" menu_url_en="official_profiles/imigrasi"><i class="icon-plus"></i> add</button></td>
                                </tr>
                                <tr>
                                    <td><a href="<?php echo $this->Html->url(array('controller' => 'costs', 'action' => 'index')); ?>">Biaya Keimigrasian</td>
                                    <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="Biaya Keimigrasian" menu_url="costs/index" menu_url_en="costs/index"><i class="icon-plus"></i> add</button></td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </article>
        <article class="col-md-4">
            <div data-widget-custombutton="false" data-widget-editbutton="false" class="jarviswidget" style="" role="widget">
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"> 
                        <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-toggle-btn" href="#" data-original-name="Collapse"><i class="fa fa-minus "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-fullscreen-btn" href="javascript:void(0);" data-original-name="Fullscreen"><i class="fa fa-resize-full "></i></a> <a data-placement="bottom" name="" rel="tooltip" class="button-icon jarviswidget-delete-btn" href="javascript:void(0);" data-original-name="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a href="javascript:void(0);" class="dropdown-toggle color-box selector" data-toggle="dropdown"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span data-original-name="Green Grass" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-green" class="bg-color-green"></span></li><li><span data-original-name="Dark Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenDark" class="bg-color-greenDark"></span></li><li><span data-original-name="Light Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenLight" class="bg-color-greenLight"></span></li><li><span data-original-name="Purple" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-purple" class="bg-color-purple"></span></li><li><span data-original-name="Magenta" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-magenta" class="bg-color-magenta"></span></li><li><span data-original-name="Pink" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-pink" class="bg-color-pink"></span></li><li><span data-original-name="Fade Pink" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-pinkDark" class="bg-color-pinkDark"></span></li><li><span data-original-name="Light Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueLight" class="bg-color-blueLight"></span></li><li><span data-original-name="Teal" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-teal" class="bg-color-teal"></span></li><li><span data-original-name="Ocean Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blue" class="bg-color-blue"></span></li><li><span data-original-name="Night Sky" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueDark" class="bg-color-blueDark"></span></li><li><span data-original-name="Night" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-darken" class="bg-color-darken"></span></li><li><span data-original-name="Day Light" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-yellow" class="bg-color-yellow"></span></li><li><span data-original-name="Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orange" class="bg-color-orange"></span></li><li><span data-original-name="Dark Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orangeDark" class="bg-color-orangeDark"></span></li><li><span data-original-name="Red Rose" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-red" class="bg-color-red"></span></li><li><span data-original-name="Light Red" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-redLight" class="bg-color-redLight"></span></li><li><span data-original-name="Purity" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-white" class="bg-color-white"></span></li><li><a data-original-name="Reset widget color to default" data-placement="bottom" rel="tooltip" data-widget-setstyle="" class="jarviswidget-remove-colors" href="javascript:void(0);">Remove</a></li></ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Flow Immigration Services</h2>				
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="content">

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>
                        <?php if (!empty($steps)): ?>
                            <table class="table table-striped" id="list-flow">
                                <thead>
                                <th>Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($steps as $step): ?>
                                        <tr>
                                            <td><a href="<?php echo $this->Html->url(array('controller' => 'steps', 'action' => 'edit', $step['Step']['id'])); ?>"><?php echo $step['Step']['name']; ?></td>
                                            <td width="15%"><button class="button btn-s-small btn-success add-menu" menu_name="<?php echo $step['Step']['name'] ?>" menu_url="step_details/index/<?php echo $step['Step']['id'] ?>" menu_url_en="step_details/index/<?php echo $step['Step']['id'] ?>"><i class="icon-plus"></i> add</button></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </article>

    </div>
    <br/>

</section>
<!-- end widget grid -->
<script type="text/javascript">
    $('.translate').click(function() {
        var name = $('#MenuName').val();
        $.ajax({
            type: 'GET',
            url: SERVER + 'langs/translate?text=' + name,
            success: function(response) {
                $('#MenuNameEn').val(response);
            }
        })
    });

    $('.add-menu').click(function() {
        var menuName = $(this).attr('menu_name');
        var menuUrl = $(this).attr('menu_url');
        var menuUrlEn = $(this).attr('menu_url_en');
        $('#MenuName').val(menuName);
        $('#MenuUrl').val(menuUrl);
        $('#MenuUrlEn').val(menuUrlEn);
    });
</script>


<!-- end widget grid -->

<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/jquery.dataTables-cust.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ColReorder.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/FixedColumns.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ColVis.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ZeroClipboard.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/media/js/TableTools.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/DT_bootstrap.js"></script>


<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function() {

        pageSetUp();

        /*
         * BASIC
         */
        $('#list-pages,#list-categories,#list-document-categories,#list-flow').dataTable({
            "sPaginationType": "bootstrap_full"
        });

        /* END BASIC */

        /* Add the events etc before DataTables hides a column */
        $("#datatable_fixed_column thead input").keyup(function() {
            oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
        });

        $("#datatable_fixed_column thead input").each(function(i) {
            this.initVal = this.value;
        });
        $("#datatable_fixed_column thead input").focus(function() {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("#datatable_fixed_column thead input").blur(function(i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = this.initVal;
            }
        });


        var oTable = $('#datatable_fixed_column').dataTable({
            "sDom": "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            //"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "bSortCellsTop": true
        });



        /*
         * COL ORDER
         */
        $('#datatable_col_reorder').dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "R<'dt-top-row'Clf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "fnInitComplete": function(oSettings, json) {
                $('.ColVis_Button').addClass('btn btn-default btn-sm').html('Columns <i class="icon-arrow-down"></i>');
            }
        });

        /* END COL ORDER */

        /* TABLE TOOLS */
        $('#datatable_tabletools').dataTable({
            "sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "oTableTools": {
                "aButtons": ["copy", "print", {
                        "sExtends": "collection",
                        "sButtonText": 'Save <span class="caret" />',
                        "aButtons": ["csv", "xls", "pdf"]
                    }],
                "sSwfPath": "<?php echo Router::url('/'); ?>js/admin/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
            "fnInitComplete": function(oSettings, json) {
                $(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
                    $(this).addClass('btn-sm btn-default');
                });
            }
        });

        /* END TABLE TOOLS */
    })

</script>
