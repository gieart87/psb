<div class="menus view">
    <h2><?php echo __('Menu'); ?></h2>
    <dl>

        <dt><?php echo __('Name'); ?></dt>
        <dd>
            <?php echo h($menu['Menu']['name']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Url'); ?></dt>
        <dd>
            <?php echo h($menu['Menu']['url']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Parent Menu'); ?></dt>
        <dd>
            <?php echo $this->Html->link($menu['ParentMenu']['name'], array('controller' => 'menus', 'action' => 'view', $menu['ParentMenu']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Status'); ?></dt>
        <dd>
            <?php echo h($status[$menu['Menu']['status']]); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Position'); ?></dt>
        <dd>
            <?php echo h($menu['Menu']['position']); ?>
            &nbsp;
        </dd>
    </dl>
    <br/>
    <h3><?php echo __('Child Menus'); ?></h3>
    <?php echo $this->Html->link(__('New Child Menu'), array('controller' => 'menus', 'action' => 'add')); ?> 
    <?php if (!empty($menu['ChildMenu'])): ?>
        <table cellpadding = "0" cellspacing = "0">
            <tr>
                <th><?php echo __('Id'); ?></th>
                <th><?php echo __('Name'); ?></th>
                <th><?php echo __('Url'); ?></th>
                <th><?php echo __('Parent Id'); ?></th>
                <th><?php echo __('Status'); ?></th>
                <th><?php echo __('Position'); ?></th>
                <th class="actions"><?php echo __('Actions'); ?></th>
            </tr>
            <?php
            $i = 0;
            foreach ($menu['ChildMenu'] as $childMenu):
                ?>
                <tr>
                    <td><?php echo $childMenu['id']; ?></td>
                    <td><?php echo $childMenu['name']; ?></td>
                    <td><?php echo $childMenu['url']; ?></td>
                    <td><?php echo $childMenu['parent_id']; ?></td>
                    <td><?php echo $childMenu['status']; ?></td>
                    <td><?php echo $childMenu['position']; ?></td>
                    <td class="actions">
                        <?php echo $this->Html->link(__('View'), array('controller' => 'menus', 'action' => 'view', $childMenu['id'])); ?>
                        <?php echo $this->Html->link(__('Edit'), array('controller' => 'menus', 'action' => 'edit', $childMenu['id'])); ?>
                        <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'menus', 'action' => 'delete', $childMenu['id']), null, __('Are you sure you want to delete # %s?', $childMenu['id'])); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>
