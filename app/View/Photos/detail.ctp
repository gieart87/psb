<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Detail Photo</h1>
        <!--        <ul class="pull-right breadcrumb">
                    <li><a href="index.html">Home</a></li>
                    <li><a href="index.html">Gallery</a></li>
                    <li class="active">Pantai Parangtritis</li>
                </ul>-->
    </div>
</div>

<!-- Page Detail -->
<div class="container content"> 	
    <div class="row portfolio-item margin-top-30 margin-bottom-50"> 
        <!-- Carousel -->
        <div class="col-md-7">
            <?php if (!empty($photo) && file_exists('img/' . $photo['Image']['large'])): ?>
                <?php echo $this->Html->image('timthumb.php?src=' . $photo['Image']['large'] . '&h=400&w=650', array('class' => 'img-responsive')); ?>
            <?php else: ?>
                <img src="http://placehold.it/650x400" class="img-responsive"/>
            <?php endif; ?>
        </div>
        <!-- End Carousel -->
        <!-- Content Info -->        
        <div class="col-md-5">
            <?php echo $photo['Photo']['description']; ?>
            <ul class="list-unstyled">
                <li><i class="fa fa-user color-green"></i> <?php echo $photo['User']['name']; ?></li>
                <li><i class="fa fa-calendar color-green"></i> <?php echo $this->General->humanDate2($photo['Photo']['created']) ?></li>
                <li><i class="fa fa-picture-o color-green"></i> <?php echo $this->Html->link($photo['Gallery']['name'], array('controller' => 'galleries', 'action' => 'index', $photo['Gallery']['permalink'])) ?></li>
            </ul>
        </div>
        <!-- End Content Info -->        
    </div><!--/row-->
</div>