<?php echo $this->Html->script('admin/articles'); ?>
<?php echo $this->General->initMce(); ?>
<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">

                <?php echo $this->element('breadcrumb'); ?>
                <?php echo $this->Form->create('Article', array('type' => 'file')); ?>
                <fieldset>
                    <legend><?php echo __('Add Article'); ?></legend>
                    <?php
                    echo $this->Form->input('title', array('class' => 'required span6'));
                    echo $this->Form->input('body', array('class' => 'required'));
                    echo $this->Form->input('image', array('type' => 'file'));
                    echo $this->Form->input('category_id');
                    ?>
                </fieldset>
                <input type="submit" class="btn btn-primary" value="save">
                <?php echo $this->Form->end(); ?>


            </div> <!-- /container -->

        </div> <!-- /main-inner -->

    </div> <!-- /main -->
</div>


