<div class="row">
    <div class="container">
        <div class="row">
            <section class="pull-left span8" id="page-sidebar">
                <?php //echo $this->element('breadcrumb'); ?>
                <?php if (!empty($articles)): ?>
                    <?php foreach ($articles as $article): ?>
                        <article class="box">
                            <h3 class="post-title"><a href="<?php echo $this->Html->url(array('controller' => 'articles', 'action' => 'read', $article['Article']['permalink'])) ?>"><?php echo $article['Article']['title'] ?></a></h3>
                            <ul class="post-meta">
                                <li><i class="icon-calendar"></i> <?php echo $this->General->humanDate($article['Article']['created']); ?></li>
                                <li><i class="icon-user"></i> <?php $article['User']['username'] ?></li>
                                <li><i class="icon-list-alt"></i> </li>
                            </ul>
                            <div class="post-media">
                                <?php $image = $this->General->getSingleImage('Article', $article['Article']['id']); ?>
                                <?php if (!empty($image)): ?>
                                    <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=300&w=600'); ?>
                                <?php endif; ?>
                            </div>
                            <div class="post-content">
                                <?php echo $this->General->wordLimiter($article['Article']['body'], 20); ?>
                                <p><a class="btn  btn-primary" href="<?php echo $this->Html->url(array('controller' => 'articles', 'action' => 'read', $article['Article']['permalink'])) ?>"><i class="icon-chevron-right icon-white"></i>Continue reading</a></p>
                            </div>
                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>

                <?php echo $this->element('paging') ?>
            </section>

            <?php echo $this->element('public/right_sidebar'); ?>
        </div>
    </div>
</div>