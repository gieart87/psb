<?php echo $this->Html->script('admin/articles'); ?>
<?php echo $this->General->initMce(); ?>
<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">
                <?php echo $this->element('breadcrumb'); ?>
                <?php echo $this->Form->create('Article', array('type' => 'file')); ?>
                <fieldset>
                    <legend><?php echo __('Edit Article'); ?></legend>
                    <?php
                    echo $this->Form->input('id');
                    echo $this->Form->input('title', array('class' => 'required span6'));
                    echo $this->Form->input('body', array('class' => 'required'));
                    $image = $this->General->getSingleImage('Article', $this->request->data['Article']['id']);

                    if (!empty($image)):
                        echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=200&w=200');
                        echo '<br/>';
                        echo $this->Html->link(__('Delete'), array('controller' => 'images', 'action' => 'delete', $image['Image']['id'], $this->request->data['Article']['id'], 'Article', 'edit'), null, __('Are you sure you want to delete # %s?', $image['Image']['id']));
                    endif;
                    echo $this->Form->input('image', array('type' => 'file'));
                    echo $this->Form->input('category_id');
                    ?>
                </fieldset>
                <input type="submit" class="btn btn-primary" value="save">
                <?php echo $this->Form->end(); ?>


            </div> <!-- /container -->

        </div> <!-- /main-inner -->

    </div> <!-- /main -->
</div>