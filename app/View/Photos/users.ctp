<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">

                <?php echo $this->element('breadcrumb'); ?>
                <div class="row-fluid">
                    <div class="span12">
                        <?php echo $this->Html->link(__('+ New Article'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <br/>
                <div class="row-fluid">
                    <div class="span12">

                        <table cellpadding="0" cellspacing="0" border="0" id="table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>User</th>
                                    <th>Created</th>
                                    <th>Modified</th>
                                    <th width="75px">Update</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>User</th>
                                    <th>Created</th>
                                    <th>Modified</th>
                                    <th width="75px">Update</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($articles as $article): ?>
                                    <tr>
                                        <td><?php echo h($article['Article']['id']); ?>&nbsp;</td>
                                        <td><?php echo h($article['Article']['title']); ?>&nbsp;</td>
                                        <td><?php echo h($status[$article['Article']['status']]); ?>&nbsp;</td>
                                        <td><?php echo $article['User']['name'] ?></td>
                                        <td><?php echo h($article['Article']['created']); ?>&nbsp;</td>
                                        <td><?php echo h($article['Article']['modified']); ?>&nbsp;</td>
                                        <td class="actions">

                                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $article['Article']['id'])); ?> | 

                                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $article['Article']['id']), null, __('Are you sure you want to delete # %s?', $article['Article']['id'])); ?>


                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div> 
                </div>

            </div> <!-- /container -->

        </div> <!-- /main-inner -->

    </div> <!-- /main -->
</div>
