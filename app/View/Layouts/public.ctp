<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>SMK N 1 PONJONG</title>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link href="<?php echo Router::url('/'); ?>css/public/global-style.css" rel="stylesheet" type="text/css" media="screen">
        <link href="<?php echo Router::url('/'); ?>css/public/general.css" rel="stylesheet" type="text/css" media="screen">
        <link rel="icon" href="images/favicon.png" type="image/png">

        <!-- JavaScript -->
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/bootstrap/<?php echo Router::url('/'); ?>js/public/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/modernizr.custom.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery.mousewheel-3.0.6.pack.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery.easing.js"></script>

        <!--[if lt IE 9]>
            <script src="<?php echo Router::url('/'); ?>js/public/html5shiv.js"></script>
            <script src="<?php echo Router::url('/'); ?>js/public/respond.min.js"></script>
        <![endif]-->

        <!-- Plugins -->
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/masonry/masonry.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/page-scroller/jquery.ui.totop.min.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/mixitup/jquery.mixitup.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/mixitup/jquery.mixitup.init.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/easy-pie-chart/jquery.easypiechart.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/waypoints/waypoints.min.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/sticky/jquery.sticky.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery.wp.custom.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery.wp.switcher.js"></script>

        <script src="<?php echo Router::url('/'); ?>js/default.js"></script>
        <?php echo $this->General->basePath(); ?>
        <script src="<?php echo Router::url('/'); ?>js/bootstrap.js"></script>


        <script type="text/javascript" src="<?php echo Router::url('/'); ?>assets/public/totemticker/jquery.totemticker.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#audio-video').totemticker({
                    row_height: '60px',
                    next: '#ticker-next',
                    previous: '#ticker-previous',
                    stop: '#stop',
                    start: '#start',
                    mousestop: true,
                    speed: 700, /* Speed of transition animation in milliseconds */
                    interval: 2000, /* Time between change in milliseconds */
                });

                $('#otomotif').totemticker({
                    row_height: '60px',
                    next: '#ticker-next',
                    previous: '#ticker-previous',
                    stop: '#stop',
                    start: '#start',
                    mousestop: true,
                    speed: 700, /* Speed of transition animation in milliseconds */
                    interval: 2000, /* Time between change in milliseconds */
                });

                $('#multimedia').totemticker({
                    row_height: '60px',
                    next: '#ticker-next',
                    previous: '#ticker-previous',
                    stop: '#stop',
                    start: '#start',
                    mousestop: true,
                    speed: 700, /* Speed of transition animation in milliseconds */
                    interval: 2000, /* Time between change in milliseconds */
                });


            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#btReload').click(function() {
                    location.reload(true);
                });    // RELOAD PAGE ON BUTTON CLICK EVENT.

                // SET AUTOMATIC PAGE RELOAD TIME TO 5000 MILISECONDS (5 SECONDS).
                setInterval('refreshPage()', 300000);
            });
            function refreshPage() {
                location.reload();
            }
        </script>


    </head>	

    <body  class="wp-theme-1 wp-theme-3"> 
        <div class="wrapper">
            <header>    
                <div id="navOne" class="navbar navbar-wp" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle navbar-toggle-aside-menu">
                                <i class="fa fa-outdent icon-custom"></i>
                            </button>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="index.html" title="Boomerang | One template. Infinite solutions">
                                SMK N 1 PONJONG GUNUNGKIDUL
                            </a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active"><a href="<?php echo Router::url('/'); ?>">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>

                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </header>
            <?php echo $content_for_layout; ?>
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-9 copyright">
                            2014 © Gie-Art.com All rights reserverd. 
                        </div>
                        <div class="col-lg-3 footer-logo">

                        </div>
                    </div>
                </div>
            </footer></div>        
    </body>
</html>