<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title> SmartAdmin </title>
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Use the correct meta names below for your web application
                 Ref: http://davidbcalhoun.com/2010/viewport-metatag 
                 
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">-->

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Basic Styles -->
        <!--<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">-->
        <!--<link rel="stylesheet" type="text/css" media="screen" href="css/font-awesome.min.css">-->
        <?php
        echo $this->Html->css(array(
            'admin/bootstrap.min',
            'admin/font-awesome.min'
        ));
        ?>

        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
        <!--<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-production.css">-->
        <!--<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-skins.css">-->
        <?php
        echo $this->Html->css(array(
            'admin/smartadmin-production',
            'admin/smartadmin-skins'
        ));
        ?>

        <!-- SmartAdmin RTL Support is under construction
        <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.css"> -->

        <!-- We recommend you use "your_style.css" to override SmartAdmin
             specific styles this will also ensure you retrain your customization with each SmartAdmin update.
        <link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

        <!--Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp--> 
        <!--<link rel="stylesheet" type="text/css" media="screen" href="css/demo.css">-->
        <?php
        echo $this->Html->css(array(
//            'admin/demo',
        ));
        ?>
        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo Router::url('/') ?>img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo Router::url('/') ?>img/favicon/favicon.ico" type="image/x-icon">

        <!-- GOOGLE FONT -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    </head>
    <body class="desktop-detected pace-done smart-style-3 fixed-header fixed-ribbon">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->

        <!-- LOAD JavaScripts -->
        <?php echo $this->element('admin/javascript'); ?>
        <!-- END JavaScripts -->

        <!-- HEADER -->
        <?php echo $this->element('admin/header'); ?>
        <!-- END HEADER -->

        <!-- Left panel : Navigation area -->
        <!-- Note: This width of the aside area can be adjusted through LESS variables -->
        <?php echo $this->element('admin/sidebar') ?>
        <!-- END NAVIGATION -->

        <!-- MAIN PANEL -->
        <div id="main" role="main">

            <!-- RIBBON -->
            <?php echo $this->element('admin/ribbon'); ?>
            <!-- END RIBBON -->

            <!-- MAIN CONTENT -->
            <div id="content">

                <?php echo $content_for_layout; ?>

            </div>
            <!-- END MAIN CONTENT -->

        </div>
        <!-- END MAIN PANEL -->

        <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
        Note: These tiles are completely responsive,
        you can add as many as you like
        -->
        <?php echo $this->element('admin/shortcut'); ?>
        <!-- END SHORTCUT AREA -->


    </body>

</html>
