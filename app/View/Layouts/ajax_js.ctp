<?php
echo $this->Html->script(array(
"jquery/jquery-1.6.2.min",
"jquery/jquery-ui-1.8.2.custom.min",
"public/default",
"public/jquery-1.7.min",
"public/prettify",
"public/bootstrap-modal",
"public/bootstrap-alerts",
"public/bootstrap-twipsy",
"public/bootstrap-popover",
"public/bootstrap-dropdown",
"public/bootstrap-scrollspy",
"public/bootstrap-tabs",
"public/bootstrap-buttons",
"public/sliderproduct",
"public/demo",
"public/slider"
));
echo $this->HelpTablet->basePath();
echo $this->Html->script(array(
"bootstrap",
"public/login"
));



echo $this->Html->css(array(
"public/docs",
"public/prettify",
"public/bootstrap",
"public/general",
"public/fancymoves",
"public/demo"
));
?>
<script>
    $(function () {
        $("a[rel=twipsy]").twipsy({
            live: true
        })
    })
</script>
<script>
    $(function () { prettyPrint() })
</script>
<style type="text/css">
    body {
        padding-top: 60px;
    }
</style>
<?php echo $content_for_layout; ?>
