<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Kantor Imigrasi Yogyakarta</title>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?php echo Router::url('/'); ?>img/public/favicon.ico">
        <!-- CSS Global Compulsory-->
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/bootstrap.css">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/style.css">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/custom.css">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/header1.css">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/responsive.css">       
        <!-- CSS Implementing Plugins -->    
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/font-awesome.css">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/flexslider.css">    	
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/parallax-slider.css">
        <!-- CSS Theme -->    
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/blue.css" id="style_color">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/header1-blue.css" id="style_color-header-1">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/timeline.css">
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>full_calendar/css/fullcalendar.css">
        <!-- Additional CSS -->
        <link rel="stylesheet" href="<?php echo Router::url('/'); ?>css/public/general.css">
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery-1.js"></script>
        <script src="<?php echo Router::url('/'); ?>js/default.js"></script>
        <?php echo $this->General->basePath(); ?>
        <script src="<?php echo Router::url('/'); ?>js/bootstrap.js"></script>




        <!--Start of Zopim Live Chat Script-->
        <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
            window.$zopim || (function(d, s) {
                var z = $zopim = function(c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function(o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute('charset', 'utf-8');
                $.src = '//v2.zopim.com/?25YT7r0lTAeBZTYkYncXAqFrOijApd2d';
                z.t = +new Date;
                $.
                        type = 'text/javascript';
                e.parentNode.insertBefore($, e)
            })(document, 'script');
        </script>
        <!--End of Zopim Live Chat Script-->
    </head>	

    <body class="boxed-layout container animated fadeOut"> 
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "/connect.facebook.net/id_ID/all.js#xfbml=1&appId=375744289189168";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <?php echo $this->element('public/header'); ?>
        <?php
        if (!empty($home) && $home == 1) {
            echo $this->element('public/slider');
        }
        ?>
        <?php echo $this->Session->flash(); ?>
        <?php echo $content_for_layout; ?>
        <!-- End Content Part -->

        <?php echo $this->element('public/footer'); ?>


        <!-- JS Global Compulsory -->			


        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery-migrate-1.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/bootstrap.js"></script> 
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/hover-dropdown.js"></script> 
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/back-to-top.js"></script>
        <!-- JS Implementing Plugins -->           
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery_002.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/jquery.js"></script> 
        <!-- JS Page Level -->           
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/app.js"></script>
        <script type="text/javascript" src="<?php echo Router::url('/'); ?>js/public/index.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                App.init();
                App.initSliders();
                Index.initParallaxSlider();
            });</script>
        <!--[if lt IE 9]>
            <script src="assets/plugins/respond.js"></script>
        <![endif]-->


        <div title="Scroll Back to Top" style="position: fixed; bottom: 5px; right: 5px; opacity: 0; cursor: pointer;" id="topcontrol">
        </div>

        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_shortname = 'imigrasijogja';
            // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function() {
                var s = document.createElement('script');
                s.async = true;
                s.type = 'text/javascript';
                s.src = '/' + disqus_shortname + '.disqus.com/count.js';
                (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
            }());</script>

        <!-- Popup Ads on Landing Page -->
        <?php
        $popupAds = $this->Session->read('popup_ads');
        ?>
        <?php if (empty($popupAds)): ?>
            <?php
            $_SESSION['popup_ads'] = 1;
            ?>
            <script type="text/javascript">
                $(document).ready(function() {

                    var id = '#dialog';
                    //Get the screen height and width
                    var maskHeight = $(document).height();
                    var maskWidth = $(window).width();
                    //Set heigth and width to mask to fill up the whole screen
                    $('#mask').css({'width': maskWidth, 'height': maskHeight});
                    //transition effect		
                    $('#mask').fadeIn(800);
                    $('#mask').fadeTo("slow", 0.8);
                    //Get the window height and width
                    var winH = $(window).height();
                    var winW = $(window).width();
                    //Set the popup window to center
                    $(id).css('top', winH / 2 - $(id).height() / 2 - 50);
                    $(id).css('left', winW / 2 - $(id).width() / 2);
                    //transition effect
                    $(id).fadeIn(500);
                    //if close button is clicked
                    $('.window .close').click(function(e) {
                        //Cancel the link behavior
                        e.preventDefault();
                        $('#mask').hide();
                        $('.window').hide();
                    });
                    //if mask is clicked
                    $('#mask').click(function() {
                        $(this).preventDefault();
                        $(this).hide();
                        $('.window').hide();
                    });
                });

            </script>


            <div id="boxes">
                <div style="top:150px; left: 551.5px; display: none;" id="dialog" class="window">
                    <div align="right" style="font-weight:bold; margin:5px 3px 0 0;"><a href="javascript:void()" class="close"><i class="fa fa-times-circle-o"></i></a></div>

                    <div align="center" style="margin:5px 0 5px 0;">
                        <?php echo $this->Html->image('public/stop_pungutan_liar.jpg'); ?>
                    </div>
                </div>

                <!-- Mask to cover the whole screen -->
                <div style="width: 2000px; height: 2000px; display: none; opacity: 0.7;" id="mask"></div>
            </div>
            <!-- End Popup Ads -->
        <?php endif; ?>
    </body>
</html>