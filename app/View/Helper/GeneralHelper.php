<?php

App::uses('AppHelper', 'View/Helper');
App::uses('Sanitize', 'Utility');

class GeneralHelper extends AppHelper {

    var $name = 'General';
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Js'
    );

    #===========================GENERAL============================================#

    public function basePath() {
        $default = array();
        $default['basePath'] = Router::url('/');    //basePath kita

        $output = $this->Html->scriptBlock('$.extend(Project, ' . $this->Js->object($default) . ');'); // kita write json di default.js
        echo $output;
    }

    public function getSetting($key) {
        $config = ClassRegistry::init('Setting')->findByKey($key);
        if (!empty($config['Setting']['value'])) {
            return $config['Setting']['value'];
        } else {
            return $key;
        }
    }

    public function getYears($start = null) {
        if ($start == null) {
            $start = 2000;
        }
        $years = array();
        for ($i = $start; $i <= date("Y"); $i++) {
            $years[$i] = $i;
        }
        return $years;
    }

    public function getMonths($index = null) {
        $months = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        if ($index == null) {
            return $months;
        } else {
            return $months[$index];
        }
    }

    public function humanDate($datetime) {
        return date("D, d M Y H:i:s", strtotime($datetime));
    }

    public function humanDate2($date) {
        return date("D, d M Y", strtotime($date));
    }

    public function getRangeDates($start, $end) {
        $datediff = abs($end - $start);
        return floor($datediff / (60 * 60 * 24));
    }

    public function initMce() {
        echo '  <script type="text/javascript">
            tinyMCE.init({
                theme : "advanced",
                mode : "textareas",
                theme_advanced_toolbar_location : "top",
                convert_urls : false,
                height: 400,
                width: 400
                

            });
        </script>';
    }

    function urlTitle($str, $separator = 'dash', $lowercase = TRUE) {
        if ($separator == 'dash') {
            $search = '_';
            $replace = '-';
        } else {
            $search = '-';
            $replace = '_';
        }

        $trans = array(
            '&\#\d+?;' => '',
            '&\S+?;' => '',
            '\s+' => $replace,
            '[^a-z0-9\-\._]' => '',
            $replace . '+' => $replace,
            $replace . '$' => $replace,
            '^' . $replace => $replace,
            '\.+$' => ''
        );

        $str = strip_tags($str);

        foreach ($trans as $key => $val) {
            $str = preg_replace("#" . $key . "#i", $val, $str);
        }

        if ($lowercase === TRUE) {
            $str = strtolower($str);
        }

        return trim(stripslashes($str));
    }

    public function wordLimiter($str, $limit = 100, $end_char = '&#8230;') {
        if (trim($str) == '') {
            return $str;
        }

        preg_match('/^\s*+(?:\S++\s*+){1,' . (int) $limit . '}/', $str, $matches);

        if (strlen($str) == strlen($matches[0])) {
            $end_char = '';
        }

        return rtrim($matches[0]) . $end_char;
    }

    public function parsePath($path) {
        $folder = 'images';
        $newPath = $folder . '/' . end(explode('/', $path));

        return $newPath;
    }

    public function getSingleImage($type, $key) {

        $conditions = array(
            'Image.type' => $type,
            'Image.key' => $key
        );
        $image = ClassRegistry::init('Image')->find('first', array('conditions' => $conditions));
        if (!empty($image)) {
            return $image;
        } else {
            return array();
        }
    }

    public function getMultiImage($type) {
        $conditions = array(
            'Image.type' => $type
        );

        $images = ClassRegistry::init('Image')->find('all', array('conditions' => $conditions, 'order' => array('Image.id' => 'DESC')));

        return $images;
    }

    public function getSlides() {

        $conditions = array(
            'Slide.status' => 1
        );

        $slides = ClassRegistry::init('Slide')->find('all', array('conditions' => $conditions, 'order' => array('Slide.position' => 'ASC')));

        return $slides;
    }

    public function getLinks() {
        $conditions = array(
            'Link.status' => 1
        );

        $links = ClassRegistry::init('Link')->find('all', array('conditions' => $conditions, 'order' => array('Link.position' => 'ASC')));

        return $links;
    }

    public function getFirstArticle($categoryId = null) {
        App::import('Model', 'Article');

        $Article = new Article();


        $conditions = array();
        $conditions['joins'] = array(
            array(
                'table' => 'articles_categories',
                'alias' => 'ArticlesCategory',
                'type' => 'INNER',
                'conditions' => array(
                    'ArticlesCategory.article_id = Article.id'
                )
            ),
            array(
                'table' => 'categories',
                'alias' => 'Category',
                'type' => 'INNER',
                'conditions' => array(
                    'Category.id = ArticlesCategory.category_id',
                    'Category.id = ' . $categoryId,
                    'Article.status = 1'
                )
            )
        );
        $articles = ClassRegistry::init('Article')->find('first', $conditions);


        if (!empty($articles['Article'])) {
            return $articles['Article'];
        } else {
            return array();
        }
    }

    public function getLatestArticles($limit = null, $exceptArticleId = null, $categoryId = null) {
        $conditions = array(
            'Article.status' => 1,
            'Article.id !=' => $exceptArticleId,
        );

//        if ($categoryId != null) {
//            $conditions['Category.id'] = $categoryId;
//        }
        $articles = ClassRegistry::init('Article')->find('all', array('conditions' => $conditions, 'limit' => $limit, 'order' => array('Article.id' => 'DESC')));

        $data = array();
        if (!empty($articles)) {

            foreach ($articles as $article) {

                if (!empty($categoryId)) {
                    $categoryIds = array();
                    if (!empty($article['Category'])) {
                        foreach ($article['Category'] as $category) {
                            $categoryIds[] = $category['id'];
                        }
                    }

                    if (in_array($categoryId, $categoryIds)) {
                        $data[] = $article;
                    }
                } else {
                    $data[] = $article;
                }
            }
        }
        return $data;
    }

    public function getPopulerArticles($limit = null) {
        $conditions = array(
            'Article.status' => 1,
            'Category.status' => 1
        );

        $articles = ClassRegistry::init('Article')->find('all', array('conditions' => $conditions, 'limit' => $limit, 'order' => array('Article.hits' => 'DESC')));
        return $articles;
    }

    public function getCategories() {
        $conditions = array(
            'Category.status' => 1
        );

        $categories = ClassRegistry::init('Category')->find('all', array('conditions' => $conditions, 'order' => array('Category.name' => 'ASC')));
        return $categories;
    }

    public function getFrontPage() {
        $conditions = array(
            'Page.id' => 1,
            'Page.status' => 1
        );
        $pages = ClassRegistry::init('Page')->find('all', array('conditions' => $conditions));
        return $pages;
    }

    public function getPages() {
        $conditions = array(
            'Page.status' => 1
        );
        $pages = ClassRegistry::init('Page')->find('all', array('conditions' => $conditions));
        return $pages;
    }

    public function getPagesInCategory($categoryId) {
        $conditions = array(
            'Page.status' => 1,
            'Page.page_category_id' => $categoryId
        );
        $pages = ClassRegistry::init('Page')->find('all', array('conditions' => $conditions));
        return $pages;
    }

    public function getUpcomingEvents($limit = null) {
        $conditions = array(
            'Event.date >' => date("Y-m-d"),
            'Event.status' => 1
        );

        $events = ClassRegistry::init('Event')->find('all', array('conditions' => $conditions, 'order' => array('Event.date' => 'DESC'), 'limit' => $limit));

        return $events;
    }

    public function getGalleries() {
        $conditions = array(
            'Gallery.status' => 1
        );
        $galleries = ClassRegistry::init('Gallery')->find('all', array('conditions' => $conditions));
        return $galleries;
    }

    public function getRecentPhotos($limit = null) {
        $photos = ClassRegistry::init('Photo')->find('all', array('conditions' => array('Photo.status' => 1), 'order' => array('Photo.id' => 'DESC'), 'limit' => $limit));

        return $photos;
    }

    public function getRecentEvents($limit = null) {
        $options = array();
        $options['order'] = array('Event.start' => 'DESC');
        $options['limit'] = $limit;
        $revents = ClassRegistry::init('Event')->find('all', $limit);
        return $revents;
    }

    public function getDetailPhoto($key) {
        $conditions = array(
            'Image.type' => 'Photo',
            'Image.key' => $key
        );
        $image = ClassRegistry::init('Image')->find('first', array('conditions' => $conditions));

        $gallery = ClassRegistry::init('Gallery')->findById($image['Photo']['gallery_id']);

        $image['Gallery'] = $gallery['Gallery'];

        return $image;
    }

    public function getMenu() {
        $menus = ClassRegistry::init('Menu')->getMenu();
        return $menus;
    }

    public function isExistPrevMenu($position) {
        $menu = ClassRegistry::init('Menu')->getPrevMenu($position);
        if (!empty($menu)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isExistNextMenu($position) {
        $menu = ClassRegistry::init('Menu')->getNextMenu($position);
        if (!empty($menu)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isExistPrevSlide($position) {
        $slide = ClassRegistry::init('Slide')->getPrevSlide($position);
        if (!empty($slide)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isExistNextSlide($position) {
        $slide = ClassRegistry::init('Slide')->getNextSlide($position);
        if (!empty($slide)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isExistPrevWidget($sort) {
        $widget = ClassRegistry::init('Widget')->getPrevWidget($sort);
        if (!empty($widget)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isExistNextWidget($sort) {
        $widget = ClassRegistry::init('Widget')->getNextWidget($sort);
        if (!empty($widget)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    #===================================USER===============#

    public function isAdmin($user_id) {
        $user = ClassRegistry::init('User')->findById($user_id);
        if (!empty($user)):
            if ($user['User']['role_id'] == 1) {
                return TRUE;
            } else {
                return FALSE;
            }
        else:
            return FALSE;
        endif;
    }

    public function isOperator($user_id) {
        $user = ClassRegistry::init('User')->findById($user_id);
        if (!empty($user)):
            if ($user['User']['role_id'] == 2) {
                return TRUE;
            } else {
                return FALSE;
            }
        else:
            return FALSE;
        endif;
    }

    public function isMember($user_id) {
        $user = ClassRegistry::init('User')->findById($user_id);
        if (!empty($user)):

            if ($user['User']['role_id'] == 3) {
                return TRUE;
            } else {
                return FALSE;
            }

        else:
            return FALSE;
        endif;
    }

    #=======================================================#
//    public function facebookApp() {
//        echo '<div id="fb-root"></div>
//<script>(function(d, s, id) {
//  var js, fjs = d.getElementsByTagName(s)[0];
//  if (d.getElementById(id)) return;
//  js = d.createElement(s); js.id = id;
//  js.src = "//connect.facebook.net/jv_ID/all.js#xfbml=1&appId=418973451447284";
//  fjs.parentNode.insertBefore(js, fjs);
//}(document, "script", "facebook-jssdk"));</script>';
//    }

    public function facebookApp() {
        echo '<div id="fb-root"></div>
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : "418973451447284", // App ID
                    status     : true, // check login status
                    cookie     : true, // enable cookies to allow the server to access the session
                    xfbml      : true  // parse XFBML
                });

                // Additional initialization code here
            };

            // Load the SDK Asynchronously
            (function(d){
                var js, id = "facebook-jssdk", ref = d.getElementsByTagName("script")[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement("script"); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js";
                ref.parentNode.insertBefore(js, ref);
            }(document));
        </script>';
    }

    #================================ FORUM ==#

    function getRecentTopicForum() {
        App::import('Model', 'Forum.Topic');

        $topic = new Topic();
        return $topic->getLatest();
    }

    function getWidgets($position) {
        $conditions = array(
            'Widget.status' => 1,
            'Widget.position' => $position
        );
        $widgets = ClassRegistry::init('Widget')->find('all', array('conditions' => $conditions, 'order' => array('Widget.sort' => 'ASC')));
        return $widgets;
    }

    function getPagesByPermalinkCategory($permalink) {
        $pages = ClassRegistry::init('Page')->findPagesByPermalinkCategory($permalink);
        return $pages;
    }

    function generateStarRating($qty) {
        $star = '';
        for ($i = 1; $i <= $qty; $i++) {
            $star .='<i class="fa fa-star"></i>';
        }
        return $star;
    }

    function getDocumentCategoriesByParent($parentId) {
        return ClassRegistry::init('DocumentCategory')->getDocumentCategoriesByParent($parentId);
    }

    function lang($key) {
        $langs = ClassRegistry::init('Lang')->langs($_SESSION['lang']);
        return isset($langs[$key]) ? $langs[$key] : $key;
    }

}

?>
