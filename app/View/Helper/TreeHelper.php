<?php

class TreeHelper extends AppHelper {

    var $tab = "  ";
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Js'
    );

    function show($name, $data) {
        list($modelName, $fieldName) = explode('/', $name);
        $output = $this->list_element($data, $modelName, $fieldName, 0);

        return $this->output($output);
    }

    function showStructure($name, $data) {
        list($modelName, $fieldName) = explode('/', $name);
        $output = $this->listStructure($data, $modelName, $fieldName, 0);

        return $this->output($output);
    }

    function indentTree($array, $counter = 0) {
        $ret = '';
        $array2 = array();
        $pre = '';
        for ($i = 1; $i < $counter; $i++) {
            $pre .= '--';
        }

        foreach ($array as $key => $value) {
            if ($key == 'children') {
                if (isset($value['Menu']) || (isset($value['children']) && sizeof($value['children']) > 0)) {
                    $indented[] = $this->indentTree($value, ++$counter);
                } else {
                    if (sizeof($value) > 0) {
                        $indented[] = $this->indentTree($value, $counter);
                    }
                }
            } elseif ($key == 'Menu') {
                $indented[$value['id']] = ' ' . $pre . ' ' . $value['name'];
            } elseif (isset($value['Menu']['name'])) {
                $indented[] = $this->indentTree($value, $counter);
            }
        }
        return $this->flatten_array($indented, 2);
    }

    function indentTreeCategory($array, $counter = 0) {
        $ret = '';
        $array2 = array();
        $pre = '';
        for ($i = 1; $i < $counter; $i++) {
            $pre .= '--';
        }

        foreach ($array as $key => $value) {
            if ($key == 'children') {
                if (isset($value['DocumentCategory']) || (isset($value['children']) && sizeof($value['children']) > 0)) {
                    $indented[] = $this->indentTreeCategory($value, ++$counter);
                } else {
                    if (sizeof($value) > 0) {
                        $indented[] = $this->indentTreeCategory($value, $counter);
                    }
                }
            } elseif ($key == 'DocumentCategory') {
                $indented[$value['id']] = ' ' . $pre . ' ' . $value['name'];
            } elseif (isset($value['DocumentCategory']['name'])) {
                $indented[] = $this->indentTreeCategory($value, $counter);
            }
        }
        return $this->flatten_array($indented, 2);
    }

    function flatten_array($array, $preserve_keys = 0, &$out = array()) {
        foreach ($array as $key => $child) {
            if (is_array($child)) {
                $out = $this->flatten_array($child, $preserve_keys, $out);
            } elseif ($preserve_keys + is_string($key) > 1) {
                $out[$key] = $child;
            } else {
                $out[] = $child;
            }
        }
        return $out;
    }

    function list_element($data, $modelName, $fieldName, $level) {
        $baseUrl = Router::url('/');
        $tabs = "\n" . str_repeat($this->tab, $level * 2);
        $li_tabs = $tabs . $this->tab;
        $output = $tabs . "<ul>";
        foreach ($data as $key => $val) {

            $output .= $li_tabs . "<li><a href='" . $baseUrl . $val[$modelName]['url'] . "'>" . $val[$modelName][$fieldName] . '</a>';
            if (isset($val['children'][0])) {
                $output .= $this->list_element($val['children'], $modelName, $fieldName, $level + 1);
                $output .= $li_tabs . "</li>";
            } else {
                $output .= "</li>";
            }
        }
        $output .= $tabs . "</ul>";


        return $output;
    }

    function listStructure($data, $modelName, $fieldName, $level) {
        $baseUrl = Router::url('/');
        $tabs = "\n" . str_repeat($this->tab, $level * 2);
        $li_tabs = $tabs . $this->tab;
        $output = $tabs . "<ul>";
        foreach ($data as $key => $val) {

            $image = $this->getSingleImage('OfficialProfile', $val[$modelName]['key']);
            if (!empty($image)):
                $imageUrl = Router::url('/') . 'img/timthumb.php?src=' . $image['Image']['large'] . '&h=100&w=85';
            endif;
            $officialProfile = $this->getOfficialProfile($val[$modelName]['key']);

            $content = '<span class="jabatan">' . $val[$modelName][$fieldName] . '</span><br/><span class="img-structure"><img src="' . $imageUrl . '"/></span><br/><span class="person-name">' . $officialProfile['OfficialProfile']['name'] . '</span>';

            $output .= $li_tabs . "<li><a href='" . Router::url('/') . 'official_profiles/view/' . $val[$modelName]['id'] . "'>" . $content . '</a>';
            if (isset($val['children'][0])) {
                $output .= $this->listStructure($val['children'], $modelName, $fieldName, $level + 1);
                $output .= $li_tabs . "</li>";
            } else {
                $output .= "</li>";
            }
        }
        $output .= $tabs . "</ul>";


        return $output;
    }

    function getMenu() {
        return ClassRegistry::init('Menu')->getMenu();
    }

    function getStructure() {
        return ClassRegistry::init('Structure')->getStructure();
    }

    function getMenus() {
        $menus = ClassRegistry::init('Menu')->find('threaded', array('order' => array('Menu.position' => 'ASC')));
        return $menus;
    }

    // Structure tree table structures
    function structureTree($array, $counter = 0) {
        $ret = '';
        $array2 = array();
        $pre = '';
        for ($i = 1; $i < $counter; $i++) {
            $pre .= '--';
        }

        $indented = array();

        foreach ($array as $key => $value) {
            if ($key == 'children') {
                if (isset($value['Structure']) || (isset($value['children']) && sizeof($value['children']) > 0)) {
                    $indented[] = $this->structureTree($value, ++$counter);
                } else {
                    if (sizeof($value) > 0) {
                        $indented[] = $this->structureTree($value, $counter);
                    }
                }
            } elseif ($key == 'Structure') {
                $indented[$value['id']] = ' ' . $pre . ' ' . $value['name'];
            } elseif (isset($value['Structure']['name'])) {
                $indented[] = $this->structureTree($value, $counter);
            }
        }

        return $this->flatten_array($indented, 2);
    }

    function getBootstrapMenu() {
        $mainMenu = '';
        $dropdownClass = 'dropdown-submenu';
        $menuClass = 'dropdown-menu animated fadeInUp';
        $menus = $this->getMenus();
        if ($this->Session->read('lang') == 'EN'):
            foreach ($menus as $key => $val) {
                $menus[$key]['Menu']['name'] = $val['Menu']['name_en'];
                $menus[$key]['Menu']['url'] = $val['Menu']['url_en'];
                if (!empty($menus[$key]['children'])) {
                    foreach ($menus[$key]['children'] as $id => $menu) {
                        $menus[$key]['children'][$id]['Menu']['name'] = $menu['Menu']['name_en'];
                        $menus[$key]['children'][$id]['Menu']['url'] = $menu['Menu']['url_en'];
                        if (!empty($menus[$key]['children'][$id]['children'])) {
                            foreach ($menus[$key]['children'][$id]['children'] as $x => $value) {
                                $menus[$key]['children'][$id]['children'][$x]['Menu']['name'] = $value['Menu']['name_en'];
                                $menus[$key]['children'][$id]['children'][$x]['Menu']['url'] = $value['Menu']['url_en'];
                            }
                        }
                    }
                }
            }

        endif;


        $mainMenu .= '<ul class="nav navbar-nav">';
        if (!empty($menus)):
            foreach ($menus as $menu):
                if (empty($menu['children'])):
                    if ($menu['Menu']['url'] == 'home') {
                        $mainMenu .='<li><a href="' . Router::url('/') . '">' . $menu['Menu']['name'] . '</a></li>';
                    } else {
                        $mainMenu .='<li><a href="' . Router::url('/') . $menu['Menu']['url'] . '">' . $menu['Menu']['name'] . '</a></li>';
                    }
                else:
                    $mainMenu .='<li class="dropdown">';
                    $mainMenu .= '<a href="' . Router::url('/') . $menu['Menu']['url'] . '" class="dropdown-toggle" data-toggle="dropdown">' . $menu['Menu']['name'] . ' <b class="caret"></b></a>';
                    $mainMenu .= '<ul class="' . $menuClass . '">';
                    foreach ($menu['children'] as $first):

                        if (empty($first['children'])):
                            $mainMenu .= '<li><a href="' . Router::url('/') . $first['Menu']['url'] . '">' . $first['Menu']['name'] . '</a></li>';
                        else:
                            $mainMenu .= '<li class="' . $dropdownClass . '">';
                            $mainMenu .= '<a href="' . Router::url('/') . $first['Menu']['url'] . '">' . $first['Menu']['name'] . '</a>';
                            $mainMenu .= '<ul class="' . $menuClass . '">';

                            foreach ($first['children'] as $second):

                                if (empty($second['children'])):
                                    $mainMenu .= '<li><a href="' . Router::url('/') . $second['Menu']['url'] . '">' . $second['Menu']['name'] . '</a></li>';
                                else:
                                    $mainMenu .= '<li class="' . $dropdownClass . '">';
                                    $mainMenu .= '<a href="' . Router::url('/') . $second['Menu']['url'] . '">' . $second['Menu']['name'] . '</a>';
                                    $mainMenu .= '<ul class="' . $menuClass . '">';
                                    foreach ($second['children'] as $third):
                                        $mainMenu .= '<li><a href="' . Router::url('/') . $third['Menu']['url'] . '">' . $third['Menu']['name'] . '</a></li>';
                                    endforeach;
                                    $mainMenu .= '</ul>';
                                    $mainMenu .= '</li>';

                                endif;
                            endforeach;
                            $mainMenu .= '</ul>';
                            $mainMenu .= '</li>';

                        endif;
                    endforeach;
                    $mainMenu .='</ul>';
                    $mainMenu .='</li>';

                endif;
            endforeach;
//            if ($this->Session->read('Auth.User.id')) {
//                $mainMenu .= '<li class="dropdown">
//    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
//        Akunku
//        <b class="caret"></b>
//    </a>
//    <ul class="dropdown-menu">
//        <li><a href="' . Router::url('/') . 'users/profile">Profil</a></li>
//        <li><a href="' . Router::url('/') . 'users/logout">Keluar</a></li>
//    </ul>
//
//</li>';
//            } else {
//
//                $mainMenu .= '<li class="dropdown">
//    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
//        Login
//        <b class="caret"></b>
//    </a>
//    <ul class="dropdown-menu">
//        <li><a href="' . Router::url('/') . 'users/login">Login</a></li>
//        <li><a href="' . Router::url('/') . 'users/register">Daftar</a></li>
//    </ul>
//
//</li>';
//            }
        endif;
        $mainMenu .= '</ul>';

        return $mainMenu;
    }

}

?> 