<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span9">
                        <div class="widget widget-box">
                            <div class="widget-header">
                                <i class="icon-file"></i>
                                <h3>Tambah Pengaturan</h3>
                            </div> <!-- /widget-header -->
                            <div class="widget-content">
                                <?php echo $this->Form->create('Setting'); ?>
                                <fieldset>
                                    <?php
                                    echo $this->Form->input('type');
                                    echo $this->Form->input('key');
                                    echo $this->Form->input('value');

                                    echo $this->Form->input('description');
                                    ?>
                                </fieldset>

                                <?php echo $this->Form->submit('Simpan', array('div' => false, 'class' => 'btn btn-primary btn-medium')); ?>
                                <?php echo $this->Form->end(); ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->