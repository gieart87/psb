<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <h2>Pengaturan</h2>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add')); ?>"><i class="icon-plus"></i> Tambah</a>
                    </div>
                </div>
                <br/>
                <div class="row-fluid">
                    <div class="span12">

                        <table cellpadding="0" cellspacing="0" border="0" id="table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Tipe</th>
                                    <th>Key</th>
                                    <th>Value</th>
                                    <th>Description</th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($settings as $setting): ?>

                                    <tr>
                                        <td><?php echo h($setting['Setting']['type']); ?>&nbsp;</td>
                                        <td><?php echo h($setting['Setting']['key']); ?>&nbsp;</td>
                                        <td><?php echo h($setting['Setting']['value']); ?>&nbsp;</td>
                                        <td>
                                            <?php echo $setting['Setting']['description']; ?>
                                        </td>
                                        <td class="actions">
                                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $setting['Setting']['id'])); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->