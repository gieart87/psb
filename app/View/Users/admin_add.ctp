<div class="users form">
<?php echo $this->Form->create('User');?>
	<fieldset>
		<legend><?php echo __('Admin Add User'); ?></legend>
	<?php
		echo $this->Form->input('username');
		echo $this->Form->input('name');
		echo $this->Form->input('password');
		echo $this->Form->input('phone');
		echo $this->Form->input('unlimited');
		echo $this->Form->input('unlimited_until');
		echo $this->Form->input('quota');
		echo $this->Form->input('quota_limit');
		echo $this->Form->input('created_date');
		echo $this->Form->input('last_edit');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index'));?></li>
	</ul>
</div>
