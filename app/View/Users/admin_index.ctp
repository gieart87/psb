<script type="text/javascript">
    /*Table book list*/
    $(function() {
        otTable = $('#table').dataTable({
            "sDom": "<'row-fluid'<'span7'l><'span5'f>r>t<'row-fluid'<'span4'i><'span8 pull-right'p>>",
            "sPaginationType": "bootstrap"
        });

        otTable.fnSort([[0, 'desc']]);

        $('.dataTables_filter').addClass("pull-right");
        $('[name="table_length"]').css("width", "55px");
    })
</script>
<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="page-header">
                            <h2>Daftar User</h2>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">

                        <table cellpadding="0" cellspacing="0" border="0" id="table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Balance</th>
                                    <th>Created</th>
                                    <th>Status</th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($users as $user): ?>
                                    <tr>
                                        <td><?php echo h($user['User']['id']); ?>&nbsp;</td>
                                        <td><?php echo h($user['User']['username']); ?>&nbsp;</td>
                                        <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                                        <td><?php echo number_format($user['User']['balance']); ?>&nbsp;</td>

                                        <td><?php echo h($user['User']['created']); ?>&nbsp;</td>

                                        <td><?php echo h($status[$user['User']['status']]); ?>&nbsp;</td>
                                        <td class="actions">
                                            <div class="btn-group">
                                                <a class="btn btn-primary" href="#">Aksi</a>
                                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id'])); ?></li>
                                                    <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->