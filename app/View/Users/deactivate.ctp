<div class="account-container">

    <div class="content clearfix">

        <?php echo $this->Form->create('User', array('action' => 'do_deactivate')); ?>
        <?php echo $this->Form->hidden('username'); ?>
        <?php echo $this->Form->hidden('token'); ?>


        <h1>Deactivate Account</h1>		

        <div class="login-fields">

            <p>Are you sure want to deactivate your account?</p>

        </div> <!-- /login-fields -->

        <div class="login-actions">
            <div class="row-fluid">
                <div class="span6">
                    <button class="button btn btn-warning btn-large">Deactivate!</button>&nbsp;
                </div>
                <div class="span6">
                    <a class="button btn btn-success btn-large" href="<?php echo $this->Html->url('/'); ?>">Not Now</a>
                </div>
            </div>
        </div> <!-- .actions -->


        <?php echo $this->Form->end() ?>
        <br/>
    </div> <!-- /content -->

</div> <!-- /account-container -->