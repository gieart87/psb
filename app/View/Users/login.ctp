<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Login System</h1>
    </div>
</div>

<!-- Page Detail -->
<div class="container content">   
    <div class="row margin-bottom-30">
        <div class="col-md-9 mb-margin-bottom-30">

            <br>
            <?php echo $this->Form->create('User', array('action' => 'login')); ?>
            <label>Email</label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('username', array('label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Username')); ?>
                </div>                
            </div>

            <label>Password <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('password', array('value' => '', 'label' => false, 'div' => false, 'class' => 'form-control', 'placeholder' => 'Password')); ?>
                </div>                
            </div>
            <p><button type="submit" class="btn-u">Login</button></p>
            <div class="row">
                <div class="col-md-7">

                    Forgot Password? <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'forgot_password')); ?>">Click here.</a>
                </div>
            </div>
            <?php echo $this->Form->end() ?>
        </div><!--/col-md-9-->

        <div class="col-md-3">
            <!-- Contacts -->
            <div class="headline"><h2>Contacts</h2></div>
            <ul class="list-unstyled who margin-bottom-30">
                <li><a href="#"><i class="fa fa-home"></i>Jalan Solo KM.10 Yogyakarta</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i>halo@imigrasijogja.com</a></li>
                <li><a href="#"><i class="fa fa-phone"></i>0274-23423</a></li>
            </ul>

            <!-- Business Hours -->
            <div class="headline"><h2>Jam Kerja</h2></div>
            <ul class="list-unstyled margin-bottom-30">
                <li><strong>Senin-Kamis:</strong> 08.00 - 16.00</li>
                <li><strong>Jumat:</strong> 08.00 - 11.00</li>
            </ul>

            <!-- Why we are? -->
        </div><!--/col-md-3-->
    </div>
</div>
<hr>
<!-- End Page Detail -->      
