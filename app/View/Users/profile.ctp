<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">
                <br>
                <div class="span8">      		

                    <div class="widget ">

                        <div class="widget-header">
                            <i class="icon-user"></i>
                            <h3>Data akun Anda</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content">
                            <div class="tab-content">
                                <?php echo $this->Form->create('User', array('action' => 'profile', 'type' => 'file')); ?>
                                <?php echo $this->Form->input('id'); ?>
                                <div class="clearfix">
                                    <label for="xlInput">Email :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('username', array('readOnly' => 'true','label' => false, 'div' => false, 'class' => 'span6')); ?>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <label for="xlInput">Nama Lengkap :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('Customer.name', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <label for="xlInput">Password :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('password', array('value' => '', 'label' => false, 'div' => false, 'class' => 'span6')); ?>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <label for="xlInput">Konfirmasi Password :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('confirm_password', array('value' => '', 'type' => 'password', 'label' => false, 'div' => false, 'class' => 'span6')); ?>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <label for="xlInput">Telepon :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('Customer.phone', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <label for="xlInput">Alamat :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('Customer.address', array('label' => false, 'div' => false, 'type' => 'textarea', 'rows' => 3, 'class' => 'span6')); ?>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <label for="xlInput">Twitter :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('twitter', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <label for="xlInput">Facebook :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('facebook', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                    </div>
                                </div>

                                <div class="clearfix">
                                    <label for="xlInput">Avatar :</label>
                                    <div class="input">
                                        <?php echo $this->Form->input('avatar', array('type' => 'file', 'label' => false, 'div' => false)); ?>
                                    </div>
                                </div>
                                <?php if ($this->HelpTablet->isAnAuthor($this->Session->read('Auth.User.id')) == TRUE): ?>
                                    <h3>Data Penulis</h3><hr/>
                                    <?php echo $this->Form->input('Author.id', array('label' => false, 'div' => false)); ?>
                                    <div class="clearfix">
                                        <label for="xlInput">Nama Lengkap * :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.name', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Alamat Kantor / Rumah *  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.address', array('label' => false, 'div' => false, 'type' => 'textarea', 'rows' => 3, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Kota *  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.city', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Telepon *  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.phone', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Fax :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.fax', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">No Rekening Bank :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.bank_account_number', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <label for="xlInput">Nama Pemilik Rekening Bank :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.bank_account_name', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Nama Bank :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Author.bank_name', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if ($this->HelpTablet->isAPublisher($this->Session->read('Auth.User.id')) == TRUE): ?>
                                    <?php echo $this->Form->input('Publisher.id', array('label' => false, 'div' => false)); ?>
                                    <h3>Data Penerbit</h3><hr/>
                                    <div class="clearfix">
                                        <label for="xlInput">Logo :</label>
                                        <div class="input">
                                            <img src="<?php echo $this->request->data['Publisher']['logo']; ?>"/>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Nama Lengkap * :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.name', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Alamat Kantor / Rumah *  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.address', array('label' => false, 'div' => false, 'type' => 'textarea', 'rows' => 3, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Kota *  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.city', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Telepon *  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.phone', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Email Penerbit *  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.email', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Logo Penerbit  :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.logo', array('type' => 'file', 'label' => false, 'div' => false)); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Fax :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.fax', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">No Rekening Bank :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.bank_account_number', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>

                                    <div class="clearfix">
                                        <label for="xlInput">Nama Pemilik Rekening Bank :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.bank_account_name', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Nama Bank :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Publisher.bank_name', array('label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label for="xlInput">Tipe Publikasi :</label>
                                        <div class="input">
                                            <?php echo $this->Form->input('Type', array('type' => 'select', 'multiple' => true, 'label' => false, 'div' => false, 'class' => 'span6')); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="actions">
                                    <input type="submit" class="btn btn-primary" value="Simpan">
                                </div>

                                <?php echo $this->Form->end(); ?>




                            </div>
                            <!-- Div yang bagian kanan -->

                        </div> <!-- /widget-content -->

                    </div> <!-- /widget -->

                </div> <!-- /span8 -->


                <div class="span4">


                    <div class="widget widget-box">

                        <div class="widget-header">
                            <h3>Avatar</h3>
                        </div> <!-- /widget-header -->

                        <div class="widget-content center">

                            <?php if (!empty($this->request->data['User']['avatar'])): ?>
                                <?php echo $this->Html->image($this->request->data['User']['avatar']); ?>
                            <?php else: ?>
                                <?php echo $this->Html->image('avatar.jpg') ?>
                            <?php endif; ?>
                        </div> <!-- /widget-content -->

                    </div> <!-- /widget-box -->
                    <div class="widget widget-box">
                        <div class="widget-header">
                            <h3>Deposit Anda</h3>
                        </div>
                        <div class="widget-content center">
                            <h2>IDR <?php echo number_format($currentBalance); ?></h2>
                            <br/>
                            <a href="<?php echo $this->Html->url(array('controller' => 'vouchers', 'action' => 'index'))?>">tambah deposit</a>
                        </div>
                    </div>
                    <div class="widget widget-box">
                        <div class="widget-header">
                            <h3>Upgrade Member</h3>
                        </div>
                        <div class="widget-content center">
                            <a href="<?php echo $this->Html->url(array('controller' => 'publishers', 'action' => 'add')) ?>">Menjadi Penerbit</a>
                        </div>
                    </div>
                    <div class="widget widget-box">
                        <div class="widget-header">
                            <h3>Berlanggan info</h3>
                        </div>
                        <div class="widget-content">
                            <?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'newsletter')); ?>
                            <div class="clearfix">
                                <label for="multiSelect">Anda akan mendapatkan informasi buku-buku terbaru yang terbit di aksaramaya via email sesuai dengan kategori yang Anda pilih pada isian di bawah:</label>
                                <div class="input">
                                    <?php
                                    //echo $this->Form->input('categories', array('label' => false, 'multiple' => true));
                                    echo $this->Form->input('Category', array(
                                        'label' => false,
                                        'div' => false,
                                        'type' => 'select',
                                        //'multiple' => 'checkbox',
                                        'multiple' => true,
                                        'options' => $categories,
                                        'selected' => $this->Html->value('Category.Category'),
                                    ));
                                    ?>
                                </div>
                            </div>


                            <div class="actions">
                                <input type="submit" class="btn btn-primary" value="Simpan">
                            </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div> <!-- /span4 -->

            </div> <!-- /row -->

        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->
