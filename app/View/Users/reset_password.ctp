<div class="account-container">

    <div class="content clearfix">
        <h1>Reset Password</h1>	
        <?php echo $this->Form->create('User', array('action' => 'reset_password')); ?>
        <div class="login-fields">

            <p>Ubah akun Anda dengan password baru:</p>

            <?php echo $this->Form->input('id', array('label' => false, 'div' => false)); ?>
            <div class="field">
                <label for="username">Email:</label>
                <?php echo $this->Form->input('username', array('label' => false, 'div' => false)); ?>
            </div>
            <div class="field">
                <label for="password">Password:</label>
                <?php echo $this->Form->input('password', array('value' => '', 'label' => false, 'div' => false)); ?>
            </div>
            <div class="field">
                <label for="confirm_password">Konfirmasi Password:</label>
                <?php echo $this->Form->input('confirm_password', array('value' => '', 'type' => 'password', 'label' => false, 'div' => false)); ?>
            </div>
        </div> <!-- /login-fields -->

        <div class="login-actions">
            <button class="button btn btn-warning btn-large">Reset Sekarang</button>
        </div> <!-- .actions -->

        <?php echo $this->Form->end(); ?>
    </div> <!-- /content -->

</div> <!-- /account-container -->
<br/>