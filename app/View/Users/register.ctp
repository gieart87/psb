
<div class="account-container register">

    <div class="content clearfix">

        <?php echo $this->Form->create('User', array('action' => 'register')); ?>

        <h1>Create Your Account</h1>			

        <div class="login-fields">

            <p>Create your free account:</p>

            <div class="field">
                <label for="firstname">Full Name:</label>
                <?php echo $this->Form->input('name', array('label' => false, 'div' => false, 'class' => 'login required', 'placeholder' => 'Full Name')); ?>
            </div> <!-- /field -->
            <div class="field">
                <label for="lastname">Email (username) :</label>	
                <?php echo $this->Form->input('username', array('label' => false, 'div' => false, 'class' => 'login required email', 'placeholder' => 'Email')); ?>
            </div> <!-- /field -->


            <div class="field">
                <label for="email">Password:</label>
                <?php echo $this->Form->input('password', array('label' => false, 'div' => false, 'class' => 'login required', 'id' => 'password', 'minlength' => 6, 'placeholder' => 'Password')); ?>
            </div> <!-- /field -->

            <div class="field">
                <label for="email">Password:</label>
                <?php echo $this->Form->input('confirm_password', array('type' => 'password', 'label' => false, 'div' => false, 'class' => 'login required', 'id' => 'confirm_password', 'equalTo' => '#password', 'placeholder' => 'Password')); ?>
            </div> <!-- /field -->
            <div class="field">
                <script type="text/javascript">
                    var RecaptchaOptions = {
                        theme: 'custom',
                        custom_theme_widget: 'recaptcha_widget',
                        lang: 'en'
                    };
                </script>
                <div id="recaptcha_widget" style="display:none">

                    <div id="recaptcha_image"></div>
                    <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>

                    <span class="recaptcha_only_if_image">Enter the words above or &nbsp;</span>
                    <span class="recaptcha_only_if_audio">Enter the numbers you hear or &nbsp;</span>
                    <a href="javascript:Recaptcha.reload()" title="Get new captcha"> <i class="icon-refresh"></i></a>&nbsp;&nbsp;
                    <a href="javascript:Recaptcha.switch_type('audio')" class="recaptcha_only_if_image" title="Get audio captcha"><i class="icon-volume-up"></i></a>&nbsp;
                    <a href="javascript:Recaptcha.switch_type('image')" class="recaptcha_only_if_audio" title="Get image captcha"><i class="icon-picture"></i></a>&nbsp;&nbsp;
                    <a href="javascript:Recaptcha.showhelp()" title="Help?" ><i class="icon-question-sign"></i></a>&nbsp;
                    <input type="text" id="recaptcha_response_field" name="data[User][recaptcha_response_field]" class="required" placeholder="Type here" />
                    <?php echo $this->Recaptcha->error(); ?>
                </div>

                <script type="text/javascript"
                        src="http://www.google.com/recaptcha/api/challenge?k=6Led1eESAAAAACFOPJswOenwDbD8U9Nvg-ZsDa-v">
                </script>
                <noscript>
                <iframe src="http://www.google.com/recaptcha/api/noscript?k=6Led1eESAAAAACFOPJswOenwDbD8U9Nvg-ZsDa-v"
                        height="300" width="500" frameborder="0"></iframe><br>
                <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                </textarea>
                <input type="hidden" name="recaptcha_response_field"
                       value="manual_challenge">
                </noscript>
                <?php
//            echo $this->Recaptcha->show(array(
//                'theme' => 'clean',
//                'lang' => 'en',
//            ));
                ?>
                <!--</div>-->

            </div> <!-- /login-fields -->

            <div class="login-actions">
                <span class="login-checkbox">
                    <input id="Field" name="Field" type="checkbox" class="field login-checkbox required" value="First Choice" tabindex="4" />
                    <label class="choice" for="Field">I have read and agree with the Terms of Use.</label>
                </span>

                <button class="button btn btn-primary btn-large">Sign Up</button>

            </div> <!-- .actions -->

            <?php echo $this->Form->end() ?>

        </div> <!-- /content -->

    </div> <!-- /account-container -->

</div>
<!-- Text Under Box -->
<div class="login-extra">
    Already have an account? <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'login')); ?>">Login</a>
</div> <!-- /login-extra -->
