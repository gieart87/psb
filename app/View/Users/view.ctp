<?php
echo $this->Html->script("carts/cart");
?>
<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">

                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget">
                            <div class="widget-header">
                                <i class="icon-pushpin"></i>
                                <h3>Detail User</h3>
                            </div>
                            <div class="widget-content">

                                <div class="searchbox">

                                    <div style="margin-left: 0px; width:220px;float:left">
                                        <img class="img-polaroid imgbig" src="<?php echo $user['User']['avatar']; ?>" alt=""/>
                                    </div>
                                    <div class="span6" style="width:280px">
                                        <div class="searchboxdesc">
                                            <h1><?php echo h($user['User']['name']); ?></h1>

                                            <p>

                                                <b>Total Reads  :</b> <?php echo h($user['Statistic']['total_reads']); ?><br>
                                                <b>Total Followers :</b> <?php echo $user['Statistic']['total_followers'] ?><br>
                                                <b>Total Followings :</b> <?php echo h($user['Statistic']['total_followings']); ?>
                                            </p>


                                        </div>
                                    </div>
                                </div>
                            </div> <!--widget content--> 
                            <br/>
                            <div class="widget-header">
                                <i class="icon-pushpin"></i>
                                <h3>Deskripsi</h3>
                            </div>
                            <div class="widget-content">
                                <?php echo $user['User']['about'] ?>
                            </div>
<!--                            <div class="fygbox">Buku yang Ditulis :</div>
                            <div class="fyg">
                                <div class="fygleft obL"></div>
                                <div class="fygright obR"></div>
                                <ul id="otherbook">
                                </ul>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /container -->

    </div> <!-- /main-inner -->

</div> <!-- /main -->
<script type="text/javascript">
//    $(function() {
//        var authorId = <?php echo $user['User']['id']; ?>;
//
//        $.getJSON(SERVER + 'authors/books?author_id=' + authorId, function(data) {
//            showOtherBooks(data.data);
//
//        });
//
//        function showOtherBooks(data) {
//            var book = '';
//            $.each(data, function(i, v) {
//                book += '<li class="pophover" data-content="' + v.Book.title + '" rel="popover" data-original-title="' + v.Book.title + '"><a href="' + SERVER + 'books/view/' + v.Book.id + '"><img src="' + v.Book.cover + '" style="height:147px" class="img-polaroid"></a></li>';
//            });
//            $('#otherbook').html(book).carouFredSel({
//                auto: false,
//                prev: '.obL',
//                next: '.obR',
//                mousewheel: true,
//                swipe: {
//                    onMouse: true,
//                    onTouch: true
//                }
//            });
//
//        }
//
//
//    })
</script>