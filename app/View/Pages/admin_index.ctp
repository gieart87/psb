<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add')) ?>">Tambah</a>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
    </div>
</div>
<br/>
<section class="" id="widget-grid">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
            <div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false">
                <!-- widget options:
                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                data-widget-colorbutton="false"
                data-widget-editbutton="false"
                data-widget-togglebutton="false"
                data-widget-deletebutton="false"
                data-widget-fullscreenbutton="false"
                data-widget-custombutton="false"
                data-widget-collapsed="true"
                data-widget-sortable="false"

                -->
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Halaman</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">

                        </div>

                        <table id="dt_basic" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Title</th>
                                    <th>Status</th>

                                    <th>User</th>
                                    <th>Date</th>
                                    <th>Set Menu</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($pages)): ?>
                                    <?php $no = 1; ?>
                                    <?php foreach ($pages as $page): ?>

                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $page['Page']['title']; ?></td>
                                            <td><?php echo $status[$page['Page']['status']]; ?></td>

                                            <td><?php echo $page['User']['name']; ?></td>
                                            <td><?php echo $page['Page']['created']; ?></td>
                                            <td>
                                                <?php echo $this->Html->link(__('Set Menu'), array('controller' => 'menus', 'action' => 'add', '?' => array('name' => $page['Page']['title'],'name_en' => $page['Page']['title_en'], 'url' => 'pages/read/' . $page['Page']['permalink'], 'url_en' => 'pages/read/' . $page['Page']['permalink_en']))); ?>
                                            </td>
                                            <td>
                                                <ul class="actions">
                                                    <li>
                                                        <?php echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('action' => 'edit', $page['Page']['id']), array('escape' => false)); ?>
                                                    </li>
                                                    <li>
                                                        <?php echo $this->Html->link(__('<i class="fa fa-plus"></i>'), array('controller' => 'content_tabs', 'action' => 'add', '?' => array('type' => 'Page', 'key' => $page['Page']['id'])), array('escape' => false)); ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'), array(
                                                            'action' => 'delete', $page['Page']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $page['Page']['id'])
                                                        );
                                                        ?>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>



<!-- end widget grid -->



<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/jquery.dataTables-cust.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ColReorder.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/FixedColumns.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ColVis.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ZeroClipboard.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/media/js/TableTools.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/DT_bootstrap.js"></script>


<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function() {

        pageSetUp();

        /*
         * BASIC
         */
        $('#dt_basic').dataTable({
            "sPaginationType": "bootstrap_full"
        });

        /* END BASIC */

        /* Add the events etc before DataTables hides a column */
        $("#datatable_fixed_column thead input").keyup(function() {
            oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
        });

        $("#datatable_fixed_column thead input").each(function(i) {
            this.initVal = this.value;
        });
        $("#datatable_fixed_column thead input").focus(function() {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("#datatable_fixed_column thead input").blur(function(i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = this.initVal;
            }
        });


        var oTable = $('#datatable_fixed_column').dataTable({
            "sDom": "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            //"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "bSortCellsTop": true
        });



        /*
         * COL ORDER
         */
        $('#datatable_col_reorder').dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "R<'dt-top-row'Clf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "fnInitComplete": function(oSettings, json) {
                $('.ColVis_Button').addClass('btn btn-default btn-sm').html('Columns <i class="icon-arrow-down"></i>');
            }
        });

        /* END COL ORDER */

        /* TABLE TOOLS */
        $('#datatable_tabletools').dataTable({
            "sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "oTableTools": {
                "aButtons": ["copy", "print", {
                        "sExtends": "collection",
                        "sButtonText": 'Save <span class="caret" />',
                        "aButtons": ["csv", "xls", "pdf"]
                    }],
                "sSwfPath": "<?php echo Router::url('/'); ?>js/admin/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
            "fnInitComplete": function(oSettings, json) {
                $(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
                    $(this).addClass('btn-sm btn-default');
                });
            }
        });

        /* END TABLE TOOLS */
    })

</script>
