<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Struktur Organisasi</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li class="active">Struktur Organisasi</li>
        </ul>
    </div>
</div>

<div class="container content">     
    <div class="row blog-page">    
        <!-- Page Detail -->
        <div class="col-md-12">
            <!--Blog Post-->        
            <div class="blog margin-bottom-40">
                <div class="tree">
                    <?php
                    $structures = $this->Tree->getStructure();
                    $structure = $this->Tree->showStructure('Structure/name', $structures);
                    echo $structure;
                    ?>
                </div>

            </div>
            <!--End Blog Post-->        

        </div>
        <!-- End Page Detail -->


    </div><!--/row-->        
</div>