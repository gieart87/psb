<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-fullscreenbutton="false" data-widget-sortable="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-pencil"></i> </span>
                    <h2>Edit Halaman</h2>

                </header>

                <!-- widget div-->
                <div role="content">
                    <!-- widget content -->
                    <div class="widget-body no-padding">    

                        <!-- NEW COL START -->
                        <?php echo $this->Form->create('Page', array('action' => 'edit', 'type' => 'file', 'class' => 'smart-form')); ?>
                        <fieldset>
                            <div class="row">
                                <section class="col col-12 pull-right">
                                    <button type="button" class="btn btn-warning translate"><i class="fa fa-rotate-right"></i> Translate</button>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Title (ID)</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('title', array('placeholder' => 'title (ID)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">Title (EN)</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('title_en', array('placeholder' => 'title (EN)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Body (ID)</label>
                                    <label class="textarea">
                                        <?php echo $this->Form->input('body', array('type' => 'textarea', 'class' => 'mceEditor', 'placeholder' => 'body (ID)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">Body (EN)</label>
                                    <label class="textarea">
                                        <?php echo $this->Form->input('body_en', array('type' => 'textarea', 'class' => 'mceEditor', 'placeholder' => 'body (EN)', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>

                            <div class="row">
                                <section>
                                    <?php if (!empty($contentTabs)): ?>
                                        <ul class="content-tabs-list">
                                            <?php foreach ($contentTabs as $content): ?>
                                                <li>
                                                    <?php echo $content['ContentTab']['title'] ?>
                                                    <a href="<?php echo $this->Html->url(array('controller' => 'content_tabs', 'action' => 'edit', $content['ContentTab']['id'])); ?>"><i class="fa fa-pencil-square-o"></i></a>
                                                    <a href="<?php echo $this->Html->url(array('controller' => 'content_tabs', 'action' => 'edit', $content['ContentTab']['id'])); ?>"><i class="fa fa-trash-o"></i></a>
                                                </li>
                                            <?php endforeach; ?>
                                            <li><?php echo $this->Html->link(__('<i class="fa fa-plus"></i> Tambah Konten'), array('controller' => 'content_tabs', 'action' => 'add', '?' => array('type' => 'Page', 'key' => $this->request->data['Page']['id'])), array('escape' => false)); ?></li>
                                        </ul>
                                    <?php endif; ?>

                                </section>  
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Category</label>
                                    <label class="select"> 
                                        <?php echo $this->Form->input('page_category_id', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">Status</label>
                                    <label class="select"> 										
                                        <?php echo $this->Form->input('status', array('options' => $status, 'placeholder' => 'Status', 'label' => FALSE, 'div' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Featured Image (optional)</label>
                                    <div class="input input-file state-success">
                                        <span class="button"><input type="file" onchange="this.parentNode.nextSibling.value = this.value" name="data[Page][image]" >Browse</span><input type="text" readonly="" placeholder="Include some files" class="valid">
                                    </div>
                                </section>

                                <section class="col col-6">
                                    <label class="label">Layout</label>
                                    <label class="select"> 										
                                        <?php echo $this->Form->input('theme', array('options' => $themes, 'selected' => 0, 'placeholder' => 'Status', 'label' => FALSE, 'div' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-6">
                                    <?php
                                    $image = $this->General->getSingleImage('Page', $this->request->data['Page']['id']);
                                    if (!empty($image)):
                                        echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=200&w=525');
                                        echo '<br/>';
                                        echo $this->Html->link(__('Delete'), array('controller' => 'images', 'action' => 'delete', $image['Image']['id'], $this->request->data['Page']['id'], 'Article', 'edit'), null, __('Are you sure you want to delete # %s?', $image['Image']['id']));
                                    endif;
                                    ?>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <button class="btn btn-primary" type="submit">
                                Save
                            </button>
                            <button class="btn btn-primary" type="button" onclick="history.back();">
                                Back
                            </button>
                        </footer>



                        <?php echo $this->Form->end(); ?>
                        <!-- END COL -->		

                    </div>

                    <!-- END ROW -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->


        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
    $('.translate').click(function() {
        var title = $('#PageTitle').val();
        $.ajax({
            type: 'GET',
            url: SERVER + 'langs/translate?text=' + title,
            success: function(response) {
                $('#PageTitleEn').val(response);


                var body = tinymce.get('PageBody').getContent();
                body = body.replace(/(<([^>]+)>)/ig, "");
                $.ajax({
                    type: 'GET',
                    url: SERVER + 'langs/translate?text=' + body,
                    success: function(response) {
                        tinymce.get('PageBodyEn').setContent(response);
                    }
                });


            }
        })
    });
</script>