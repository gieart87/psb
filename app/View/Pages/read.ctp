<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Page Detail</h1>
<!--        <ul class="pull-right breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="">Features</a></li>
            <li><a href="">Pages Archive</a></li>
            <li class="active">Jawa Ipsum</li>
        </ul>-->
    </div>
</div>

<div class="container content">		
    <div class="row blog-page">    
        <!-- Page Detail -->
        <div class="col-md-9 md-margin-bottom-60">
            <!--Blog Post-->        
            <div class="blog margin-bottom-40">
                <h2><?php echo $page['Page']['title']; ?></h2>

                <div class="blog-post-tags">
                    <ul class="list-unstyled list-inline blog-info">
                        <li><i class="fa fa-calendar"></i> <?php echo $page['Page']['created']; ?></li>
                        <li><i class="fa fa-pencil"></i> <?php echo $page['User']['name']; ?></li>
                    </ul>

                </div>
                <div class="blog-img">
                    <?php $image = $this->General->getSingleImage('Page', $page['Page']['id']); ?>
                    <?php if (!empty($image)): ?>
                        <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=350&w=825', array('class' => 'img-responsive')); ?>
                    <?php endif; ?>
                </div>
                <?php echo $page['Page']['body']; ?>
            </div>
            <!--End Blog Post-->        
            <hr>
            <!-- Tabs Widget --> 
            <?php if (!empty($contentTabs)): ?>
                <div class="tab-content">
                    <div class="tab-v2 margin-bottom-40">

                        <ul class="nav nav-tabs">
                            <?php $i = 1; ?>
                            <?php foreach ($contentTabs as $list): ?>
                                <?php
                                $class = '';
                                if ($i == 1) {
                                    $class = 'class="active"';
                                }
                                ?>
                                <li <?php echo $class; ?>><a data-toggle="tab" href="#tab-<?php echo $i; ?>"><?php echo $list['ContentTab']['title']; ?></a></li>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </ul>                
                        <div class="tab-content">

                            <?php $i = 1; ?>
                            <?php foreach ($contentTabs as $tab): ?>
                                <?php
                                $class = 'class="tab-pane"';
                                if ($i == 1) {
                                    $class = 'class="tab-pane active"';
                                }
                                ?>
                                <div id="tab-<?php echo $i; ?>" <?php echo $class; ?>>
                                    <?php echo $tab['ContentTab']['body']; ?>
                                </div>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>           
                </div>           
            <?php endif; ?>
            <!-- End Tabs Widget -->
        </div>
        <?php echo $this->element('public/right_sidebar'); ?>
    </div><!--/row-->        
</div>
