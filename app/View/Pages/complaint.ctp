<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left">Pengaduan Masyarakat</h1>
        <ul class="pull-right breadcrumb">
            <li><a href="<?php echo Router::url('/'); ?>">Home</a></li>
            <li class="active">Pengaduan Masyarakat</li>
        </ul>
    </div>
</div>

<!-- Page Detail -->
<div class="container content">   
    <div class="row margin-bottom-30">
        <div class="col-md-9 mb-margin-bottom-30">

            <br>
            <?php echo $this->Form->create('Page', array('action' => 'complaint')); ?>
            <label>Name</label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('name', array('label' => FALSE, 'div' => FALSE, 'class' => 'form-control', 'required' => TRUE)); ?>
                </div>                
            </div>

            <label>Email <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('email', array('type' => 'email', 'label' => FALSE, 'div' => FALSE, 'class' => 'form-control', 'required' => 'true')); ?>
                </div>                
            </div>
            <label>Address</label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('address', array('label' => FALSE, 'div' => FALSE, 'class' => 'form-control', 'required' => TRUE)); ?>
                </div>                
            </div>
            <label>Topic</label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('complaint_topic_id', array('options' => $complaintTopics, 'label' => FALSE, 'div' => FALSE, 'class' => 'form-control', 'required' => TRUE)); ?>
                </div>                
            </div>
            <label>Message</label>
            <div class="row margin-bottom-20">
                <div class="col-md-11 col-md-offset-0">
                    <?php echo $this->Form->input('message', array('type' => 'textarea', 'label' => FALSE, 'div' => FALSE, 'class' => 'form-control', 'required' => TRUE)); ?>
                </div>                
            </div>

            <p><button type="submit" class="btn-u">Send</button></p>
            <?php echo $this->Form->end(); ?>
        </div><!--/col-md-9-->

        <?php echo $this->element('public/right_sidebar'); ?>
    </div>
</div>
