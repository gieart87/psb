
<div class="container">
    <!--=== Layanan ===-->
    <div class="layanan">
        <div class="row">
            <?php
            $wniPages = $this->General->getPagesByPermalinkCategory('wni');
            ?>
            <div class="col-md-6 col-sm-12 col-xs-12 md-margin-bottom-40">
                <div class="bg-layanan">
                    <h2>Layanan untuk Warga Negara Indonesia</h2>
                    <ul>
                        <?php if (!empty($wniPages)): ?>
                            <?php foreach ($wniPages as $page): ?>
                                <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'read', $page['Page']['permalink'])); ?>"><?php echo $page['Page']['title']; ?></a></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>

            </div><!--/col-md-6-->  

            <?php
            $wnaPages = $this->General->getPagesByPermalinkCategory('wna');
            ?>
            <div class="col-md-6 col-sm-12 col-xs-12  md-margin-bottom-40">
                <div class="bg-layanan">                
                    <h2>Layanan untuk Warga Negara Asing</h2>
                    <ul>
                        <?php if (!empty($wnaPages)): ?>
                            <?php foreach ($wnaPages as $page): ?>
                                <li><a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'read', $page['Page']['permalink'])); ?>"><?php echo $page['Page']['title']; ?></a></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>

            </div><!--/col-md-6-->

        </div><!--/row-->   
    </div><!--/Layanan-->    
    <!--=== End Layanan ===-->
</div>
<!--=== Penilaian ===-->
<div class="penilaian">
    <div class="container">
        <div class="row">
            <div class="col-md-9 md-margin-bottom-40">
                <h3>Berikan penilaian terhadap pelayanan yang kami berikan</h3>
            </div><!--/col-md-8-->  
            <div class="col-md-3">
                <a href="<?php echo $this->Html->url(array('controller' => 'questions', 'action' => 'index')); ?>"><button class="nilai btn-nilai"><i class="fa fa-pencil"> Berikan Penilaian</i></button></a>
            </div><!--/col-md-4-->  
        </div><!--/row-->   
    </div><!--/container--> 
</div><!--/Penilaian-->    
<!--=== End Penilaian ===-->

<div class="container">
    <div class="row">
        <!--=== Berita Terbaru ===-->
        <div class="col-md-6">
            <div class="headline"><h2>Berita Terbaru</h2></div>
            <?php if (!empty($recentArticles)): ?>
                <div class="col-md-8">
                    <div class="thumbnails thumbnail-style thumbnail-kenburn">
                        <div class="thumbnail-img">
                            <div class="overflow-hidden">
                                <?php
                                $image = $this->General->getSingleImage('Article', $recentArticles[0]['Article']['id']);
                                if (!empty($image)):
                                    echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=300&w=300', array('class' => 'img-responsive'));
                                endif;
                                ?>
                            </div>
                            <a class="btn-tgl hover-effect" href="<?php echo $this->Html->url(array('controller' => 'articles', 'action' => 'read', $recentArticles[0]['Article']['permalink'])); ?>"><?php echo $recentArticles[0]['Article']['created']; ?></a> 
                            <a class="btn-judul " href="<?php echo $this->Html->url(array('controller' => 'articles', 'action' => 'read', $recentArticles[0]['Article']['permalink'])); ?>"><?php echo $recentArticles[0]['Article']['title']; ?></a>                     
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <?php
                    $i = 0;
                    foreach ($recentArticles as $article):
                        ?>
                        <?php if ($i > 0): ?>
                            <div class="thumbnails thumbnail-style thumbnail-list">
                                <div class="thumbnail-img">
                                    <div class="overflow-hidden">
                                        <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.jpg" alt="">
                                    </div>
                                    <div class="caption">
                                        <a href="<?php echo $this->Html->url(array('controller' => 'articles', 'action' => 'read', $article['Article']['permalink'])); ?>">Judul</a>                   
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <!--=== End Berita Terbaru ===-->

        <!--=== Featured ===-->
        <div class="col-md-6">
            <div class="headline"><h2>Featured</h2></div>
            <div class="col-md-8">
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/2.jpg" alt="">
                        </div>
                        <a class="btn-tgl hover-effect" href="#">04 Maret 2014</a> 
                        <a class="btn-judul " href="#">Lorem Ipsum Dolor</a>                      
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="thumbnails thumbnail-style thumbnail-list">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.jpg" alt="">
                        </div>
                        <a class="btn-tgl-list hover-effect" href="#">04 Maret 2014</a>                   
                    </div>
                    <div class="caption">
                        <p><a class="hover-effect" href="#">Jawa Ipsum</a></p>
                    </div>
                </div>
                <div class="thumbnails thumbnail-style thumbnail-list">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.png" alt="">
                        </div>
                        <a class="btn-tgl-list hover-effect" href="#">04 Maret 2014</a>                   
                    </div>
                    <div class="caption">
                        <p><a class="hover-effect" href="#">Jawa Ipsum</a></p>
                    </div>
                </div>
                <div class="thumbnails thumbnail-style thumbnail-list">
                    <div class="thumbnail-img">
                        <div class="overflow-hidden">
                            <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.jpg" alt="">
                        </div>
                        <a class="btn-tgl-list hover-effect" href="#">04 Maret 2014</a>                   
                    </div>
                    <div class="caption">
                        <p><a class="hover-effect" href="#">Jawa Ipsum</a></p>
                    </div>
                </div>
            </div>
        </div>
        <!--=== End Featured ===-->
        <div class="row">
            <div class="col-md-4">
                <div class="headline"><h2>Upcoming Event</h2></div>
                <div class="thumbnails thumbnail-style thumbnail-kenburn">
                    <ul>
                        <li>
                            <a href="">Pelantikan Pejabat Struktural Kantor Imigrasi Kelas I Yogyakarta</a>
                        </li>
                        <li><a href="">Pelelangan Barang</a></li>
                        <li><a href="">Senam Sehat Bersama Imigrasi Yogyakarta</a></li>
                        <li><a href="">Pembuatan Paspor Masal</a></li>
                    </ul>
                </div>
            </div>


            <div class="col-md-4">
                <div class="headline"><h2>Recent Gallery</h2></div>
                <div class="col-md-6">
                    <div class="gallery-page">
                        <div class="thumbnail thumbnails">
                            <div class="overflow-hidden">
                                <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.jpg" alt="">
                            </div>                   
                        </div>
                    </div>
                    <div class="gallery-page">
                        <div class="thumbnail thumbnails">
                            <div class="overflow-hidden">
                                <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.jpg" alt="">
                            </div>                   
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="gallery-page">
                        <div class="thumbnail thumbnails">
                            <div class="overflow-hidden">
                                <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.jpg" alt="">
                            </div>                   
                        </div>
                    </div>
                    <div class="gallery-page">
                        <div class="thumbnail thumbnails">
                            <div class="overflow-hidden">
                                <img class="img-responsive" src="<?php echo Router::url('/'); ?>img/public/1.jpg" alt="">
                            </div>                   
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="headline"><h2>Kantor Imigrasi on Facebook</h2></div>
                <div class="thumbnails thumbnail-style thumbnail-kenburn">

                </div>
            </div>
        </div><!--/row-->
    </div>    


    <div class="container">
        <!-- Our Clients -->
        <div id="clients-flexslider" class="flexslider home clients">
            <div class="headline"><h2>Link Eksternal</h2></div>	

            <div style="overflow: hidden; position: relative;" class="flex-viewport"><ul style="width: 3400%; transition-duration: 0.6s; transform: translate3d(0px, 0px, 0px);" class="slides">
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/hp_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/hp.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/igneus_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/igneus.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/vadafone_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/vadafone.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/walmart_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/walmart.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/shell_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/shell.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/natural_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/natural.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/aztec_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/aztec.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/gamescast_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/gamescast.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/cisco_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/cisco.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/everyday_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/everyday.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/cocacola_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/cocacola.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/spinworkx_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/spinworkx.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/shell_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/shell.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/natural_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/natural.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/gamescast_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/gamescast.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/everyday_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/everyday.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li style="width: 125.444px; float: left; display: block;">
                        <a href="#">
                            <img src="<?php echo Router::url('/'); ?>img/public/spinworkx_grey.png" alt=""> 
                            <img src="<?php echo Router::url('/'); ?>img/public/spinworkx.png" class="color-img" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div><!--/flexslider-->
        <!-- End Our Clients -->
    </div><!--/container-->	