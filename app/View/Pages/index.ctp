<style type="text/css">
    .stat{
        font-size: 14px;
    }
</style>
<div class="pg-opt pin">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Data Terkini  Pendaftar Calon Siswa Tahun Pelajaran <?php echo date("Y")?>/<?php echo date("Y")+1;?></h2>
            </div>
            <div class="col-md-4">
                <ol class="breadcrumb">
                    <li><a href="#">Auto reload : 5 minutes</a></li>
                </ol>
            </div>

        </div>
    </div>
</div>

<section class="slice bg-3">
    <div class="w-section inverse shop">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3 class="section-title">Audio Video</h3>
                    <p>
                        Pendaftar : <strong><?php echo count($audioVideoSiswas); ?></strong>, 
                        Kuota : <strong>32</strong>
                    </p>
                        <div class="row">
                                    <div class="col-md-3">
                                       NO PEND.
                                    </div>
                                    <div class="col-md-5">
                                        NAMA 
                                    </div>
                                    <div class="col-md-2">
                                        UN
                                    </div>
                                    <div class="col-md-2">
                                        BOBOT
                                    </div>
                    </div>
                    <ul id="audio-video" class="list-peringkat">
                        <li class="pembatas">
                            Peringkat Berdasar Bobot Nilai UNAS
                        </li>
                        <?php
                        $i = 1;
                        foreach ($audioVideoSiswas as $siswa):
                            $style = '';
                            if ($i > 32):
                                $style = 'style="background:none repeat scroll 0 0 #FF0000"';
                            endif;
                            ?>
                            <li <?php echo $style; ?>>
                                <div class="row">
                                    <div class="col-md-1">
                                        <?php echo $i ?>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="pull-left"><?php echo $siswa['Siswa']['no_pendaftaran']; ?></span>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="pull-left"><?php echo substr($siswa['Siswa']['nama'], 0, 18); ?></span>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo ($siswa['Siswa']['nilai_matematika']+$siswa['Siswa']['nilai_b_inggris']+$siswa['Siswa']['nilai_ipa']+$siswa['Siswa']['nilai_b_indonesia']);?>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="pull-left" style="margin-left:-5px;"><?php echo number_format($siswa[0]['bobot'], 2); ?></span>
                                    </div>
                                </div>
                            </li>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="section-title">Otomotif</h3>
                    <p>
                        Pendaftar : <strong><?php echo count($otomotifSiswas); ?></strong>, 
                        Kuota : <strong>96</strong>
                    </p>
                        <div class="row">
                                    <div class="col-md-3">
                                       NO PEND.
                                    </div>
                                    <div class="col-md-5">
                                        NAMA 
                                    </div>
                                    <div class="col-md-2">
                                        UN
                                    </div>
                                    <div class="col-md-2">
                                        BOBOT
                                    </div>
                    </div>
                    <ul id="otomotif" class="list-peringkat">
                        <li class="pembatas">
                            Peringkat Berdasar Bobot Nilai UNAS
                        </li>
                        <?php
                        $i = 1;
                        foreach ($otomotifSiswas as $siswa):
                            $style = '';
                            if ($i > 96):
                                $style = 'style="background:none repeat scroll 0 0 #FF0000"';
                            endif;
                            ?>
                            <li <?php echo $style; ?>>
                                <div class="row">
                                    <div class="col-md-1">
                                        <?php echo $i ?>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="pull-left"><?php echo $siswa['Siswa']['no_pendaftaran']; ?></span>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="pull-left"><?php echo substr($siswa['Siswa']['nama'], 0, 18); ?></span>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo ($siswa['Siswa']['nilai_matematika']+$siswa['Siswa']['nilai_b_inggris']+$siswa['Siswa']['nilai_ipa']+$siswa['Siswa']['nilai_b_indonesia']);?>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="pull-left" style="margin-left:-5px;"><?php echo number_format($siswa[0]['bobot'], 2); ?></span>
                                    </div>
                                </div>
                            </li>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="section-title">Multimedia</h3>
                    <p>
                        Pendaftar : <strong><?php echo count($multimediaSiswas); ?></strong>,  
                        Kuota : <strong>64</strong>
                    </p>
                    <div class="row">
                                    <div class="col-md-3">
                                       NO PEND.
                                    </div>
                                    <div class="col-md-5">
                                        NAMA 
                                    </div>
                                    <div class="col-md-2">
                                        UN
                                    </div>
                                    <div class="col-md-2">
                                        BOBOT
                                    </div>
                    </div>
                    <ul id="multimedia" class="list-peringkat">
                        <li class="pembatas">
                            Peringkat Berdasar Bobot Nilai UNAS
                        </li>
                        <?php
                        $i = 1;
                        foreach ($multimediaSiswas as $siswa):
                            $style = '';
                            if ($i > 64):
                                $style = 'style="background: none repeat scroll 0 0 #FF0000"';
                            endif;
                            ?>
                            <li <?php echo $style; ?>>
                                <div class="row">
                                    <div class="col-md-1">
                                        <?php echo $i ?>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="pull-left"><?php echo $siswa['Siswa']['no_pendaftaran']; ?></span>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="pull-left"><?php echo substr($siswa['Siswa']['nama'], 0, 18); ?></span>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo ($siswa['Siswa']['nilai_matematika']+$siswa['Siswa']['nilai_b_inggris']+$siswa['Siswa']['nilai_ipa']+$siswa['Siswa']['nilai_b_indonesia']);?>
                                    </div>
                                    <div class="col-md-2">
                                        <span class="pull-left" style="margin-left:-5px;"><?php echo number_format($siswa[0]['bobot'], 2); ?></span>
                                    </div>
                                </div>
                            </li>
                            <?php $i++; ?>
                        <?php endforeach; ?>

                    </ul>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <h4>Keterangan:</h4>
                    <hr/>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3">
                    <span class="kotak-hijau">&nbsp;</span> : <span class="keterangan">Posisi Aman</span>
                </div>
                <div class="col-md-3">
                    <span class="kotak-merah">&nbsp;</span> : <span class="keterangan">Posisi Tidak Aman</span>
                </div>
                <div class="col-md-2 stat">
                    <?php if(!empty($highest)):?>
                    Bobot Tertinggi: <strong><?php echo number_format($highest,2) ?></strong>
                    <?php endif;?>
                </div>
                <div class="col-md-2 stat">
                    <?php if(!empty($lowest)):?>
                    Bobot Terendah:<strong><?php echo number_format($lowest,2) ?></strong>
                    <?php endif;?>
                </div>
                <div class="col-md-2 stat">
                    Pendaftar : <strong><?php echo $all ?></strong>
                </div>
            </div>
        </div>
    </div>
</section>
