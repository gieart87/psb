Hallo <strong><?php echo $toUser['User']['username'] ?></strong>, <br/>

Melalu pesan ini kami memberitahukan bahwa Anda menerima hadiah buku dari teman Anda <strong><?php echo $fromUser['User']['username'] ?></strong> di Aksaramaya dengan informasi sebagai berikut:<br/>
<br/>

Judul : <?php echo $gift['Book']['title'] ?><br/>
ISBN : <?php echo $gift['Book']['isbn'] ?><br/>
Penulis : <?php echo $gift['Book']['authors'] ?><br/>

Hadiah telah ditambahkan ke dalam koleksi Anda. Silahkan di cek Koleksi Anda di Aksaramaya.
<strong><a href="<?php echo $domain ?><?php echo $this->Html->url(array('admin' => false, 'payment' => false, 'controller' => 'items')) ?>">Lihat Koleksi !</a></strong>
<br/>
<br/>
Terima kasih.
<br/>
Aksaramaya.com