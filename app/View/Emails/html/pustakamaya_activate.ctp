Hello <strong><?php echo $user['User']['name'] ?></strong>,
<br/><br/>
We've sent you this email because you signed up for a new account on pustakamaya.aksaramaya.com. If you didn't mean to do that, you can safely ignore this message.
<br/><br/>
Email/Username: <?php echo $user['User']['username'] ?><br/>
Password: (Use your own password)
<br/><br/>

Visit this link to activate your account:
<a href="<?php echo $linkActivate ?>"><?php echo $linkActivate ?></a>
<br/><br/>
If clicking the link doesn't work, copying the complete link address into your browser.
Still having trouble? Contact us at cs@aksaramaya.com.
<br/><br/>
After activation of your account, you can login at http://pustakamaya.aksaramaya.com
<br/><br/>
Enjoy!
<br/><br/>
Aksaramaya Team.