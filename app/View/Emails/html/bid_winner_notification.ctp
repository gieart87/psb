Yth. <strong><?php echo $publisher['Publisher']['name'] ?></strong>, <br/>

Selamat Anda telah memenangkan proses Bidding dengan nilai <strong><?php echo $finalBid;?></strong> untuk buku berikut:
<br/>
Penulis : <?php echo $author['Author']['name'] ?><br/>
Judul : <?php echo $book['Book']['title'] ?><br/>
Kategori : <?php echo $book['Category']['name']; ?><br/>
Jenis Bidding : <?php echo $biddingTypes[$book['Book']['bidding_type']]; ?><br/>


<p>
Buku tersebut telah ditambahkan ke dalam daftar buku Anda.
</p>
<br/>
Terima kasih.
<br/>
Aksaramaya.com
