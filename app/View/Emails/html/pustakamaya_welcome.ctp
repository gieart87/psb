Dear <?php echo $user['Customer']['name'] ?>,
<br/><br/>
Congratulations! You have been registered to pustaka.aksaramaya.com . Please find below your account information:
<br/><br/>
Email/Username: <?php echo $user['User']['username'] ?><br/>
Password: (Use your own password)
<br/><br/>

Feel free to contact our Customer Support (cs@aksaramaya.com) for more information. We look forward to see you at Aksaramaya.com eBookstore. Thank You!
<br/><br/>
Best Regards,
<br/><br/>
Aksaramaya.com