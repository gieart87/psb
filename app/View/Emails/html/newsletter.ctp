Hallo <strong><?php echo $user['User']['username'] ?></strong>, <br/>

Melalui pesan ini kami memberitahukan bahwa ada terbitan terbaru di Aksaramaya dengan informasi sebagai berikut:<br/>
<br/>

<div class="row">
    <div class="span14"> 
        <table class="bordered-table zebra-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Tipe</th>

                </tr>
            </thead>
            <?php if (!empty($newsletters)): ?>
                <?php $no = 1; ?>
                <tbody>
                    <?php foreach ($newsletters as $newsletter): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $this->HelpTablet->getTitleByTypeKey($newsletter['Newsletter']['type'], $newsletter['Newsletter']['key']); ?></td>
                            <td><?php echo $newsletter['Newsletter']['type']; ?></td>
                            <?php if ($newsletter['Newsletter']['type'] == 'Book'): ?>
                                <td><a href="<?php echo $domain ?><?php echo $this->Html->url(array('controller' => 'books', 'action' => 'view', $newsletter['Newsletter']['key'])) ?>">Lihat detail</a></td>
                            <?php endif; ?>
                        </tr>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>

        </table>
    </div>
</div>

<br/>
<br/>
Terima kasih.
<br/>
AksaraMaya.com