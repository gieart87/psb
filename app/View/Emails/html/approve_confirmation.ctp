
Hallo <strong><?php echo $order['User']['username'] ?></strong>,<br/><br/>
Konfirmasi order Anda dengan data berikut telah kami terima dan kami setujui:<br/><br/>

	<strong> Data Konfirmasi </strong><br/>
    Atas Nama : <?php echo $confirmation['Confirmation']['bank_account_name']?><br/>
    Bank Pengirim : <?php echo $confirmation['Confirmation']['sender_bank']?><br/>
    Bank Penerima : <?php echo $confirmation['Confirmation']['receiver_bank']?><br/>
    Jumlah : <?php echo number_format($confirmation['Confirmation']['amount'])?><br/>
    Kode Order : <?php echo $confirmation['Order']['code']?><br/>
    Tanggal Transfer : <?php echo $confirmation['Confirmation']['payment_date']?><br/>
    <br/>
    
    <strong> Data Order</strong><br/>
    <div class="row">
    <div class="span14">

        <p>Kode. Order  :  <?php echo $order['Order']['code'] ?></p>
        <p>Tanggal Order  :  <?php echo $order['Order']['created'] ?></p>
        <p>Batas Waktu Pembayaran :  <?php echo $order['Order']['payment_deadline'] ?></p>
        <p>Customer  :  <?php echo $order['User']['username'] ?></p>
    </div>
</div>

<div class="row">
    <div class="span14"> 
        <table class="bordered-table zebra-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Tipe</th>
                    <th>Harga</th>
                    <th>Qty</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <?php if (!empty($order['OrderDetail'])): ?>
                <?php $no = 1; ?>
                <tbody>
                    <?php foreach ($order['OrderDetail'] as $orderDetail): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $this->HelpTablet->getTitleByTypeKey($orderDetail['type'], $orderDetail['key']); ?></td>
                            <td><?php echo $orderDetail['type']; ?></td>
                            <?php $price = $orderDetail['price'] - ($orderDetail['discount_percent'] / 100 * $orderDetail['price']); ?>
                            <td><?php echo number_format($price) ?></td>
                            <td><?php echo $orderDetail['qty']; ?></td>
                            <td><?php echo number_format($price * $orderDetail['qty']) ?></td>
                        </tr>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>
                        Total
                    </th>
                    <th>
                        <?php echo number_format($order['Order']['total']) ?>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<br/>
    
    

Kami mengucapkan banyak terima kasih telah berbelanja buku digital di aksaramaya.com, semoga bermanfaat.
<br/>
<br/>
Aksaramaya.com

