<div class="row">
    <div class="span14">

        <p>Kode. Order  :  <?php echo $order['Order']['code'] ?></p>
        <p>Tanggal Order  :  <?php echo $order['Order']['created'] ?></p>
        <p>Batas Waktu Pembayaran :  <?php echo $order['Order']['payment_deadline'] ?></p>
        <p>Customer  :  <?php echo $order['User']['username'] ?></p>
    </div>
</div>

<div class="row">
    <div class="span14"> 
        <table class="bordered-table zebra-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Tipe</th>
                    <th>Harga</th>
                    <th>Durasi / Qty</th>
                    <th>Biaya / Sub Total</th>
                </tr>
            </thead>
            <?php if (!empty($order['OrderDetail'])): ?>
                <?php $no = 1; ?>
                <tbody>
                    <?php foreach ($order['OrderDetail'] as $orderDetail): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $this->HelpTablet->getTitleByTypeKey($orderDetail['type'], $orderDetail['key']); ?></td>
                            <td><?php echo $orderDetail['type']; ?></td>
                            <td>
                                <?php if ($orderDetail['type'] != 'Book'): ?>
                                    <?php echo number_format($price) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if ($orderDetail['type'] == 'Book' && !empty($orderDetail['qty_days'])): ?>
                                    <?php echo $orderDetail['qty_days']; ?> bulan
                                <?php else: ?>
                                    <?php echo $orderDetail['qty']; ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if ($orderDetail['type'] == 'Book' && !empty($orderDetail['qty_days'])): ?>
                                    <?php echo number_format($orderDetail['price']); ?>
                                <?php else: ?>
                                    <?php echo number_format($orderDetail['net_price'] * $orderDetail['qty']) ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>
                        Total
                    </th>
                    <th>
                        <?php echo number_format($order['Order']['total']) ?>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="row">
    <div class="span16 biru">
        <p><b>PEMBAYARAN</b></p>
        <p>Silahkan salah satu rekening berikut:<br>
            Bank BCA KCU Serpong - Tangerang No. Rekening 4970809100 a.n. PT Woolu Aksara Maya</p>
        <p><b>KONFIRMASI</b></p>
        <p>* Silahkan  lakukan  konfirmasi  pembayaran  dengan memilih  salah satu cara di bawah ini:<br>
            Konfirmasi  via&nbsp;<a href="<?php echo $domain ?><?php echo $this->Html->url(array('controller' => 'confirmations', 'action' => 'add', $order['Order']['code'])) ?>"><?php echo $this->HelpTablet->getSetting('Main.SiteUrl') . 'confirmations/add/' . $order['Order']['code']; ?></a></p>
        <p>&bull;Kirim SMS ke Nomor 0815  689 2648, ketik [No.  Pesanan]  spasi  [Bank Pengirim]  spasi  [Bank Penerima]  spasi [Tanggal  dan Jam Pembayaran],Contoh:  1234 BCA Mandiri  25-04-2010 14.00 WIB.</p>
        <div style="text-align:right">
            <p>Terima Kasih</p>
            <br />
            <p>Aksaramaya.com</p>
        </div>
    </div>
</div>
