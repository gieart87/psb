Hello <strong><?php echo $user['UserMoco']['username'] ?></strong>,
<br/><br/>
Kami mengirimkan email ini karena Anda telah meminta proses sinkronisasi akun Moco dengan member Epustaka Anda dengan data sebagai berikut:
<br/>
ID Card : <?php echo $user['Student']['nim'] ?><br/>
Library Card : <?php echo $user['Student']['library_card'] ?><br/>
Nama Epustaka : <?php echo $user['Library']['name'] ?><br/>
Jika Anda tidak merasa tidak meminta proses sinkronisasi, abaikan email ini.

<br/><br/>

Kunjungi tautan untuk proses sinkronisasi:
<a href="<?php echo $linkSync ?>"><?php echo $linkSync ?></a>
<br/><br/>
If clicking the link doesn't work, copying the complete link address into your browser.
Still having trouble? Contact us at cs@aksaramaya.com.
<br/><br/>
After activation of your account, you can login at http://www.aksaramaya.com
<br/><br/>
Enjoy!
<br/><br/>
Aksaramaya Team.