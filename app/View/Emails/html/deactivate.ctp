Hello <strong><?php echo $user['User']['name'] ?></strong>,<br/>
Congratulations! You have been registered to Aksaramaya.com eBookstore. Please find below your account information:
<br/><br/>
Email/Username: <?php echo $user['User']['username'] ?><br/>
Password: (Use your own password)
<br/><br/>

Visit this link to deactivate your account:
<a href="<?php echo $linkDeactivate ?>"><?php echo $linkDeactivate ?></a>
<br/><br/>
If clicking the link doesn't work, copying the complete link address into your browser.
Still having trouble? Contact us at cs@aksaramaya.com.
<br/><br/>
Enjoy!
<br/><br/>
Moco.co.id