Yth. <strong><?php echo $author['Author']['username'] ?></strong>, <br/>

Dengan ini memberitahukan bahwa proses bidding terhadap naskah berikut telah selasai dengan nilai Bidding terpilih adalah <strong><?php echo $finalBid;?></strong> :
<br/>
Penulis : <?php echo $author['Author']['name'] ?><br/>
Judul : <?php echo $book['Book']['title'] ?><br/>
Kategori : <?php echo $book['Category']['name']; ?><br/>
Jenis Bidding : <?php echo $biddingTypes[$book['Book']['bidding_type']]; ?><br/>
<p>
<p>
Pemenang bidding naskah Anda adalah <strong><?php echo $publisher['name'];?></strong>. 
</p>
Kami mengucapkan terima kasih atas partisipasi Anda dalam proses bidding ini.

</p>
<br/>
Terima kasih.
<br/>
Aksaramaya.com
