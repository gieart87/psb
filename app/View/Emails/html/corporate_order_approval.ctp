Helo <strong><?php echo $corporateOrder['User']['username']; ?></strong>,<br/>
Selamat, order Anda telah kami proses dan telah valid dengan data berikut:<br/><br/>

Kode : <?php echo $corporateOrder['CorporateOrder']['code']; ?><br/>
Tanggal : <?php echo $this->HelpTablet->humanDate2($corporateOrder['CorporateOrder']['order_date']); ?><br/>
Instansi : <?php echo $this->HelpTablet->getClientNameByUserId($corporateOrder['CorporateOrder']['user_id']); ?><br/>
Jumlah Buku : <?php echo $corporateOrder['CorporateOrder']['num_books']; ?><br/>
Jangka Waktu : <?php echo $corporateOrder['CorporateOrder']['num_years']; ?><br/>
Masa Aktif : <?php echo $this->HelpTablet->humanDate2($corporateOrder['CorporateOrder']['start']);?> - <?php echo $this->HelpTablet->humanDate2($corporateOrder['CorporateOrder']['end']);?><br/>
Total Biaya :  IDR <?php echo number_format($corporateOrder['CorporateOrder']['total_price']); ?><br/>

Mulai saat ini Anda bisa memilih buku-buku di Aksaramaya.com untuk diunduh ke dalam library Anda sejumlah  <strong><?php echo $corporateOrder['CorporateOrder']['num_books']; ?></strong> judul buku.
Selamat menikmati pengalaman yang luar biasa berbelanja buku digital di Aksaramaya.com
<br/>
<br/>
CATATAN : Buku dalam koleksi Anda akan terhapus otomatis ketika masa aktif habis dan tidak dilakukan perpanjangan.
<br/>
Terima kasih.
<br/>
Aksaramaya.com