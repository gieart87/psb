Hallo <strong><?php echo $suggestion['User']['username'] ?></strong>, <br/>

Melalu pesan ini kami memberitahukan bahwa buku yang Anda usulkan, telah dibeli oleh Perpustakaan Anda, Sehingga Anda
dapat meminjamnya sekarang juga. Berikut ini informasi detailnya :<br/>
<br/>

Judul : <?php echo $suggestion['Book']['title'] ?><br/>
ISBN : <?php echo $suggestion['Book']['isbn'] ?><br/>
Penulis : <?php echo $suggestion['Book']['authors'] ?><br/>
Perpustakaan :  <?php echo $suggestion['Library']['name'] ?><br/>
<strong><a href="<?php echo $domain ?><?php echo $this->Html->url(array('admin' => false, 'payment' => false, 'controller' => 'books', 'action' => 'view', $suggestion['Book']['id'])) ?>">Pinjam Sekarang !</a></strong>
<br/>
<br/>
Terima kasih.
<br/>
Aksaramaya.com