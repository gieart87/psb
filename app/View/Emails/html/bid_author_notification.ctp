Yth. <strong><?php echo $author['Author']['name'] ?></strong>, <br/>

Dengan ini memberitahukan bahwa proses bidding terhadap naskah yang Anda upload telah selesai dengan nilai Bidding terpilih adalah <strong><?php echo $finalBid;?></strong> untuk buku berikut:
<br/>
Penulis : <?php echo $author['Author']['name'] ?><br/>
Judul : <?php echo $book['Book']['title'] ?><br/>
Kategori : <?php echo $book['Category']['name']; ?><br/>
Jenis Bidding : <?php echo $biddingTypes[$book['Book']['bidding_type']]; ?><br/>
<p>
<p>
Pemenang bidding naskah Anda adalah <strong><?php echo $publisher['Publisher']['name'];?></strong>. Nashkah Anda akan melalu proses editing oleh penerbit tersebut
sebelum dipublikasikan ke pelanggan.
</p>
Kami mengucapkan selamat dan kami tunggu karya-karya Anda selanjutnya.

</p>
<br/>
Terima kasih.
<br/>
Aksaramaya.com
