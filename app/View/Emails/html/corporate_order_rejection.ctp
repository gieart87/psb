Helo <strong><?php echo $corporateOrder['User']['username']; ?></strong>,<br/>
Kami mohon maaf belum bisa menerima order Anda karena ada beberapa sarat yang belum terpenuhi pada order Anda berikut:<br/><br/>

Kode : <?php echo $corporateOrder['CorporateOrder']['code']; ?><br/>
Tanggal : <?php echo $this->HelpTablet->humanDate2($corporateOrder['CorporateOrder']['order_date']); ?><br/>
Instansi : <?php echo $this->HelpTablet->getClientNameByUserId($corporateOrder['CorporateOrder']['user_id']); ?><br/>
Jumlah Buku : <?php echo $corporateOrder['CorporateOrder']['num_books']; ?><br/>
Jangka Waktu : <?php echo $corporateOrder['CorporateOrder']['num_years']; ?><br/>

Jika Anda pertanyaan jangan sungkan untuk bertanya kepada kontak support kami. Semoga di lain kesempatan Anda lebih beruntung.
<br/>
<br/>
<br/>
Terima kasih.
<br/>
Aksaramaya.com