Dear <?php echo $student['Student']['name'] ?>,<br/><br/>

Congratulations! You have been registered to  <?php echo $student['Client']['name'] ?>. Please find below your account information:<br/><br/>

Name: <?php echo $student['Student']['name'] ?><br/>
Email/Username: <?php echo $student['User']['username'] ?><br/>
Password: (Your own password)<br/>
Fakultas: <?php echo $student['Faculty']['name'] ?><br/>
Program Studi: <?php echo $student['Department']['name'] ?><br/><br/>

Feel free to contact our Customer Support (cs@aksaramaya.com) for more information. We look forward to see you at <?php echo $student['Client']['name'] ?>. Thank You!
<br/><br/>
Best Regards,
<br/><br/>
<a href="<?php echo $student['Client']['main_uri'] ?>"><?php echo $student['Client']['name'] ?></a>
