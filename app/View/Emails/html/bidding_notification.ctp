Yth. <strong><?php echo $publisher['Publisher']['name'] ?></strong>, <br/>

Kami informasikan bahwa ada naskah buku baru yang telah dikirim oleh:
<br/>
Penulis : <?php echo $author['Author']['name'] ?><br/>
Judul : <?php echo $book['Book']['title'] ?><br/>
Kategori : <?php echo $book['Category']['name'];?><br/>
Jenis Bidding : <?php echo $biddingTypes[$book['Book']['bidding_type']];?><br/>


<p>
Untuk melihat draft naskah buku, sinopsis, potensi buku ini, serta mengikuti bidding silakan lihat di panel admin penerbit di Aksaramaya.com dengan login terlebih dahulu.
</p>
<br/>
Terima kasih.
<br/>
Aksaramaya.com
