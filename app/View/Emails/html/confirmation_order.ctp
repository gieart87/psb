<strong>Perhatian !</strong><br/><br/>
Ada konfirmasi pembayaran yang masuk ke AksaraMaya dengan data sebagai berikut:<br/><br/>

    Nama Pelanggan : <?php echo $confirmation['Confirmation']['bank_account_name']?><br/>
    Bank Pengirim : <?php echo $confirmation['Confirmation']['sender_bank']?><br/>
    Bank Penerima : <?php echo $confirmation['Confirmation']['receiver_bank']?><br/>
    Jumlah : <?php echo number_format($confirmation['Confirmation']['amount'])?><br/>
    Kode Order : <?php echo $confirmation['Order']['code']?><br/>
    Tanggal Transfer : <?php echo $confirmation['Confirmation']['payment_date']?><br/>
    

Mohon segera di cek. 
<br/>
Terima kasih.
<br/>
Aksaramaya.com