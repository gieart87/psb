<div class="row">
    <div class="span14">

        <p>Kode. Order  :  <?php echo $order['Order']['code'] ?></p>
        <p>Tanggal Order  :  <?php echo $order['Order']['created'] ?></p>
        <p>Customer  :  <?php echo $order['User']['username'] ?></p>
    </div>
</div>

<div class="row">
    <div class="span14"> 
        <table class="bordered-table zebra-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Judul</th>
                    <th>Tipe</th>
                    <th>Harga</th>
                    <th>Durasi / Qty</th>
                    <th>Biaya / Sub Total</th>
                </tr>
            </thead>
            <?php if (!empty($order['OrderDetail'])): ?>
                <?php $no = 1; ?>
                <tbody>
                    <?php foreach ($order['OrderDetail'] as $orderDetail): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $this->HelpTablet->getTitleByTypeKey($orderDetail['type'], $orderDetail['key']); ?></td>
                            <td><?php echo $orderDetail['type']; ?></td>
                            <td>
                                <?php if ($orderDetail['type'] != 'Book'): ?>
                                    <?php echo number_format($price) ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if ($orderDetail['type'] == 'Book' && !empty($orderDetail['qty_days'])): ?>
                                    <?php echo $orderDetail['qty_days']; ?> bulan
                                <?php else: ?>
                                    <?php echo $orderDetail['qty']; ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php if ($orderDetail['type'] == 'Book' && !empty($orderDetail['qty_days'])): ?>
                                    <?php echo number_format($orderDetail['price']); ?>
                                <?php else: ?>
                                    <?php echo number_format($orderDetail['net_price'] * $orderDetail['qty']) ?>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </tbody>
            <?php endif; ?>
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>
                        Total
                    </th>
                    <th>
                        <?php echo number_format($order['Order']['total']) ?>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<div class="row">
    <div class="span16 biru">

        <div style="text-align:right">
            <p>Terima Kasih</p>
            <br />
            <p>Aksaramaya.com</p>
        </div>
    </div>
</div>
