Helo <strong><?php echo $corporateOrder['User']['username']; ?></strong>,<br/>
Kami telah menerima order dari Anda untuk membeli sejumlah lisensi buku. Order Anda telah kami proses dan berikut ini adalah
detail order Anda.<br/><br/>

Kode : <?php echo $corporateOrder['CorporateOrder']['code']; ?><br/>
Tanggal : <?php echo $this->HelpTablet->humanDate2($corporateOrder['CorporateOrder']['order_date']); ?><br/>
Instansi : <?php echo $this->HelpTablet->getClientNameByUserId($corporateOrder['CorporateOrder']['user_id']); ?><br/>
Jumlah Buku : <?php echo $corporateOrder['CorporateOrder']['num_books']; ?><br/>
Jangka Waktu : <?php echo $corporateOrder['CorporateOrder']['num_years']; ?> tahun<br/>
Total Biaya :  IDR <?php echo number_format($corporateOrder['CorporateOrder']['total_price']); ?><br/>
Deadline Pembayaran : <?php echo $this->HelpTablet->humanDate($corporateOrder['CorporateOrder']['payment_deadline']); ?><br/>

Segera lakukan pembayaran sebelum deadline pembayaran, dan konfirmasikan kepada kami untuk diproses selanjutnya.
<br/>
<br/>
<br/>
Terima kasih.
<br/>
Aksaramaya.com