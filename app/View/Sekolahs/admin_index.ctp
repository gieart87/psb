<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add')) ?>">Tambah Sekolah</a>
    </div>
    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
    </div>
</div>
<br/>
<section class="" id="widget-grid">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->

        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
            <div class="jarviswidget jarviswidget-color-darken" data-widget-editbutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>Daftar Sekolah</h2>

                </header>

                <!-- widget div-->
                <div>

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <div class="widget-body-toolbar">
                        </div>

                        <table id="dt_basic" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>Nama</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (!empty($sekolahs)): ?>
                                    <?php $no = 1; ?>
                                    <?php foreach ($sekolahs as $sekolah): ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $sekolah['Sekolah']['nama'] ?></td>
                                            <td>
                                                <ul class="actions">
                                                    <li>
                                                        <?php echo $this->Html->link(__('<i class="fa fa-edit"></i>'), array('action' => 'edit', $sekolah['Sekolah']['id']), array('escape' => false)); ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        echo $this->Form->postLink(__('<i class="fa fa-trash-o"></i>'), array(
                                                            'action' => 'delete', $sekolah['Sekolah']['id']), array('escape' => false), __('Are you sure you want to delete # %s?', $sekolah['Sekolah']['id'])
                                                        );
                                                        ?>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <?php $no++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->

</section>



<!-- end widget grid -->



<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/jquery.dataTables-cust.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ColReorder.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/FixedColumns.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ColVis.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/ZeroClipboard.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/media/js/TableTools.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/datatables/DT_bootstrap.js"></script>


<script type="text/javascript">

// DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function() {

    pageSetUp();

        /*
        * BASIC
         */
         $('#dt_basic').dataTable({
        "sPaginationType": "bootstrap_full"
            });

        /* END BASIC */

        /* Add the events etc before DataTables hides a column */
        $("#datatable_fixed_column thead input").keyup(function() {
            oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
        });

        $("#datatable_fixed_column thead input").each(function(i) {
            this.initVal = this.value;
            });
        $("#datatable_fixed_column thead input").focus(function() {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
                });
        $("#datatable_fixed_column thead input").blur(function(i) {
            if (this.value == "") {
            this.className = "search_init";
                this.value = this.initVal;
                }
        });


        var oTable = $('#datatable_fixed_column').dataTable({
            "sDom": "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            //"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
            "oLanguage": {
            "sSearch": "Search all columns:"
                },
            "bSortCellsTop": true
            });



            /*
         * COL ORDER
        */
         $('#datatable_col_reorder').dataTable({
        "sPaginationType": "bootstrap",
            "sDom": "R<'dt-top-row'Clf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "fnInitComplete": function(oSettings, json) {
                $('.ColVis_Button').addClass('btn btn-default btn-sm').html('Columns <i class="icon-arrow-down"></i>');
            }
        });

            /* END COL ORDER */

        /* TABLE TOOLS */
        $('#datatable_tabletools').dataTable({
            "sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "oTableTools": {
            "aButtons": ["copy", "print", {
                        "sExtends": "collection",
                        "sButtonText": 'Save <span class="caret" />',
                        "aButtons": ["csv", "xls", "pdf"]
                    }],
                    "sSwfPath": "<?php echo Router::url('/'); ?>js/admin/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
                                "fnInitComplete": function(oSettings, json) {
                                    $(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
                                        $(this).addClass('btn-sm btn-default');
                });
            }
        });

                                /* END TABLE TOOLS */
                            })

</script>

<!-- Your GOOGLE ANALYTICS CODE Below -->
<script type="text/javascript">
//    var _gaq = _gaq || [];
//    _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
//    _gaq.push(['_trackCategoryview']);
//
//    (function() {
//        var ga = document.createElement('script');
//        ga.type = 'text/javascript';
//        ga.async = true;
//        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
//        var s = document.getElementsByTagName('script')[0];
//        s.parentNode.insertBefore(ga, s);
//    })();

</script>



