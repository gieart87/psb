<?php

$this->PhpExcel->createWorksheet();
$this->PhpExcel->setDefaultFont('Calibri', 12);

// define table cells
$table = array(
    array('label' => __('No'), 'width' => 'auto', 'filter' => true),
    array('label' => __('No Pendaftaran'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Nama'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Tempat lahir'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Tanggal lahir'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Jenis kelamin'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Asal Sekolah'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Alamat'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Nilai B. Indonesia'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Nilai B. Inggris'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Nilai Matematika'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Nilai IPA'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Bobot'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Pilihan 1'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Pilihan 2'), 'width' => 'auto', 'filter' => true),
//    array('label' => __('Tgl Daftar'), 'width' => 'auto', 'filter' => true)
);

// heading
$this->PhpExcel->addTableHeader($table, array('name' => 'Cambria', 'bold' => true, 'offset' => 0));
if (!empty($siswas)):
    $no = 1;
    foreach ($siswas as $siswa):
        $bobot = 0;
        $bobot = (4 * $siswa['Siswa']['nilai_matematika']) + (3 * $siswa['Siswa']['nilai_b_inggris']) + (2 * $siswa['Siswa']['nilai_ipa']) + (1 * $siswa['Siswa']['nilai_b_indonesia']);


//        $this->PhpExcel->addTableRow(array(
//            $no,
//            $siswa['Siswa']['no_pendaftaran'],
//            $siswa['Siswa']['nama'],
//            $siswa['Siswa']['tempat_lahir'],
//            $siswa['Siswa']['tgl_lahir'],
//            $kelamins[$siswa['Siswa']['kelamin']],
//            $sekolahs[$siswa['Siswa']['sekolah_id']],
//            $siswa['Siswa']['alamat'],
//            $siswa['Siswa']['nilai_b_indonesia'],
//            $siswa['Siswa']['nilai_b_inggris'],
//            $siswa['Siswa']['nilai_matematika'],
//            $siswa['Siswa']['nilai_ipa'],
//            $bobot,
//            $jurusans[$siswa['Siswa']['pilihan_satu']],
//            $jurusans[$siswa['Siswa']['pilihan_dua']],
//            $siswa['Siswa']['created']
//        ));
        $this->PhpExcel->addTableRow(array(
            $no,
            $siswa['Siswa']['no_pendaftaran'],
//            $siswa['Siswa']['nama'],
//            $siswa['Siswa']['tempat_lahir'],
//            $siswa['Siswa']['tgl_lahir'],
//            $kelamins[$siswa['Siswa']['kelamin']],
//            $sekolahs[$siswa['Siswa']['sekolah_id']],
//            $siswa['Siswa']['alamat'],
//            $siswa['Siswa']['nilai_b_indonesia'],
//            $siswa['Siswa']['nilai_b_inggris'],
//            $siswa['Siswa']['nilai_matematika'],
//            $siswa['Siswa']['nilai_ipa'],
//            $bobot,
//            $jurusans[$siswa['Siswa']['pilihan_satu']],
//            $jurusans[$siswa['Siswa']['pilihan_dua']],
//            $siswa['Siswa']['created']
        ));

        $no++;
    endforeach;
    $this->PhpExcel->addTableRow(array(
        '',
        ''
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        '',
//        ''
    ));

endif;

$this->PhpExcel->addTableFooter();
$this->PhpExcel->output('Rekap-Pendaftar-' . $jurusans[$_GET['jurusan']] . '.xlsx');
?>