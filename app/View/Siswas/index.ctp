
<div class="two-col">
    <?php echo $this->element('public/left'); ?>

    
    <div class="primary">
        <?php if (!empty($events)): ?>
            <?php foreach ($events as $event): ?>
                <div class="post-content clearfix">

                    <div class="thumb">
                        <a href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'read', $event['Event']['permalink'])) ?>">
                            <?php $image = $this->General->getSingleImage('Event', $event['Event']['id']); ?>
                            <?php if (!empty($image)): ?>
                                <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=100&w=100'); ?>
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="desc">
                        <h4 class="title"><a href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'read', $event['Event']['permalink'])) ?>"><?php echo $event['Event']['title'] ?></a></h4>
                        <div class="dates"><?php echo $this->General->humanDate($event['Event']['created']); ?></div>
                        <?php echo $this->General->wordLimiter($event['Event']['description'],20); ?><br/><br/>
                        <!-- dibatesin 160 karakter -->
                        <a href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'read', $event['Event']['permalink'])) ?>" class="more">SELENGKAPNYA</a>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>


        <div class="pagination">
            <?php
		echo $this->Paginator->prev('< ' . __('previous '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__(' next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
        </div>
    </div>

</div> <!-- two-col -->

<?php echo $this->element('public/right'); ?>