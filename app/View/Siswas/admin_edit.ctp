<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">
        <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
            <div data-widget-custombutton="false" data-widget-editbutton="false" id="wid-id-3" class="jarviswidget jarviswidget-sortable" style="" role="widget">
                <header role="heading">
                    <div class="jarviswidget-ctrls" role="menu"> 
                        <a data-placement="bottom" title="" rel="tooltip" class="button-icon jarviswidget-toggle-btn" href="#" data-original-title="Collapse"><i class="fa fa-minus "></i></a> <a data-placement="bottom" title="" rel="tooltip" class="button-icon jarviswidget-fullscreen-btn" href="javascript:void(0);" data-original-title="Fullscreen"><i class="fa fa-resize-full "></i></a> <a data-placement="bottom" title="" rel="tooltip" class="button-icon jarviswidget-delete-btn" href="javascript:void(0);" data-original-title="Delete"><i class="fa fa-times"></i></a></div><div class="widget-toolbar" role="menu"><a href="javascript:void(0);" class="dropdown-toggle color-box selector" data-toggle="dropdown"></a><ul class="dropdown-menu arrow-box-up-right color-select pull-right"><li><span data-original-title="Green Grass" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-green" class="bg-color-green"></span></li><li><span data-original-title="Dark Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenDark" class="bg-color-greenDark"></span></li><li><span data-original-title="Light Green" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-greenLight" class="bg-color-greenLight"></span></li><li><span data-original-title="Purple" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-purple" class="bg-color-purple"></span></li><li><span data-original-title="Magenta" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-magenta" class="bg-color-magenta"></span></li><li><span data-original-title="Pink" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-pink" class="bg-color-pink"></span></li><li><span data-original-title="Fade Pink" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-pinkDark" class="bg-color-pinkDark"></span></li><li><span data-original-title="Light Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueLight" class="bg-color-blueLight"></span></li><li><span data-original-title="Teal" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-teal" class="bg-color-teal"></span></li><li><span data-original-title="Ocean Blue" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blue" class="bg-color-blue"></span></li><li><span data-original-title="Night Sky" data-placement="top" rel="tooltip" data-widget-setstyle="jarviswidget-color-blueDark" class="bg-color-blueDark"></span></li><li><span data-original-title="Night" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-darken" class="bg-color-darken"></span></li><li><span data-original-title="Day Light" data-placement="left" rel="tooltip" data-widget-setstyle="jarviswidget-color-yellow" class="bg-color-yellow"></span></li><li><span data-original-title="Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orange" class="bg-color-orange"></span></li><li><span data-original-title="Dark Orange" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-orangeDark" class="bg-color-orangeDark"></span></li><li><span data-original-title="Red Rose" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-red" class="bg-color-red"></span></li><li><span data-original-title="Light Red" data-placement="bottom" rel="tooltip" data-widget-setstyle="jarviswidget-color-redLight" class="bg-color-redLight"></span></li><li><span data-original-title="Purity" data-placement="right" rel="tooltip" data-widget-setstyle="jarviswidget-color-white" class="bg-color-white"></span></li><li><a data-original-title="Reset widget color to default" data-placement="bottom" rel="tooltip" data-widget-setstyle="" class="jarviswidget-remove-colors" href="javascript:void(0);">Remove</a></li></ul>
                    </div>
                    <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                    <h2>Tambah Pendaftar</h2>				
                    <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span>
                </header>

                <!-- widget div-->
                <div role="content">

                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->

                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <!--<form novalidate="novalidate" class="smart-form" id="order-form">-->
                        <?php echo $this->Form->create('Siswa', array('type' => 'file', 'class' => 'smart-form', 'id' => 'order-form')); ?>
                        <?php echo $this->Form->hidden('id'); ?>
                        <fieldset>
                            <legend>NO Pendaftaran dan Pilihan Jurusan</legend>
                            <div class="row">
                                <section class="col col-4">
                                    <label class="label">Nomor Pendaftaran</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('no_pendaftaran', array('placeholder' => 'nomor pendaftaran', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="label">Pilihan Satu</label>
                                    <label class="select">
                                        <?php echo $this->Form->input('pilihan_satu', array('class' => 'select2', 'options' => $jurusans, 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="label">Pilihan Dua</label>
                                    <label class="select">
                                        <?php echo $this->Form->input('pilihan_dua', array('class' => 'select2', 'options' => $jurusans, 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>

                            </div>
                            <div class="row">

                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Identitas Calon Siswa</legend>
                            <div class="row">
                                <section class="col col-2">
                                    <label class="label">Jenis Kelamin</label>
                                    <label class="select">
                                        <?php echo $this->Form->input('kelamin', array('empty' => array(0 => 'Jenis Kelamin'), 'class' => 'number', 'min' => 1, 'options' => $kelamins, 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                     <section class="col col-3">
                                    <label class="label">Agama</label>
                                    <label class="select">
                                        <?php
                                        $agamas = array(
                                            'Islam' => 'Islam',
                                            'Kristen' => 'Kristen',
                                            'Katolik' => 'Katolik',
                                            'Hindu' => 'Hindu',
                                            'Budha' => 'Budha',
                                            'Konghucu' => 'Konghucu'
                                        );
                                        ?>
                                        <?php echo $this->Form->input('agama', array('empty' => array(0 => 'Agama'), 'class' => 'select2','options' => $agamas, 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Asal Sekolah (<a href="#"  data-toggle="modal" data-target="#myModal">tambah list asal sekolah</a>)</label>
                                    <label class="select">
                                        <?php echo $this->Form->input('sekolah_id', array('class' => 'select2', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-4">
                                    <label class="label">Alamat</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('alamat', array('placeholder' => 'alamat', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Jenis Kelamin</label>
                                    <label class="select">
                                        <?php echo $this->Form->input('kelamin', array('empty' => array(0 => 'Jenis Kelamin'), 'class' => 'number', 'min' => 1, 'options' => $kelamins, 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Asal Sekolah (<a href="#"  data-toggle="modal" data-target="#myModal">tambah list asal sekolah</a>)</label>
                                    <label class="select">
                                        <?php echo $this->Form->input('sekolah_id', array('class' => 'select2', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">Alamat</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('alamat', array('placeholder' => 'alamat', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Nilai Mapel UAN</legend>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Bahasa Indonesia</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('nilai_b_indonesia', array('class' => 'number required', 'min' => 0, 'max' => 10.00, 'placeholder' => 'Bahasa Indonesia', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Bahasa Inggris</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('nilai_b_inggris', array('class' => 'number required', 'min' => 0, 'max' => 10.00, 'placeholder' => 'Bahasa Inggris', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Matematika</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('nilai_matematika', array('class' => 'number required', 'min' => 0, 'max' => 10.00, 'placeholder' => 'Matematika', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                                <section class="col col-3">
                                    <label class="label">Ilmu Pengetahuan Alam</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('nilai_ipa', array('class' => 'number required', 'min' => 0, 'max' => 10.00, 'placeholder' => 'Ilmu Pengetahuan Alam', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>

                        </fieldset>
                        <fieldset>
                            <legend>Status</legend>
                            <div class="row">
                                <section class="col col-3">
                                    <label class="label">Status</label>
                                    <label class="input">
                                        <?php echo $this->Form->input('status', array('options' => $status, 'placeholder' => 'Ilmu Pengetahuan Alam', 'div' => FALSE, 'label' => FALSE)); ?>
                                    </label>
                                </section>
                            </div>
                        </fieldset>
                        <footer>
                            <button class="btn btn-primary" type="submit">
                                Save
                            </button>
                            <button class="btn btn-warning" type="button" onclick="history.back();">
                                Back
                            </button>
                        </footer>
                        <?php echo $this->Form->end; ?>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
        </article>

    </div>

</section>
<!-- end widget grid -->

<!-- Tambah asal sekolah -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                    ×
                </button>
                <h4 id="myModalLabel" class="modal-title">Tambah Asal Sekolah</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" id="nama_sekolah" placeholder="Nama Sekolah" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-default" type="button">
                    Batal
                </button>
                <button class="btn btn-primary button-tambah-sekolah" type="button">
                    Simpan
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

<script type="text/javascript">
                                $(document).ready(function() {
                                    $('#timepicker').timepicker({
                                        showMeridian: false,
                                    });

                                    $('.select2').each(function() {
                                        var $this = $(this);
                                        var width = $this.attr('data-select-width') || '100%';
                                        //, _showSearchInput = $this.attr('data-select-search') === 'true';
                                        $this.select2({
                                            //showSearchInput : _showSearchInput,
                                            allowClear: true,
                                            width: width
                                        })
                                    });

                                    $("#order-form").validate();
                                });
</script>

<script type="text/javascript">
    $('.translate').click(function() {
        var title = $('#EventTitle').val();
        $.ajax({
            type: 'GET',
            url: SERVER + 'langs/translate?text=' + title,
            success: function(response) {
                $('#EventTitleEn').val(response);


                var body = tinymce.get('EventDetails').getContent();
                body = body.replace(/(<([^>]+)>)/ig, "");
                $.ajax({
                    type: 'GET',
                    url: SERVER + 'langs/translate?text=' + body,
                    success: function(response) {
                        tinymce.get('EventDetailsEn').setContent(response);
                    }
                });


            }
        })
    });

    $('.button-tambah-sekolah').click(function() {
        var namaSekolah = $('#nama_sekolah').val();
        $.ajax({
            type: 'POST',
            url: SERVER + 'admin/sekolahs/add',
            data: {
                nama: namaSekolah
            },
            success: function(response) {
                if (response.meta.code == 200) {
                    window.location.href = window.location.href;
                }
            }
        })
    });
</script>