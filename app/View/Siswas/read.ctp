<div class="one-col">

    <div class="primary">
        <div class="dates"><?php echo $this->General->humanDate($event['Event']['created']); ?></div>
        <h2 class="title"><?php echo $event['Event']['title'] ?></h2>
        <div class="post-detail clearfix">
            <?php $image = $this->General->getSingleImage('Event', $event['Event']['id']); ?>
            <?php if (!empty($image)): ?>
                <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=199&w=260'); ?>
            <?php endif; ?>
            <p>
                Tanggal : <strong><?php echo $this->General->humanDate2($event['Event']['date']);?></strong><br/>
                Waktu  : <strong><?php echo $event['Event']['time'];?></strong><br/>
                Lokasi  : <strong><?php echo $event['Event']['location']?></strong><br/>
                Deskripsi :
            </p>
            <?php echo $event['Event']['description']; ?>
        </div>
         <div class="fb-comments" data-href="<?php echo $this->Html->url(array('controller' => 'events', 'action' => 'read', $event['Event']['permalink'])) ?>" data-num-posts="5" data-width="700"></div>

<!--        <h3>Komentar</h3>
        <table class="testi-box">
            <tr>
                <td class="col_20 tL">&nbsp;</td>
                <td class="ballon-T"></td>
                <td class="col_20 tR">&nbsp;</td>
            </tr>
            <tr>
                <td class="ballon-L">&nbsp;</td>
                <td>
                    <span class="testi-msg">
                        Demikian pula, tidak adakah orang yang mencintai atau mengejar atau ingin mengalami penderitaan, bukan semata-mata karena penderitaan itu sendiri
                    </span>
                </td>
                <td class="ballon-R">&nbsp;</td>
            </tr>
            <tr class="btm-box">
                <td class="bL">&nbsp;</td>
                <td class="bT"></td>
                <td class="bR">&nbsp;</td>
            </tr>
        </table>
        <strong>Chandra</strong>, <em>17 years</em><br />
        <em>Male, Australia</em>
        <hr />

        <div class="pagination">
            <ul class="clearfix">
                <li><a href="#">&laquo; prev</a></li>
                <li><a href="#" class="curent">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">next &raquo;</a></li>
            </ul>
        </div>



        <h3>Tulis Komentar</h3>
        <div class="row20 formfields box">
            <form action="">
                <fieldset>
                    <p>(<span class="required">*</span><em>Wajib diisi!</em>)</p>
                    <div class="row10 clearfix">
                        <div class="labelfields"><label for="nama"><span class="required">*</span>Nama Lengkap:</label></div>
                        <div class="inputfields"><input type="text" id="nama" size="30" /></div>
                    </div>
                    <div class="row10 clearfix">
                        <div class="labelfields"><label for="email">Email:</label></div>
                        <div class="inputfields"><input type="text" id="email" size="30" /></div>
                    </div>
                    <div class="row10 clearfix">
                        <div class="labelfields"><label for="alamat">Alamat:</label></div>
                        <div class="inputfields"><textarea id="alamat" cols="40"></textarea></div>
                    </div>
                    <div class="row10 clearfix">
                        <div class="labelfields"><label for="testi">Testimoni:</label></div>
                        <div class="inputfields"><textarea id="testi" cols="40" rows="6"></textarea></div>
                    </div>
                    <div class="row20 clearfix">
                        <div class="labelfields">&nbsp;</div>
                        <div class="inputfields">
                            <img src="#" /><br />
                            <label for="captcha">Masukkan kode di atas</label><br />
                            <input type="text" id="captcha" />
                        </div>
                    </div>
                    <div class="row10 clearfix">
                        <div class="labelfields"><label>&nbsp;</label></div>
                        <div class="inputfields"><input type="submit" value="Submit" /></div>
                    </div>
                </fieldset>
            </form>
        </div>-->
    </div>

</div> <!-- two-col -->

<?php echo $this->element('public/right'); ?>