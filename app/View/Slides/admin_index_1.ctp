<script type="text/javascript">
    /*Table book list*/
    $(function() {
        otTable = $('#table').dataTable({
            "sDom": "<'row-fluid'<'span7'l><'span5'f>r>t<'row-fluid'<'span4'i><'span8 pull-right'p>>",
            "sPaginationType": "bootstrap"
        });

        otTable.fnSort([[0, 'desc']]);

        $('.dataTables_filter').addClass("pull-right");
        $('[name="table_length"]').css("width", "55px");
    })
</script>
<div class="main">
    <div class="main-inner">
        <div class="container" style="margin-top: 10px;">
            <div class="container-fluid">

                <?php echo $this->element('breadcrumb'); ?>
                <div class="row-fluid">
                    <div class="span12">
                        <?php echo $this->Html->link(__('+ New Slide'), array('action' => 'add'), array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <br/>
                <div class="row-fluid">
                    <div class="span12">

                        <table cellpadding="0" cellspacing="0" border="0" id="table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Slide</th>
                                    <th>Title</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th width="75px">Update</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Slide</th>
                                    <th>Title</th>
                                    <th>Position</th>
                                    <th>Status</th>
                                    <th width="75px">Update</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php foreach ($slides as $slide): ?>
                                    <tr>
                                        <td><?php echo h($slide['Slide']['id']); ?>&nbsp;</td>
                                        <?php $image = $this->General->getSingleImage('Slide', $slide['Slide']['id']); ?>
                                        <td>
                                            <?php if (!empty($image)): ?>
                                                <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=100&w=100'); ?>
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo h($slide['Slide']['title']); ?>&nbsp;</td>
                                        <td>
                                            <?php if ($this->General->isExistPrevSlide($slide['Slide']['position'])): ?>
                                                <?php echo $this->Html->link('up', array('action' => 'up', $slide['Slide']['position'])); ?>
                                            <?php else: ?>
                                                up
                                            <?php endif; ?>
                                            |
                                            <?php if ($this->General->isExistNextSlide($slide['Slide']['position'])): ?>
                                                <?php echo $this->Html->link('down', array('action' => 'down', $slide['Slide']['position'])); ?>
                                            <?php else: ?>
                                                down
                                            <?php endif; ?>

                                            &nbsp;
                                        </td>
                                        <td><?php echo h($status[$slide['Slide']['status']]); ?>&nbsp;</td>

                                        <td class="actions">
                                            <div class="btn-group">
                                                <a class="btn btn-primary" href="#">Aksi</a>
                                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                                <ul class="dropdown-menu">
                                                    <li><?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $slide['Slide']['id'])); ?></li>
                                                    <li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $slide['Slide']['id']), null, __('Are you sure you want to delete # %s?', $slide['Slide']['id'])); ?></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div> 
                </div>

            </div> <!-- /container -->

        </div> <!-- /main-inner -->

    </div> <!-- /main -->
</div>
