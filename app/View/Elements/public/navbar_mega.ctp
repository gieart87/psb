<!--Navbar-->
<section id="pav-mainnav">
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Navbar -->
        <?php
        $menus = $this->Tree->getMenus();

        if ($this->Session->read('lang') == 'EN'):
            foreach ($menus as $key => $val) {
                $menus[$key]['Menu']['name'] = $val['Menu']['name_en'];
                $menus[$key]['Menu']['url'] = $val['Menu']['url_en'];
                if (!empty($menus[$key]['children'])) {
                    foreach ($menus[$key]['children'] as $id => $menu) {
                        $menus[$key]['children'][$id]['Menu']['name'] = $menu['Menu']['name_en'];
                        $menus[$key]['children'][$id]['Menu']['url'] = $menu['Menu']['url_en'];
                        if (!empty($menus[$key]['children'][$id]['children'])) {
                            foreach ($menus[$key]['children'][$id]['children'] as $x => $value) {
                                $menus[$key]['children'][$id]['children'][$x]['Menu']['name'] = $value['Menu']['name_en'];
                                $menus[$key]['children'][$id]['children'][$x]['Menu']['url'] = $value['Menu']['url_en'];
                            }
                        }
                    }
                }
            }

        endif;
        ?>
        <?php if (!empty($menus)): ?>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php foreach ($menus as $menu): ?>
                        <?php if (empty($menu['children'])): ?>
                            <li><a href="<?php echo Router::url('/') . $menu['Menu']['url'] ?>"><?php echo $menu['Menu']['name']; ?></a></li>
                        <?php else: ?>  
                            <li class="dropdown">
                                <a href="<?php echo Router::url('/') . $menu['Menu']['url'] ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $menu['Menu']['name']; ?><b class="caret"></b></a>
                                <ul class="dropdown-menu animated fadeInUp" role="menu">
                                    <?php
                                    $leftChildOne = array();
                                    $rightChildOne = array();
                                    $itemNumber = (int) (count($menu['children']) / 2);
                                    ?>
                                    <?php if ($itemNumber < 2): ?>
                                        <?php foreach ($menu['children'] as $childOne): ?>
                                            <li><a href="<?php echo Router::url('/') . $childOne['Menu']['url'] ?>"><?php echo $childOne['Menu']['name']; ?></a></li>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php
                                        $no = 1;
                                        ?>
                                        <?php foreach ($menu['children'] as $childOne): ?>
                                            <?php
                                            if ($no <= $itemNumber) {
                                                $leftChildOne[] = $childOne;
                                            } else {
                                                $rightChildOne[] = $childOne;
                                            }
                                            $no++;
                                            ?>
                                        <?php endforeach; ?>

                                        <div class="col-md-12">
                                            <?php if (!empty($leftChildOne)): ?>
                                                <div class="col-md-6">
                                                    <ul class="garis-kiri">
                                                        <?php foreach ($leftChildOne as $left): ?>
                                                            <li><a href="<?php echo Router::url('/') . $left['Menu']['url'] ?>"><?php echo $left['Menu']['name']; ?></a></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                            <?php if (!empty($rightChildOne)): ?>
                                                <div class="col-md-6">
                                                    <ul class="garis-kiri">
                                                        <?php foreach ($rightChildOne as $right): ?>
                                                            <li><a href="<?php echo Router::url('/') . $right['Menu']['url'] ?>"><?php echo $right['Menu']['name']; ?></a></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>

                                </ul>  
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>

            </div><!-- /.navbar-collapse -->
        <?php endif; ?>
    </nav>    
</section>