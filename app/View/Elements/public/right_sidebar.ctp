<!-- Right Sidebar -->
<div class="col-md-3">
    <?php if (!empty($widgets)): ?>
        <?php
        foreach ($widgets as $key => $value) {
            echo $this->element('public/Widgets/' . $value);
        }
        ?>
    <?php else: ?>
        <div class="no-widget-box">
            There is no widget available
        </div>
    <?php endif; ?>

</div>
<!-- End Right Sidebar -->