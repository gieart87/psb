<?php $documents = $this->General->getDocumentCategoriesByParent(isset($documents[0]['DocumentCategory']['parent_id']) ? $documents[0]['DocumentCategory']['parent_id'] : NULL); ?>
<!-- Posts -->
<div class="posts margin-bottom-40">
    <div class="headline headline-md"><h2><?php echo $this->General->lang('document_categories') ?></h2></div>
    <?php if (!empty($documents)): ?>
        <ul class="other-pages">
            <?php foreach ($documents as $document): ?>
                <li> 
                    <a href="<?php echo $this->Html->url(array('controller' => 'documents', 'action' => 'category', $document['DocumentCategory']['id'])); ?>"><?php echo $document['DocumentCategory']['name'] ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

</div><!--/posts-->
<!-- End Posts -->
