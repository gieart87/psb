<!-- Search -->
<div class="headline headline-md"><h2><?php echo $this->General->lang('search_label'); ?></h2></div>
<div class="margin-bottom-40">
    <?php
    echo $this->Form->create('Article', array(
        'url' => array('action' => 'search'),
        'class' => 'input-group'
    ));
    ?>
    <input type="text" class="form-control" name="data[Article][q]" placeholder=" <?php echo $this->General->lang('search_keyword') ?>">
    <span class="input-group-btn">
        <button class="btn-u" type="button"><i class="fa fa-search"></i></button>
    </span>
    <?php echo $this->Form->end(); ?>
</div>