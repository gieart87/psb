<!-- Tag -->
<div class="headline headline-md"><h2>Blog Tags</h2></div>
<ul class="list-unstyled blog-tags margin-bottom-30">
    <li><a href="#"><i class="fa fa-tags"></i> Imigrasi</a></li>
    <li><a href="#"><i class="fa fa-tags"></i> Dalam negeri</a></li>
    <li><a href="#"><i class="fa fa-tags"></i> Luar negeri</a></li>
    <li><a href="#"><i class="fa fa-tags"></i> Lokal</a></li>
    <li><a href="#"><i class="fa fa-tags"></i> Jogja</a></li>
    <li><a href="#"><i class="fa fa-tags"></i> Barang bukti</a></li>
    <li><a href="#"><i class="fa fa-tags"></i> Organisasi</a></li>
    <li><a href="#"><i class="fa fa-tags"></i> Karyawan</a></li>
</ul>
<!-- End Tag -->