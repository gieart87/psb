<?php $articles = $this->General->getLatestArticles(4); ?>
<!-- Posts -->
<div class="posts margin-bottom-40">
    <div class="headline headline-md"><h2><?php echo $this->General->lang('recent_posts'); ?></h2></div>
    <?php if (!empty($articles)): ?>
        <?php foreach ($articles as $article): ?>
            <dl class="dl-horizontal">
                <dt><a href="<?php echo $this->Html->url(array('controller' => 'articles', 'action' => 'read', $article['Article']['permalink'])); ?>">
                    <?php $image = $this->General->getSingleImage('Article', $article['Article']['id']); ?>

                    <?php if (!empty($image) && file_exists('img/' . $image['Image']['large'])): ?>
                        <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=60&w=60', array('class' => 'img-responsive')); ?>
                    <?php else: ?>
                        <img src="http://placehold.it/60x60"/>
                    <?php endif; ?>
                </a></dt>
                <dd>
                    <p><a href="<?php echo $this->Html->url(array('controller' => 'articles', 'action' => 'read', $article['Article']['permalink'])); ?>"><?php echo $article['Article']['title'] ?></a></p> 
                </dd>
            </dl>
        <?php endforeach; ?>
    <?php endif; ?>

</div><!--/posts-->
<!-- End Posts -->
