<?php $pages = $this->General->getPagesInCategory($page['Page']['page_category_id']); ?>
<!-- Posts -->
<div class="posts margin-bottom-40">
    <div class="headline headline-md"><h2><?php echo $this->General->lang('pages_other'); ?></h2></div>
    <?php if (!empty($pages)): ?>
        <ul class="other-pages">
            <?php foreach ($pages as $page): ?>
                <li> 
                    <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'read', $page['Page']['permalink'])); ?>"><?php echo $page['Page']['title'] ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

</div><!--/posts-->
<!-- End Posts -->
