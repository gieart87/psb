<?php $officialProfiles = $this->General->getOfficialProfiles(); ?>
<!-- Posts -->
<div class="posts margin-bottom-40">
    <div class="headline headline-md"><h2><?php echo $this->General->lang('official_profile') ?></h2></div>
    <?php if (!empty($officialProfiles)): ?>
        <?php foreach ($officialProfiles as $officialProfile): ?>
            <dl class="dl-horizontal">
                <dt><a href="<?php echo $this->Html->url(array('controller' => 'official_profiles', 'action' => 'view', $officialProfile['OfficialProfile']['id'])); ?>">
                    <?php $image = $this->General->getSingleImage('OfficialProfile', $officialProfile['OfficialProfile']['id']); ?>

                    <?php if (!empty($image) && file_exists('img/' . $image['Image']['large'])): ?>
                        <?php echo $this->Html->image('timthumb.php?src=' . $image['Image']['large'] . '&h=60&w=60', array('class' => 'img-responsive')); ?>
                    <?php else: ?>
                        <img src="http://placehold.it/60x60"/>
                    <?php endif; ?>
                </a></dt>
                <dd>
                    <p><a href="<?php echo $this->Html->url(array('controller' => 'official_profiles', 'action' => 'view', $officialProfile['OfficialProfile']['id'])); ?>"><?php echo $officialProfile['OfficialProfile']['name'] ?></a></p> 
                </dd>
            </dl>
        <?php endforeach; ?>
    <?php endif; ?>

</div><!--/posts-->
<!-- End Posts -->
