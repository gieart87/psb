<div class="headline headline-md"><h2><?php echo $this->General->lang('galleries_list'); ?></h2></div>
<?php $galleries = $this->General->getGalleries(); ?>
<?php if (!empty($galleries)): ?>
    <ul class="galleries">
        <?php foreach ($galleries as $gallery): ?>
            <li><a href="<?php echo $this->Html->url(array('controller' => 'galleries', 'action' => 'index', $gallery['Gallery']['permalink'])); ?>"><?php echo $gallery['Gallery']['name'] ?></a></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>