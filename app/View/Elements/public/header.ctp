<!--=== Top ===-->    
<div class="top">
    <div class="container"> 
        <ul class="loginbar pull-left">
            <li><a href=""><span>Hotline <?php echo $this->General->getSetting('Site.hotlineNumber'); ?></span></a></li>   
        </ul>        
        <ul class="loginbar pull-right">  
            <li class="aktif"><a href="?lang=ID">Indonesia</a></li>  
            <li class="devider"></li>   
            <li><a href="?lang=EN">English</a></li>   
        </ul>
    </div>      
</div><!--/top-->
<!--=== End Top ===-->    

<!--=== Header ===-->    
<div class="header">
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <a class="navbar-brand" href="<?php echo Router::url('/'); ?>">
                        <img class="logo-header" src="<?php echo Router::url('/'); ?>img/public/logo.png" alt="Logo">
                    </a>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <a href="#" class="navbar-brand">
                        <h3><?php echo $this->General->lang('header_title_1'); ?></h3>
                        <h4><?php echo $this->General->lang('header_title_2'); ?></h4>
                        <h2><?php echo $this->General->lang('header_title_3'); ?></h2>
                    </a>
                </div>
            </div>
        </div>    
    </div>    
</div><!--/header-->
<!--=== End Header ===-->    
<?php echo $this->element('public/navbar'); ?>