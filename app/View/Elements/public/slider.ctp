<?php $slides = $this->General->getSlides(); ?>
<!--=== Slider == -->
<?php if (!empty($slides)): ?>
    <div class="container">
        <div class="row">
            <div id="carousel-example-captions" class="carousel slide animated bounceInRight" data-ride="carousel">
                <div class="carousel-inner">
                    <?php $no = 1; ?>
                    <?php foreach ($slides as $slide): ?>
                        <?php
                        $class = '';
                        if ($no == 1) {
                            $class = 'active';
                        }
                        ?>
                        <div class="item <?php echo $class; ?> gambar animated FlipOut" style="background-image: url(<?php echo Router::url('/') . 'img/' . $slide['Image']['large']; ?>);">
                            <div class="carousel-caption">
                                <h3><?php echo $slide['Slide']['title'] ?></h3>
                                <?php if (!empty($slide['Slide']['description'])): ?>
                                    <div class="desc"><?php echo $slide['Slide']['description'] ?></div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php $no++; ?>
                    <?php endforeach; ?>
                </div>
                <a class="left carousel-control" href="#carousel-example-captions" data-slide="prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a class="right carousel-control" href="#carousel-example-captions" data-slide="next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div>
    </div>
<?php endif; ?>
<!--=== End Slider ===-->