<div class="bigboobs">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 style="font-family:'roboto_condensedregular';letter-spacing:2px;margin-top:100px;margin-bottom:30px;">TOKO BUKU DIGITAL MOCO</h1>
                <p style="font-weight:0;font-size:2em;margin-bottom:50px;">Membaca buku digital Indonesia pada platform media sosial.</p>
                <p>Go to apps >></p>
            </div>
            <div class="col-md-1">
            </div>
            <div class="col-md-4">
                <h3 class="title-content">Sign Up</h3>
                <p>Create your account.</p>
                <!--<form role="form" method="post" id="signup">-->
                <?php echo $this->Form->create('User', array('action' => 'register', 'id' => 'signup')); ?>
                <div class="form-group">
                    <input type="text" class="form-control" id="name" name="User[name]" placeholder="Full Name" required>
                </div>
                <div class="form-group">
                    <input type="email" id="email" name="User[username]" class="form-control" placeholder="Your email" required>
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="User[password]" class="form-control" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <input type="password" id="repassword" name="User[confirm_password]" class="form-control" placeholder="Retype Password" required>
                </div>
                <div class="form-group">
                    <script type="text/javascript">
                        var RecaptchaOptions = {
                            theme: 'custom',
                            custom_theme_widget: 'recaptcha_widget',
                            lang: 'en'
                        };
                    </script>
                    <div id="recaptcha_widget" style="display:none">

                        <div id="recaptcha_image"></div>
                        <div class="recaptcha_only_if_incorrect_sol" style="color:red">Incorrect please try again</div>

                        <span class="recaptcha_only_if_image">Enter the words above or &nbsp;</span>
                        <span class="recaptcha_only_if_audio">Enter the numbers you hear or &nbsp;</span>
                        <a href="javascript:Recaptcha.reload()" title="Get new captcha"> <i class="icon-refresh"></i> refresh</a>&nbsp;&nbsp;
                        <a href="javascript:Recaptcha.switch_type('audio')" class="recaptcha_only_if_image" title="Get audio captcha"><i class="icon-volume-up"></i>Audio</a>&nbsp;
                        <a href="javascript:Recaptcha.switch_type('image')" class="recaptcha_only_if_audio" title="Get image captcha"><i class="icon-picture"></i>Image</a>&nbsp;&nbsp;
                        <a href="javascript:Recaptcha.showhelp()" title="Help?" ><i class="icon-question-sign"></i></a>&nbsp;
                        <input type="text" id="recaptcha_response_field" name="data[User][recaptcha_response_field]" class="required" placeholder="Type here" />
                        <?php echo $this->Recaptcha->error(); ?>
                    </div>

                    <script type="text/javascript"
                            src="http://www.google.com/recaptcha/api/challenge?k=6Led1eESAAAAACFOPJswOenwDbD8U9Nvg-ZsDa-v">
                    </script>
                    <noscript>
                    <iframe src="http://www.google.com/recaptcha/api/noscript?k=6Led1eESAAAAACFOPJswOenwDbD8U9Nvg-ZsDa-v"
                            height="300" width="500" frameborder="0"></iframe><br>
                    <textarea name="recaptcha_challenge_field" rows="3" cols="40">
                    </textarea>
                    <input type="hidden" name="recaptcha_response_field"
                           value="manual_challenge">
                    </noscript>
                </div>

                <button type="submit" class="btn btn-ak-red pull-right" id="btn-signup"><i id="signup-proc" class=""></i> Submit</button>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>