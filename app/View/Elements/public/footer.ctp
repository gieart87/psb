<!--=== Footer ===-->
<!--Footer-->
<section id="footer">
    <div class="footer-center">
        <div class="container">
            <div class="row">
                <div class="column col-xs-12 col-sm-6 col-lg-3">
                    <div class="box">
                        <div class="box-heading"><span> Profil</span></div>
                        <ul class="list">
                            <li><a href="<?php echo Router::url('/'); ?>pages/read/tentang-kami"><?php echo $this->General->lang('about_me'); ?></a></li>
                            <li><a href="<?php echo Router::url('/'); ?>pages/structure"><?php echo $this->General->lang('org_structure'); ?></a></li>
                            <li><a href="<?php echo Router::url('/'); ?>pages/read/sejarah-imigrasi"><?php echo $this->General->lang('org_history'); ?></a></li>
                            <li><a href="<?php echo Router::url('/'); ?>head_profiles"><?php echo $this->General->lang('period_head_profile'); ?></a></li>
                            <li><a href="<?php echo Router::url('/'); ?>pages/read/visi-dan-misi"><?php echo $this->General->lang('vission_mission'); ?></a></li>
                            <li><a href="<?php echo Router::url('/'); ?>pages/read/tugas-pokok-dan-fungsi"><?php echo $this->General->lang('primary_task_function'); ?></a></li>
                        </ul>
                    </div>
                </div>

                <div class="column col-xs-12 col-sm-6 col-lg-3">
                    <div class="box">
                        <div class="box-heading"><span> Fitur</span></div>
                        <ul class="list">
                            <li><a href="<?php echo Router::url('/'); ?>questions"><?php echo $this->General->lang('ikm_label'); ?></a></li>
                            <li><a href="<?php echo Router::url('/'); ?>galleries"><?php echo $this->General->lang('galleries'); ?></a></li>
                            <li><a href="<?php echo Router::url('/'); ?>full_calendar"><?php echo $this->General->lang('event_calendar'); ?></a></li>
                        </ul>
                    </div>
                </div>

                <div class="column col-xs-12 col-sm-6 col-lg-3">
                    <div class="box">
                        <div class="box-heading">
                            <span> Follow Us</span>
                        </div>
                        <div class="social">
                            <ul>
                                <a class="animated FlipOutY" href="<?php echo $this->General->getSetting('Site.twitterUrl'); ?>"><img src="<?php echo Router::url('/'); ?>img/public/icn-twitter.png"></a>
                                <a href="<?php echo $this->General->getSetting('Site.facebookUrl'); ?>"><img src="<?php echo Router::url('/'); ?>img/public/icn-facebook.png"></a>
                            </ul>

                        </div>
                    </div>
                </div>

                <div class="column col-xs-12 col-sm-6 col-lg-3">
                    <div class="box contact-us">
                        <div class="box-heading">
                            <span><span> Alamat</span> Kantor</span>
                        </div>
                        <p>
                            <?php echo $this->General->getSetting('Site.officeAddress'); ?><br><br>
                            Phone. <?php echo $this->General->getSetting('Site.hotlineNumber'); ?><br>
                            Fax. <?php echo $this->General->getSetting('Site.officeFax'); ?><br>
                            e-Mail. <?php echo $this->General->getSetting('Site.officeEmail'); ?><br>
                        </p>
                    </div>

                </div>
            </div> 
        </div>
    </div>

    <div id="powered">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                     
                    <p class="copyright-space">

                        <a href="http://imigrasijogja.org">Copyright 2014 © <?php echo $this->General->lang('office_name'); ?></a>
                    </p>
                </div>

            </div><!--/row-->
        </div><!--/container--> 
    </div> 
    <!--=== End Footer ===-->