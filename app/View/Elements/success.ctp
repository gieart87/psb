<div class="alert alert-block alert-success">
    <a href="#" data-dismiss="alert" class="close">×</a>
    <!--<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> Check validation!</h4>-->
    <p>
        <?php echo $message; ?>
    </p>
</div>

<script>
    $(function() {
        $('.alert').animate({
            opacity: 0
        }, 50000)
    });
</script>