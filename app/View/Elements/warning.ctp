<div class="alert alert-block" style="position:absolute;z-index:1010">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $message; ?>
</div>

<script>$(function() {
        $('.alert').animate({
            opacity: 0
        }, 50000)
    });
</script>
