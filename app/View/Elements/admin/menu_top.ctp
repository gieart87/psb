<?php if ($prefix == 'admin'): ?>
    <li class="dropdown">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-book"></i>
            Publikasi
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><?php echo $this->Html->link(__('Buku'), array('controller' => 'books', 'action' => 'index', 'plugin' => false)); ?></li>
    <!--            <li><?php //echo $this->Html->link(__('Majalah'), array('controller' => 'magazines', 'action' => 'index', 'plugin' => false));           ?></li>
            <li><?php //echo $this->Html->link(__('Koran'), array('controller' => 'newspapers', 'action' => 'index', 'plugin' => false));           ?></li>
            <li><?php //echo $this->Html->link(__('Jurnal'), array('controller' => 'journals', 'action' => 'index', 'plugin' => false));           ?></li>
            <li><?php //echo $this->Html->link(__('Prosiding'), array('controller' => 'proceedings', 'action' => 'index', 'plugin' => false));           ?></li>
            <li><?php //echo $this->Html->link(__('Disertasi,Tesis'), array('controller' => 'dissertations', 'action' => 'index', 'plugin' => false));           ?></li>
            <li><?php //echo $this->Html->link(__('Tabloid'), array('controller' => 'tabloids', 'action' => 'index', 'plugin' => false));           ?></li>-->

            <li><?php echo $this->Html->link(__('Kategori Publikasi'), array('controller' => 'categories', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Komentar User'), array('controller' => 'comments', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Feedback User'), array('controller' => 'feedbacks', 'action' => 'index', 'plugin' => false)); ?></li>

        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-shopping-cart"></i>
            Data Order
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">

                                                <!--<li><?php // echo $this->Html->link(__('Order Corporate'), array('controller' => 'corporate_orders', 'action' => 'index', 'plugin' => false));                        ?></li>-->

            <li><?php echo $this->Html->link(__('Order'), array('controller' => 'orders', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Konfirmasi'), array('controller' => 'confirmations', 'action' => 'index', 'plugin' => false)); ?></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-signal"></i>
            Laporan & Statistik
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">

            <li><?php echo $this->Html->link(__('Order Terbaru'), array('controller' => 'order_details', 'action' => 'books_new_sells')); ?></li>
            <li><?php echo $this->Html->link(__('Order per Periode'), array('controller' => 'order_details', 'action' => 'books_period_sells')); ?></li>
            <li><?php echo $this->Html->link(__('Buku Terlaris'), array('controller' => 'order_details', 'action' => 'books_best_seller')); ?></li>
            <li><?php echo $this->Html->link(__('Pendapatan Penerbit'), array('controller' => 'order_details', 'action' => 'books_publishers')); ?></li>
            <li><?php echo $this->Html->link(__('Data & Statistik'), array('controller' => 'statistics', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Statistik Buku'), array('controller' => 'statistics', 'action' => 'books')); ?></li>
            <li><?php echo $this->Html->link(__('Report ePustaka'), array('controller' => 'libraries', 'action' => 'report')); ?></li>
        </ul>
    </li>

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-tasks"></i>
            Data
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><?php echo $this->Html->link(__('Daftar User'), array('controller' => 'users', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Member ePustaka'), array('controller' => 'students', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar ePustaka'), array('controller' => 'libraries', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Admin ePustaka'), array('controller' => 'librarians', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Penerbit'), array('controller' => 'publishers', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Paket Sewa'), array('controller' => 'packages', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Universitas'), array('controller' => 'universities', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Fakultas'), array('controller' => 'faculties', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Program Studi'), array('controller' => 'departments', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Aplikasi Klien'), array('controller' => 'clients', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Daftar Voucher'), array('controller' => 'vouchers', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Badges'), array('controller' => 'badges', 'action' => 'index', 'plugin' => false)); ?></li>

            <li><?php echo $this->Html->link(__('Authors'), array('controller' => 'authors', 'action' => 'index', 'plugin' => false)); ?></li>

        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-wrench"></i>
            Pengaturan
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><?php echo $this->Html->link(__('Pengaturan Umum'), array('controller' => 'settings', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Pengaturan Social Message'), array('controller' => 'social_messages', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Pengaturan Client'), array('controller' => 'configs', 'action' => 'index', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Action List'), array('controller' => 'acl', 'action' => 'acl_actions', 'plugin' => false)); ?></li>
            <li><?php echo $this->Html->link(__('Permission Setting'), array('controller' => 'acl', 'action' => 'acl_permissions', 'plugin' => false)); ?></li>

        </ul>
    </li>
<?php endif; ?>
     