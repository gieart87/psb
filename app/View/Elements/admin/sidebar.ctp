<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as it --> 

            <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'profile')); ?>">
                <img src="<?php echo Router::url('/'); ?>img/admin/avatars/sunny.png" alt="me" class="online" /> 
                <span>
                    <?php echo $this->Session->read('Auth.User.name'); ?> 
                </span>
                <i class="fa fa-angle-down"></i>
            </a> 

        </span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive

    To make this navigation dynamic please make sure to link the node
    (the reference to the nav > ul) after page load. Or the navigation
    will not initialize.
    -->
    <nav>

        <ul>
            <li <?php echo ($activeMenu == 'siswa') ? 'class="open"' : '' ?>>
                <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">PSB</span></a>
                <ul <?php echo ($activeMenu == 'siswa') ? 'style="display: block;"' : '' ?>>
                    <li>
                        <a href="<?php echo $this->Html->url(array('controller' => 'siswas', 'action' => 'index')); ?>">Pendaftar</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->Html->url(array('controller' => 'kabupatens', 'action' => 'index')); ?>">Daftar Kabupaten</a>
                    </li>
                    <li>
                        <a href="<?php echo $this->Html->url(array('controller' => 'sekolahs', 'action' => 'index')); ?>">Daftar Asal Sekolah</a>
                    </li>
                </ul>
            </li>


        </ul>
    </nav>
    <span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>

</aside>