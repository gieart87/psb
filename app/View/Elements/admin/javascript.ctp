<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo Router::url('/'); ?>js/admin/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
<script src="<?php echo Router::url('/'); ?>js/admin/libs/jquery-2.0.2.min.js"></script>

<!-- Additional Setup JS - Gie-Art -->
<script src="<?php echo Router::url('/'); ?>js/default.js"></script>
<?php echo $this->General->basePath(); ?>
<script src="<?php echo Router::url('/'); ?>js/bootstrap.js"></script>
<!-- End -->
<script>
//    if (!window.jQuery) {
//        document.write('<script src="js/admin/libs/jquery-2.0.2.min.js"><\/script>');
//    }
</script>

<!--<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>-->
<script src="<?php echo Router::url('/'); ?>js/admin/libs/jquery-ui-1.10.3.min.js"></script>
<script>
//    if (!window.jQuery.ui) {
//        document.write('<script src="js/admin/libs/jquery-ui-1.10.3.min.js"><\/script>');
//    }
</script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
<script src="js/admin/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

<!-- BOOTSTRAP JS -->
<script src="<?php echo Router::url('/'); ?>js/admin/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?php echo Router::url('/'); ?>js/admin/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="<?php echo Router::url('/'); ?>js/admin/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<!--<script src="<?php //echo Router::url('/');  ?>js/admin/plugin/jquery-validate/jquery.validate.min.js"></script>-->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/jquery-validate/jquery.validate.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/jquery-validate/additional-methods.js"></script>


<!-- JQUERY MASKED INPUT -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/fastclick/fastclick.js"></script>

<!--[if IE 7]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
<!--<script src="js/admin/demo.js"></script>-->

<!-- MAIN APP JS FILE -->
<script src="<?php echo Router::url('/'); ?>js/admin/app.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->

<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/flot/jquery.flot.cust.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/flot/jquery.flot.resize.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/flot/jquery.flot.tooltip.js"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/vectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/vectormap/jquery-jvectormap-world-mill-en.js"></script>

<!-- Full Calendar -->
<script src="<?php echo Router::url('/'); ?>js/admin/plugin/fullcalendar/jquery.fullcalendar.min.js"></script>


<!-- Custom Js -->
<script src="<?php echo Router::url('/'); ?>js/admin/general.js"></script>


