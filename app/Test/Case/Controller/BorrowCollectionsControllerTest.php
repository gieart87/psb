<?php
App::uses('BorrowCollectionsController', 'Controller');

/**
 * BorrowCollectionsController Test Case
 *
 */
class BorrowCollectionsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.borrow_collection',
		'app.collection',
		'app.library',
		'app.university',
		'app.dissertation',
		'app.faculty',
		'app.department',
		'app.student',
		'app.user',
		'app.admin',
		'app.author',
		'app.book',
		'app.publisher',
		'app.magazine',
		'app.category',
		'app.type',
		'app.publishers_type',
		'app.newspaper',
		'app.newspaper_edition',
		'app.product',
		'app.journal_edition',
		'app.journal',
		'app.download',
		'app.tabloid',
		'app.tabloid_edition',
		'app.proceedings',
		'app.proceeding_article',
		'app.journal_article',
		'app.magazine_edition',
		'app.cart',
		'app.users_category',
		'app.publishers_user',
		'app.watch_bidding',
		'app.order_detail',
		'app.order',
		'app.payment',
		'app.payment_point',
		'app.newsletter',
		'app.statistic',
		'app.statistic_temp',
		'app.bidding',
		'app.wishlist',
		'app.comment',
		'app.authors_book',
		'app.tag',
		'app.tagged',
		'app.customer',
		'app.librarian',
		'app.subscribe',
		'app.package',
		'app.page',
		'app.synchronize_token',
		'app.universities_faculty',
		'app.suggestion'
	);

}
