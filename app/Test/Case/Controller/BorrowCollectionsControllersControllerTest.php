<?php
App::uses('BorrowCollectionsControllersController', 'Controller');

/**
 * BorrowCollectionsControllersController Test Case
 *
 */
class BorrowCollectionsControllersControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.borrow_collections_controller'
	);

}
