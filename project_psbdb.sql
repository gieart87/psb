-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2015 at 08:19 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project_psbdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `permalink_en` varchar(255) NOT NULL,
  `body_en` text NOT NULL,
  `is_featured` int(11) NOT NULL,
  `status` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `hits` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `articles_categories`
--

CREATE TABLE IF NOT EXISTS `articles_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permalink_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `permalink_en` varchar(255) NOT NULL,
  `description_en` text NOT NULL,
  `status` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `key` int(11) NOT NULL,
  `mime` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `large` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jurusans`
--

CREATE TABLE IF NOT EXISTS `jurusans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `kuota` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jurusans`
--

INSERT INTO `jurusans` (`id`, `kode`, `nama`, `kuota`) VALUES
(1, 'AV', 'Teknik Audio Video', 32),
(2, 'OTO', 'Teknik Otomotif', 64),
(3, 'MM', 'Teknik Multimedia', 64);

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` int(4) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `url_en` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` int(4) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `permalink_en` varchar(255) NOT NULL,
  `body_en` text NOT NULL,
  `theme` int(11) NOT NULL,
  `status` int(4) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `page_category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `page_categories`
--

CREATE TABLE IF NOT EXISTS `page_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `permalink` varchar(255) NOT NULL,
  `permalink_en` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `name_en` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `description_en` text NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(4) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE IF NOT EXISTS `schema_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`id`, `class`, `type`, `created`) VALUES
(1, 'InitMigrations', 'Migrations', '2015-06-23 11:26:13'),
(2, 'ConvertVersionToClassNames', 'Migrations', '2015-06-23 11:26:14'),
(3, 'IncreaseClassNameLength', 'Migrations', '2015-06-23 11:26:14'),
(4, 'InitDatabase', 'app', '2015-06-23 11:26:25');

-- --------------------------------------------------------

--
-- Table structure for table `sekolahs`
--

CREATE TABLE IF NOT EXISTS `sekolahs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `sekolahs`
--

INSERT INTO `sekolahs` (`id`, `nama`) VALUES
(1, 'SMP N 1 Ponjong'),
(3, 'SMP N 1 Rongkop'),
(4, 'MTs N Semanu'),
(5, 'SMP N 1 Semanu'),
(6, 'SMP N 1 Pracimantoro'),
(7, 'SMP N 3 Satu Atap Eromoko'),
(8, 'SMP N 2 Eromoko'),
(9, 'SMP Gajahmungkur 11 Praci'),
(10, 'SMP Muhammadiyah Semanu'),
(11, 'SMP Persiapan Semanu'),
(12, 'SMP N 2 Ponjong'),
(13, 'MTs N Rongkop'),
(14, 'SMP N 3 Ponjong'),
(15, 'SMP N 3 Pracimantoro'),
(16, 'SMPN 1 Sukaresmi'),
(17, 'SMP Bhina Karya Rongkop'),
(18, 'MTs Yappy Kenteng'),
(19, 'SMP N 4 Ponjong'),
(20, 'SMP N 2 Semanu'),
(21, 'SMP N 1 Girisubo'),
(22, 'SMP Kartika Ponjong'),
(23, 'SMP Muhammadiyah Rongkop'),
(24, 'SMP Persatuan Ponjong'),
(25, 'SMP N 2 Pracimantoro'),
(26, 'SMP N 1 Karang'),
(27, 'SMP N 1 Karang Jambu'),
(28, 'SMP N 80 Jakarta'),
(29, 'MTS N Sumbergiri'),
(30, 'MTs N Sumbergiri Ponjong');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `siswas`
--

CREATE TABLE IF NOT EXISTS `siswas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_pendaftaran` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `kelamin` int(11) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `sekolah_id` int(11) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `nilai_b_indonesia` float NOT NULL,
  `nilai_b_inggris` float NOT NULL,
  `nilai_matematika` float NOT NULL,
  `nilai_ipa` float NOT NULL,
  `pilihan_satu` int(11) NOT NULL,
  `pilihan_dua` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=199 ;

--
-- Dumping data for table `siswas`
--

INSERT INTO `siswas` (`id`, `no_pendaftaran`, `nama`, `tempat_lahir`, `tgl_lahir`, `kelamin`, `agama`, `sekolah_id`, `alamat`, `nilai_b_indonesia`, `nilai_b_inggris`, `nilai_matematika`, `nilai_ipa`, `pilihan_satu`, `pilihan_dua`, `status`, `created`, `modified`) VALUES
(1, 'M-001', 'Devi Rahayuningsih', 'Gunungkidul', '2005-08-30', 2, 'Islam', 4, 'Tejo, RT 06 RW 18, Pucanganom, Rongkop, Gunungkidul', 8.8, 5.6, 4, 4.75, 3, 3, 0, '2015-07-01 09:24:05', '2015-07-01 09:24:05'),
(2, 'A-001', 'Fani Purwanti', 'Gunungkidul', '2000-06-12', 2, 'Islam', 4, 'Ngaglik, Ngeposari, Semanu, Gunungkidul', 7, 3.8, 2.7, 3.75, 1, 3, 0, '2015-07-01 09:28:29', '2015-07-01 09:28:29'),
(3, 'M-003', 'Sekar Loviana', 'Gunungkidul', '2000-09-17', 2, 'Islam', 5, 'Semanu, Semanu, Gunungkidul', 9, 6.6, 5.75, 6.25, 3, 3, 0, '2015-07-01 09:28:59', '2015-07-01 09:28:59'),
(4, 'O-013', 'Dicky Mustofa', 'Wonogiri', '1999-12-10', 1, 'Islam', 6, 'Glinggang, Pracimantoro', 8.4, 5.2, 3.75, 7.25, 2, 3, 0, '2015-07-01 09:29:54', '2015-07-01 09:29:54'),
(5, 'A-002', 'Ayuk Tri Utami', 'Gunungkidul', '1991-01-01', 2, '0', 4, 'Mojo, Ngeposari, Semanu, Gunungkidul', 8.2, 5.4, 6.5, 5, 1, 3, 0, '2015-07-01 09:31:01', '2015-07-01 09:31:01'),
(6, 'A-003', 'Feti Wijiyanti', 'Gunungkidul', '1991-01-01', 2, 'Islam', 4, 'Mojo, Ngeposari, Semanu, Gunungkidul', 6.4, 6.6, 4.2, 5.5, 1, 3, 0, '2015-07-01 09:34:41', '2015-07-01 09:34:41'),
(7, 'M-014', 'Bayu Setiaji', 'Wonogiri', '2000-01-18', 1, 'Islam', 7, 'Wonosumo, Basuhan, Eromoko, Wonogiri', 8.4, 4.8, 5.5, 7.5, 3, 1, 0, '2015-07-01 09:35:34', '2015-07-01 09:35:34'),
(8, 'A-004', 'Sintiya Rahmawati', 'Gunungkidul', '2000-11-29', 2, 'Islam', 5, 'Kangkung, Ngeposari, Semanu, Gunungkidul', 9, 4.4, 4.75, 6.25, 1, 3, 0, '2015-07-01 09:37:24', '2015-07-01 09:37:24'),
(9, 'O-006', 'Agus Windarto', 'Gunungkidul', '1999-08-17', 1, 'Islam', 8, 'Grogol, Tambakromo, Ponjong', 9, 8.2, 4, 5.5, 2, 1, 0, '2015-07-01 09:37:24', '2015-07-01 09:37:24'),
(10, 'A-005', 'Mustofa Prasetyo', 'Wonogiri', '2000-05-01', 1, 'Islam', 6, 'Kerjo, Glinggang, Pracimantoro', 6.4, 2.8, 2.5, 4, 1, 2, 0, '2015-07-01 09:39:08', '2015-07-01 09:39:08'),
(11, 'M-015', 'Ramadhan Diduk Sunanto', 'Wonogiri', '1999-12-19', 1, 'Islam', 7, 'Wonosumo, Basuhan, Eromoko, Wonogiri', 8.4, 4.4, 5, 5.5, 3, 2, 0, '2015-07-01 09:40:19', '2015-07-01 09:40:19'),
(12, 'A-006', 'Aditya Oktavian', 'Wonogiri', '2000-10-17', 1, '0', 9, 'Duren Kidul, Glinggang, Pracimantoro', 7.8, 6, 4, 5.75, 1, 3, 0, '2015-07-01 09:42:59', '2015-07-01 09:42:59'),
(13, 'M-016', 'Rofik Cahaya', 'Wonogiri', '1998-05-28', 1, 'Islam', 7, 'Wonosumo, Basuhan, Eromoko, Wonogiri', 7.8, 6, 3.25, 6.5, 3, 1, 0, '2015-07-01 09:44:15', '2015-07-01 09:44:15'),
(14, 'A-007', 'Rohmad Fahrul Syaifulloh', 'Wonogiri', '1999-11-27', 1, 'Islam', 9, 'Duren Lor, Glinggang, Pracimantoro', 9.2, 5, 7.5, 6.75, 1, 2, 0, '2015-07-01 09:45:08', '2015-07-01 09:45:08'),
(15, 'A-008', 'Dongking Saputra', 'Gunungkidul', '2000-02-29', 1, 'Kristen', 3, 'Karangwetan, Semugih, Rongkop, Gunungkidul', 8.8, 5.2, 7.5, 6.75, 1, 2, 0, '2015-07-01 09:46:59', '2015-07-01 09:46:59'),
(16, 'M-011', 'Betta Oktaviyani', 'Gunungkidul', '1999-10-24', 2, 'Islam', 10, 'Sembuku, Dadapayu, Semanu, Gunungkidul', 7.4, 5.4, 3.25, 5.25, 3, 3, 0, '2015-07-01 09:49:37', '2015-07-01 09:49:37'),
(17, 'A-009', 'Gunawan', 'Purworejo', '2000-10-07', 1, 'Islam', 11, 'Mojo, Ngeposari, Semanu, Gunungkidul', 7, 3.6, 4, 5, 1, 2, 0, '2015-07-01 09:50:20', '2015-07-01 09:50:20'),
(18, 'A-011', 'Ibnu Amri Rohadi', 'Demak', '1999-10-17', 1, 'Islam', 3, 'Gangsalan Kidul, Nglindur, Girisubo, Gunungkidul', 8, 3.2, 3.75, 5.5, 1, 3, 0, '2015-07-01 09:52:28', '2015-07-01 09:52:28'),
(19, 'M-025', 'Hestin Dwi Indriani', 'Gunungkidul', '1999-02-26', 2, 'Islam', 5, 'Weru, Petir, Rongkop, Gunungkidul', 9, 7.2, 7, 6.25, 3, 1, 0, '2015-07-01 09:53:13', '2015-07-01 10:57:59'),
(20, 'O-014', 'Sidiq Ahmadi', 'Gunungkidul', '2000-02-29', 1, 'Islam', 6, 'Sambiroto Kidul, Sambiroto, Pracimantoro', 7.8, 2.6, 3, 4.75, 2, 1, 0, '2015-07-01 09:56:46', '2015-07-01 09:56:46'),
(21, 'M-027', 'Nurul Dwi Pangestu', 'Wonogiri', '1999-11-19', 2, 'Islam', 6, 'Dilem, RT 01/RW 04, Gebangharjo, Pracimantoro', 9, 5, 3, 6.25, 3, 1, 0, '2015-07-01 09:58:45', '2015-07-01 09:58:45'),
(22, 'O-020', 'Nurwahid Susanto', 'Gunungkidul', '1999-03-01', 1, 'Islam', 1, 'Karangijo Kulon, Ponjong, Gunungkidul', 6.4, 3.8, 4, 6.75, 2, 1, 0, '2015-07-01 09:59:11', '2015-07-01 09:59:11'),
(23, 'M-005', 'Ni''am Nisbat Fatonah Saputri', 'Gunungkidul', '1999-12-01', 2, 'Islam', 11, 'Sawit, Gombang, Gunungkidul', 8.6, 6.4, 3, 6, 3, 1, 0, '2015-07-01 09:59:44', '2015-07-01 09:59:44'),
(24, 'O-021', 'Ardon Kristiano', 'Gunungkidul', '2000-05-07', 1, 'Islam', 1, 'Simo 2, Genjahan, Ponjong, Gunungkidul', 6.6, 2.6, 3.75, 6.5, 2, 1, 0, '2015-07-01 10:01:03', '2015-07-01 10:01:03'),
(25, 'M-017', 'Sutarti', 'Wonogiri', '2000-06-04', 2, 'Islam', 7, 'Danggolo, Rt010/06, Basuhan Eromoko', 8.4, 5.2, 4.75, 6, 3, 1, 0, '2015-07-01 10:02:32', '2015-07-01 10:02:32'),
(26, 'M-006', 'Anik Setyawati', 'Wonogiri', '2000-01-26', 2, 'Islam', 6, 'Bercak, Nglinggang, Pracimantoro', 8.4, 3, 3, 2.75, 3, 1, 0, '2015-07-01 10:05:35', '2015-07-01 10:05:35'),
(27, 'O-024', 'Afrizal Pramudya', 'Gunungkidul', '1999-10-26', 1, 'Kristen', 3, 'Kerdonmiri, Karangwuni, Rongkop, Gunungkidul', 7.8, 3.4, 4.25, 5.75, 2, 1, 0, '2015-07-01 10:06:32', '2015-07-01 10:06:32'),
(28, 'M-030', 'Serlly Ayu Anggraini', 'Gunungkidul', '1999-06-11', 2, 'Islam', 3, 'Sriten, Karangwuni, Rongkop, Gunungkidul', 7.6, 5, 4.2, 7, 3, 2, 0, '2015-07-01 10:08:22', '2015-07-01 10:08:22'),
(29, 'O-008', 'Sambudi Edi Nugroho', 'Gunungkidul', '2000-11-04', 1, 'Islam', 1, 'Ngeposari, Semanu, Kranggan, Gunungkidul', 9.2, 6, 4.25, 5.25, 2, 1, 0, '2015-07-01 10:08:54', '2015-07-01 10:08:54'),
(30, 'M-004', 'Octavia Dita Indah Sari', 'Gunungkidul', '1999-10-25', 2, 'Islam', 1, 'Karangijo Wetan, RT 01/RW 03, Ponjong, Ponjong, Gunungkidul', 8.2, 6.2, 3.75, 5.25, 3, 1, 0, '2015-07-01 10:10:51', '2015-07-01 10:10:51'),
(31, 'O-009', 'Yogi Andriyanto', 'Gunungkidul', '1999-08-14', 1, 'Islam', 5, 'Semuluh Kidul, Ngeposari, Semanu, Gunungkidul', 9, 5, 6.75, 6.75, 2, 1, 0, '2015-07-01 10:10:51', '2015-07-01 10:10:51'),
(32, 'M-026', 'Feni Aryati', 'Gunungkidul', '1999-04-21', 2, 'Islam', 5, 'Weru, Petir, Rongkop, Gunungkidul', 9, 8.4, 8.75, 8.25, 3, 3, 0, '2015-07-01 10:10:53', '2015-07-01 10:10:53'),
(33, 'M-028', 'Meiva Wulandari', 'Gunungkidul', '2000-05-28', 2, 'Islam', 11, 'Ngerejek', 8.2, 3.2, 3.75, 3.75, 3, 2, 0, '2015-07-01 10:12:35', '2015-07-01 10:12:35'),
(34, 'O-025', 'Galih Isnu Alam', 'Gunungkidul', '1998-12-09', 1, 'Islam', 3, 'Kerdonmiri, Karangwuni, Rongkop, Gunungkidul', 8.4, 4.4, 4, 5.5, 2, 3, 0, '2015-07-01 10:12:54', '2015-07-01 10:12:54'),
(35, 'M-023', 'Cindi Setyaningsih', 'Gunungkidul', '2000-07-20', 2, 'Islam', 5, 'Sawit, Gombang, Gunungkidul', 8.4, 8.2, 8.2, 9.5, 3, 3, 0, '2015-07-01 10:14:24', '2015-07-01 10:14:24'),
(36, 'M-041', 'Anita Rahmawati', 'Gunungkidul', '1999-08-03', 2, 'Islam', 12, 'Bedoyo Kidul, Bedoyo, Ponjong, Gunungkidul', 8.2, 4, 4.75, 5.25, 3, 1, 0, '2015-07-01 10:16:47', '2015-07-01 10:16:47'),
(37, 'O-042', 'Febri Ahmad Mundhori', 'Gunungkidul', '2000-02-11', 1, 'Islam', 13, 'Pakel, Pringombo, Rongkop, Gunungkidul', 9.6, 5.2, 6.5, 4.75, 2, 1, 0, '2015-07-01 10:17:11', '2015-07-01 10:17:11'),
(38, 'M-009', 'Iis Sugiarti', 'Wonogiri', '2001-03-03', 2, 'Islam', 6, 'Glinggang, Pracimantoro', 9.2, 5.8, 3.7, 5.5, 3, 3, 0, '2015-07-01 10:18:33', '2015-07-01 10:18:33'),
(39, 'M-022', 'Dita Andriani', 'Gunungkidul', '2000-03-21', 2, 'Islam', 4, 'Gombang, Gombang, Ponjong, Gunungkidul', 7.8, 5.2, 5.25, 5, 3, 1, 0, '2015-07-01 10:20:06', '2015-07-01 10:20:06'),
(40, 'O-036', 'Dwi Purwanto', 'Gunungkidul', '2000-07-31', 1, 'Islam', 14, 'Sawur, Sawahan, Ponjong, Gunungkidul', 8.2, 4.2, 4, 4, 2, 1, 0, '2015-07-01 10:21:06', '2015-07-01 10:21:06'),
(41, 'M-013', 'Roni Setyobudi', 'Gunungkidul', '1999-06-26', 1, 'Islam', 15, 'Ketos, Petirsari, Pracimantoro', 8, 5.8, 3.75, 4.5, 3, 1, 0, '2015-07-01 10:22:10', '2015-07-01 10:22:10'),
(42, 'M-038', 'Dedi Rohmadon', 'Gunungkidul', '1999-12-29', 1, 'Islam', 14, 'Plarung, Sawahan, Ponjong, Gunungkidul', 8.4, 6.4, 8.75, 7.25, 3, 1, 0, '2015-07-01 10:23:59', '2015-07-01 10:23:59'),
(43, 'O-038', 'Sidiq Kurniawan', 'Gunungkidul', '2000-03-31', 1, 'Islam', 14, 'Plarung, Sawahan, Ponjong, Gunungkidul ', 6.2, 4.4, 4.5, 5, 2, 1, 0, '2015-07-01 10:23:59', '2015-07-01 10:23:59'),
(44, 'M-012', 'Alvin Novian Perdana', 'Wonogiri', '2001-01-14', 1, 'Islam', 15, 'Nglaos, Petirsari, Pracimantoro', 8.6, 6.8, 5.75, 6.5, 3, 2, 0, '2015-07-01 10:24:20', '2015-07-01 10:24:20'),
(45, 'M-036', 'Didik Setiawan', 'Gunungkidul', '2000-01-10', 1, 'Islam', 14, 'Jatisari, Sawahan, Ponjong, Gunungkidul', 8, 3.4, 4.5, 5.75, 3, 1, 0, '2015-07-01 10:27:02', '2015-07-01 10:27:02'),
(46, 'M-040', 'Wahyu Ningsih', 'Gunungkidul', '1999-07-12', 2, 'Islam', 14, 'Gedong, Sawahan, Ponjong, GK', 9, 4.2, 4.75, 4.25, 3, 1, 0, '2015-07-01 10:27:09', '2015-07-01 10:27:09'),
(47, 'O-039', 'Muji Yulianto', 'Gunungkidul', '1999-07-01', 1, 'Islam', 14, 'Gedong, Sawahan, Ponjong, Gunungkidul', 6, 4.4, 5.5, 4.75, 2, 1, 0, '2015-07-01 10:27:15', '2015-07-01 10:27:15'),
(48, 'M-034', 'Melinda Kurniawati', 'Gunungkidul', '2000-05-06', 2, 'Islam', 14, 'Tengger, Sawahan, Ponjong', 8.8, 6.6, 7.75, 5.75, 3, 1, 0, '2015-07-01 10:29:53', '2015-07-01 10:29:53'),
(49, 'M-037', 'Anjas Prasetyo', 'Gunungkidul', '1999-08-15', 1, 'Islam', 14, 'Plarung, Sawahan, Ponjong, Gunungkidul', 9.4, 7.8, 8.25, 7.5, 3, 2, 0, '2015-07-01 10:30:08', '2015-07-01 10:30:08'),
(50, 'M-029', 'Ujang Aziz Pratama', 'Gunungkidul', '1999-06-12', 1, 'Islam', 3, 'Pucanganom, Rongkop', 9.4, 6.4, 7.25, 7, 3, 2, 0, '2015-07-01 10:31:22', '2015-07-01 10:31:22'),
(51, 'O-001', 'Dela Irfiani', 'Gunungkidul', '1999-09-27', 2, 'Islam', 1, 'Trenggono Kidul, Sidorejo, Ponjong, Gunungkidul', 9.6, 5.6, 7.25, 7.75, 2, 1, 0, '2015-07-01 10:32:19', '2015-07-01 10:32:19'),
(52, 'M-019', 'Dian Dwi Hastuti', 'Wonogiri', '1999-10-09', 2, 'Islam', 7, 'Donggala, Rt.01/06/Basuhan, Eromoko', 7.8, 6, 5.25, 7.5, 3, 1, 0, '2015-07-01 10:33:15', '2015-07-01 10:33:15'),
(53, 'M-031', 'Hendi Dwi Saputra Sinambela', 'Gunungkidul', '2000-04-08', 1, 'Islam', 3, 'Pampang, RT 023/RW 006, Karangwuni, Rongkop, Gunungkidul', 8, 7.4, 6.5, 8, 3, 1, 0, '2015-07-01 10:33:47', '2015-07-01 10:33:47'),
(54, 'O-002', 'Danang Waskito Jati', 'Jakarta', '2000-02-26', 1, 'Islam', 5, 'Gunungsari, Ngeposari, Semanu, Gunungkidul', 9.4, 6.6, 5.25, 5.25, 2, 3, 0, '2015-07-01 10:34:37', '2015-07-01 10:34:37'),
(55, 'M-020', 'Noerfiana Safitri', 'Wonogiri', '1999-01-06', 1, 'Islam', 7, 'Salam, Basuhan, Eromoko', 8.8, 6.2, 3.75, 6, 3, 1, 0, '2015-07-01 10:35:11', '2015-07-01 10:35:11'),
(56, 'M-032', 'Hidayati Eka Sari', 'Gunungkidul', '2000-01-10', 2, 'Islam', 3, 'Tirisan, Karangwuni, Rongkop, Gunungkidul', 8.6, 8.2, 7.5, 8.75, 3, 1, 0, '2015-07-01 10:36:47', '2015-07-01 10:36:47'),
(57, 'M-002', 'Siti Rahmah', 'Gunungkidul', '1999-11-24', 2, 'Islam', 16, 'Jl Raya Munjul, Sukaresmi, Pandeglang', 8.8, 9.8, 9.1, 9.34, 3, 1, 0, '2015-07-01 10:37:58', '2015-07-01 10:37:58'),
(58, 'O-003', 'Edianta', 'Gunungkidul', '1999-04-02', 1, 'Islam', 4, 'Tejo, Pucanganom, Rongkop, Gunungkidul', 8.6, 5, 4.5, 6.25, 2, 1, 0, '2015-07-01 10:39:10', '2015-07-01 10:39:10'),
(59, 'M-033', 'Rita Trisna Ristinah', 'Gunungkidul', '1999-07-28', 2, 'Islam', 14, 'Tambakromo, Tambakromo, Ponjong, Gunungkidul', 9, 7.6, 9, 7.5, 3, 1, 0, '2015-07-01 10:40:21', '2015-07-01 10:40:21'),
(60, 'M-008', 'Erma Viga Silviana', 'Wonogiri', '2000-06-02', 2, 'Islam', 6, 'Duren lor, glinggang, Pracimantoro', 6, 3.4, 3, 4.5, 3, 1, 0, '2015-07-01 10:40:31', '2015-07-01 10:40:31'),
(61, 'O-004', 'Diyan Nurdiyono', 'Kuningan', '1999-09-24', 1, 'Islam', 17, 'Duwet, Karangwuni, Rongkop, Gunungkidul', 5.8, 5, 4.75, 4.5, 2, 1, 0, '2015-07-01 10:42:26', '2015-07-01 10:42:26'),
(62, 'O-005', 'Stefanus Dedy Setiawan', 'Lampung', '2000-02-16', 1, 'Islam', 8, 'Klepu, Tambakromo, Ponjong, Gunungkidul', 8.8, 5, 4.25, 5, 2, 1, 0, '2015-07-01 10:45:51', '2015-07-01 10:45:51'),
(63, 'M-051', 'Setiyo Prabowo', 'Gunungkidul', '1999-12-09', 1, 'Islam', 3, 'Tirisan, Pringombo, Rongkop, GK', 7, 4.8, 6.5, 6, 3, 2, 0, '2015-07-01 10:46:33', '2015-07-01 10:46:33'),
(64, 'M-018', 'Retno Wulandari', 'Wonogiri', '1999-10-10', 2, 'Islam', 7, 'Danggolo, RT 01 / RW 06, Basuhan, Eromoko', 7.6, 5.2, 4.25, 5, 3, 1, 0, '2015-07-01 10:47:33', '2015-07-01 10:47:33'),
(65, 'M-052', 'Agus Febri Susanto', 'Gunungkidul', '1999-05-15', 1, 'Islam', 3, 'Tirisan, Pringombo, Rongkop, GK', 9.4, 7, 6.5, 6.5, 3, 2, 0, '2015-07-01 10:47:47', '2015-07-01 10:47:47'),
(66, 'O-010', 'Reza Saputra', 'Wonogiri', '2000-04-16', 1, 'Islam', 6, 'Duren Kidul, Glinggang, Pracimantoro', 6.2, 3.6, 4.25, 4.5, 2, 1, 0, '2015-07-01 10:48:08', '2015-07-01 10:48:08'),
(67, 'M-039', 'Efi Erna Wati', 'Gunungkidul', '1999-04-17', 2, 'Islam', 14, 'Plarung, Sawahan, Ponjong, Gunungkidul', 8.4, 5.8, 4.25, 4.25, 3, 1, 0, '2015-07-01 10:50:14', '2015-07-01 10:50:14'),
(68, 'M-010', 'Dwi Retno Sari', 'Wonogiri', '2000-03-09', 2, 'Islam', 6, 'Kerjo, glinggang, Pracimantoro', 7.6, 5, 4.75, 5.75, 3, 1, 0, '2015-07-01 10:50:17', '2015-07-01 10:50:17'),
(69, 'O-012', 'Heru Setiawan', 'Wonogiri', '1999-10-10', 1, 'Islam', 7, 'Ngelo, Basuhan, Eromoko', 6.8, 3.8, 3.25, 4.25, 2, 1, 0, '2015-07-01 10:51:47', '2015-07-01 10:51:47'),
(70, 'O-016', 'Agung Tri Prabowo', 'Gunungkidul', '1999-06-27', 1, 'Islam', 12, 'Bedoyo, Ponjong, Gunungkidul', 9.2, 4.6, 4.5, 4.5, 2, 2, 0, '2015-07-01 10:54:36', '2015-07-01 10:54:36'),
(71, 'M-024', 'Yesi Lusiana', 'Gunungkidul', '1999-11-01', 2, 'Islam', 12, 'Pringluang, Bedoyo, Ponjong', 8.6, 4.8, 4, 6, 3, 2, 0, '2015-07-01 10:56:48', '2015-07-01 10:56:48'),
(72, 'O-017', 'Jalu Ika Prabowo', 'Gunungkidul', '2000-04-09', 1, 'Islam', 5, 'Semuluh Lor, Ngeposari, Semanu, Gunungkidul', 7.6, 3.2, 4.5, 6, 2, 1, 0, '2015-07-01 10:57:16', '2015-07-01 10:57:16'),
(73, 'M-047', 'Novia Dwi Rahayu', 'Gunungkidul', '1998-11-09', 2, 'Islam', 12, 'Kebowan, gombang, Ponjong', 8.2, 3.4, 4.2, 5.2, 3, 1, 0, '2015-07-01 10:59:19', '2015-07-01 10:59:19'),
(74, 'M-054', 'Arinna Zakiyah', 'Gunungkidul', '1999-09-15', 2, 'Islam', 18, 'Geblug, Kenteng, Ponjong', 7.2, 5, 2.75, 3.25, 3, 1, 0, '2015-07-01 11:01:58', '2015-07-01 11:01:58'),
(75, 'A-024', 'Febri Rizky Nur', 'GunungKidul', '2000-02-03', 1, 'Islam', 18, 'Gebluk,Kenteng,Ponjong', 4.2, 3.2, 3.5, 4, 1, 2, 0, '2015-07-01 11:04:42', '2015-07-01 11:04:42'),
(76, 'O-018', 'Dian Prayoga', 'Gunungkidul', '2000-05-29', 1, 'Islam', 4, 'Ngaglik, Ngeposari, Semanu, Gunungkidul', 9.6, 7.4, 4.5, 6.25, 2, 3, 0, '2015-07-01 11:04:53', '2015-07-01 11:04:53'),
(77, 'A-021', 'Andika Agus Triantono', 'Gunungkidul', '2000-08-10', 1, 'Islam', 12, 'Karangasem, Ponjong, Gunungkidul', 6.2, 3.2, 4.25, 3.75, 1, 2, 0, '2015-07-01 11:05:28', '2015-07-01 11:05:28'),
(78, ' M-046', 'Ira Dwi Rahayu', 'Gunungkidul', '1999-12-03', 2, 'Islam', 12, 'Bedoyo kulon, Bedoyo, Ponjong, GK', 8, 7.8, 5.75, 5.75, 3, 1, 0, '2015-07-01 11:05:53', '2015-07-01 11:05:53'),
(79, 'O-019', 'Bayu Anggita', 'Gunungkidul', '2000-02-06', 1, 'Islam', 4, 'Ngaglik, Ngeposari, Semanu, Gunungkidul', 7.4, 6.6, 6.25, 5, 2, 3, 0, '2015-07-01 11:06:22', '2015-07-01 11:06:22'),
(80, 'M-045', 'Sindi Armita Putri', 'Gunungkidul', '2000-03-17', 2, 'Islam', 12, 'Ngabean kidul, karangasem, Ponjong', 8.4, 6.2, 8.75, 6.5, 3, 2, 0, '2015-07-01 11:07:26', '2015-07-01 11:07:26'),
(81, 'O-022', 'Anggit Wahyu Prasetyo', 'Gunungkidul', '2000-04-05', 1, 'Kristen', 17, 'Karangwuni, Karangwuni, Rongkop, Gunungkidul', 8, 5.4, 4.25, 5.5, 2, 3, 0, '2015-07-01 11:08:41', '2015-07-01 11:08:41'),
(82, 'M-044', 'Arlan Yudhi Pratikta', 'Gunungkidul', '1999-08-01', 1, 'Islam', 12, 'Pucanganom, Rongkop', 7.8, 3.4, 5.25, 5.5, 3, 3, 0, '2015-07-01 11:08:47', '2015-07-01 11:08:47'),
(83, 'A-022', 'Yusuf Ramadhan', 'GunungKidul', '1999-01-01', 1, 'Islam', 12, 'Karang Asem', 6, 3.4, 3.5, 3.25, 1, 2, 0, '2015-07-01 11:09:29', '2015-07-01 11:09:29'),
(84, 'M-043', 'Monika Tomi Fitri Nurhana', 'Gunungkidul', '2000-04-27', 2, 'Islam', 12, 'Bendorubuh, Semugih, Rongkop', 8.4, 5.2, 4, 5.25, 3, 1, 0, '2015-07-01 11:10:22', '2015-07-01 11:10:22'),
(85, 'O-023', 'Yoni Maulana', 'Gunungkidul', '2000-06-15', 1, 'Islam', 3, 'Karangwuni, Karangwuni, Rongkop, Gunungkidul', 9.4, 6.4, 6.25, 6.25, 2, 3, 0, '2015-07-01 11:10:30', '2015-07-01 11:10:30'),
(86, 'A-023', 'Anton Wijaya', 'Gunungkidul', '2000-05-12', 1, 'Islam', 19, 'Sunggingan', 8.4, 3.2, 3, 4.75, 1, 3, 0, '2015-07-01 11:10:51', '2015-07-01 11:10:51'),
(87, 'M-042', 'Ayuni Aisya Lestari', 'Gunungkidul', '2000-06-27', 2, 'Islam', 12, 'Bedoyo wetan, Bedoyo, Ponjong', 7.6, 4.4, 5.5, 6, 3, 1, 0, '2015-07-01 11:12:06', '2015-07-01 11:12:06'),
(88, 'O-026', 'Wahyu Dwi Rianto', 'Gunungkidul', '2000-01-12', 1, 'Islam', 3, 'Saban, Karangwunim Rongkop, Gunungkidul', 9, 4.6, 6.25, 7, 2, 3, 0, '2015-07-01 11:13:00', '2015-07-01 11:13:00'),
(89, 'A-020', 'Krismanto', 'GunungKidul', '1998-01-26', 1, 'Islam', 12, 'Bedoyo Kidul,Ponjong,GunungKidul', 6.8, 5.2, 3.5, 4.75, 1, 2, 0, '2015-07-01 11:15:08', '2015-07-01 11:15:08'),
(90, 'A-010', 'Dyah Dwi Pramesti', 'Gunungkidul', '1999-06-22', 2, 'Islam', 12, 'Ngejring', 8.4, 6.2, 6, 7.75, 1, 2, 0, '2015-07-01 11:15:12', '2015-07-01 11:15:12'),
(91, 'O-027', 'Ardi Alfianto', 'Gunungkidul', '2000-08-26', 1, 'Islam', 20, 'Ploso, Sumberwungu, Tepus, Gunungkidul', 7.4, 4.6, 4, 4.75, 2, 3, 0, '2015-07-01 11:16:57', '2015-07-01 11:16:57'),
(92, 'M-068', 'Anis Oktatris Diani', 'Gunungkidul', '1999-10-28', 2, 'Islam', 21, 'Bengle, Pucung, Girisubo, Gunungkidul', 8.6, 7.2, 4.75, 5.75, 3, 3, 0, '2015-07-01 11:18:58', '2015-07-01 11:18:58'),
(93, 'A-016', 'Slamet Prihatin', 'GunungKidul', '1999-12-16', 2, 'Islam', 12, 'Ngalasombo,Bedoyo,Ponjong,Gunungkidul', 7.2, 4.6, 4, 5.5, 1, 3, 0, '2015-07-01 11:21:33', '2015-07-01 11:21:33'),
(94, 'A-012', 'Veranika Ayu Ardhani', 'Gunungkidul', '1999-01-11', 2, 'Islam', 12, 'Ngejring, Karangwuni, Rongkop, Gunungkidul', 9.6, 7.6, 4.75, 6.25, 1, 2, 0, '2015-07-01 11:21:59', '2015-07-01 11:21:59'),
(95, 'M-067', 'Vita Kusumowati', 'Gunungkidul', '2001-06-29', 2, 'Islam', 21, 'Sadeng, Pucung, Girisubo, Gunungkidul', 7.2, 4.2, 4, 3.5, 3, 3, 0, '2015-07-01 11:22:44', '2015-07-01 11:22:44'),
(96, 'O-029', 'RIKI PRIANTO', 'GUNUNGKIDUL', '1999-09-16', 1, 'Islam', 3, 'WUNI, MELIKAN, RONGKOP, GUNUNGKIDUL', 6.8, 5, 3.75, 5.75, 2, 2, 0, '2015-07-01 11:25:20', '2015-07-01 11:25:20'),
(97, 'M-066', 'Anita Noviyanti', 'Gunungkidul', '2000-06-24', 2, 'Islam', 21, 'Kandri, Pucung, Girisubo, Gunungkidul', 8.2, 7, 5.25, 7.75, 3, 3, 0, '2015-07-01 11:25:58', '2015-07-01 11:25:58'),
(99, 'A-013', 'Esti Aprilani', 'Gunungkidul', '2000-04-26', 2, 'Islam', 12, 'Pringluwang, Gunungkidul', 9.2, 6.4, 5.25, 4.5, 1, 2, 0, '2015-07-01 11:26:14', '2015-07-01 11:26:14'),
(100, 'O-028', 'Hendi Saputra', 'Gunungkidul', '2000-04-02', 1, 'Islam', 3, 'Manggung, Tileng, Girisubo, Gunungkidul', 8.6, 5.6, 6.75, 6.5, 2, 3, 0, '2015-07-01 11:26:53', '2015-07-01 11:26:53'),
(101, 'A-014', 'Irawati', 'Gunungkidul', '1999-04-06', 2, 'Islam', 13, 'Ngalasombo,Bedoyo,Ponjong,Gunungkidul', 6.4, 4.2, 4, 3.75, 1, 3, 0, '2015-07-01 11:26:55', '2015-07-01 12:16:46'),
(102, 'M-062', 'Vironicha Kurnia Istyanti', 'Gunungkidul', '2000-03-25', 2, 'Islam', 22, 'Karangasem, Karangasem, Ponjong, Gunungkidul', 8.4, 6.2, 5.75, 5.5, 3, 1, 0, '2015-07-01 11:29:35', '2015-07-01 11:29:35'),
(103, 'O-035', 'DANANG RAHMADAN', 'GUNUNGKIDUL', '1999-01-24', 1, 'Islam', 14, 'GEDONG, SAWAHAN, PONJONG', 8.8, 5.2, 6.25, 4.5, 2, 3, 0, '2015-07-01 11:30:32', '2015-07-01 11:30:32'),
(104, 'A-026', 'Yudha Pramana', 'Gunungkidul', '2000-08-04', 1, 'Islam', 19, 'Pragak, Sunggingan, Umbul Rejo, Ponjong', 6, 4.4, 2.5, 5.25, 1, 3, 0, '2015-07-01 11:31:53', '2015-07-01 11:31:53'),
(105, 'A-028', 'Rifky Vuad', 'Gunungkidul', '2000-06-09', 1, 'Islam', 19, 'Sunggingan', 8.2, 5.6, 2.75, 5.5, 1, 3, 0, '2015-07-01 11:34:54', '2015-07-01 11:34:54'),
(106, 'M-063', 'Novi Anggraini', 'Gunungkidul', '2000-02-10', 2, 'Islam', 12, 'Jomlang Lor, Karangasem, Ponjong, Gunungkidul', 9, 4, 3.75, 6, 3, 1, 0, '2015-07-01 11:35:18', '2015-07-01 11:35:18'),
(107, 'O-048', 'Wahyu Putra Veri Setiawan', 'Gunungkidul', '1999-11-16', 1, 'Islam', 3, 'Sriten, Karangwuni, Rongkop, Gunungkidul', 9.4, 8.4, 8.75, 8.75, 2, 3, 0, '2015-07-01 11:35:27', '2015-07-01 11:35:27'),
(108, 'A-027', 'Lisda Lestari', 'Gunungkidul', '2000-01-15', 2, 'Islam', 13, 'Pringombo,Rongkop,Gunungkidul', 8, 6.8, 4.5, 4.25, 1, 3, 0, '2015-07-01 11:37:03', '2015-07-01 11:37:03'),
(109, 'O-031', 'Tofiq Efendy', 'Gunungkidul', '1998-10-26', 1, 'Islam', 14, 'Plarung, Sawahan, Ponjong, Gunungkidul ', 6.6, 3, 3.5, 3.25, 2, 1, 0, '2015-07-01 11:38:28', '2015-07-01 11:38:28'),
(110, 'O-051', 'Muhamat Alin Hamdani', 'Gunungkidul', '1999-12-03', 1, 'Islam', 5, 'Sawit, Gombang, Gunungkidul', 7.8, 6, 6, 6.25, 2, 2, 0, '2015-07-01 11:38:32', '2015-07-01 11:38:32'),
(111, 'A-015', 'Yuniar Mulatsih', 'Gunungkidul', '1999-06-15', 2, 'Islam', 12, 'Bedoyo Kulon, Bedoyo, Ponjong, Gunungkidul', 7.8, 6.4, 3.5, 4, 1, 3, 0, '2015-07-01 11:38:33', '2015-07-01 11:38:33'),
(112, 'O-047', 'Afidya Wisnu Hardi', 'Gunungkidul', '2000-01-23', 1, 'Islam', 12, 'Banombo, Pucanganom, Rongkop, Gunungkidul', 7.2, 4.2, 3.25, 7, 2, 1, 0, '2015-07-01 11:39:56', '2015-07-01 11:39:56'),
(113, 'M-060', 'Rico Anjasmara', 'Gunungkidul', '1999-12-23', 2, 'Islam', 21, 'Manggung, Tileng, Girisubo, Gunungkidul', 8.4, 5.8, 4.5, 5.75, 3, 2, 0, '2015-07-01 11:39:56', '2015-07-01 11:39:56'),
(114, 'O-032', 'Dian Aditama', 'Gunungkidul', '2000-03-31', 1, 'Islam', 14, 'Dlarung, Sawahan, Ponjong', 8.8, 6, 6.5, 6.75, 2, 3, 0, '2015-07-01 11:42:28', '2015-07-01 11:42:28'),
(115, 'A-025', 'Agung Nugroho Jati', 'Gunungkidul', '2000-04-22', 1, 'Islam', 13, 'Wotawati, Pucung, Girisubo, Gunungkidul', 8.6, 3.6, 2.75, 4.25, 1, 3, 0, '2015-07-01 11:42:31', '2015-07-01 11:42:31'),
(116, 'M-075', 'Dika Fadzoli', 'Gunungkidul', '1999-06-25', 2, 'Islam', 3, 'Mujing, Melikan, Rongkop, Gunungkidul', 9, 8, 7.5, 6.75, 3, 3, 0, '2015-07-01 11:42:45', '2015-07-01 11:42:45'),
(117, 'O-044', 'Eka Dwi Rosiani', 'Kuningan', '1999-12-02', 2, 'Islam', 12, 'Saban, Karangwuni, Rongkop, Gunungkidul', 7.6, 4.6, 5, 4.75, 2, 1, 0, '2015-07-01 11:43:27', '2015-07-01 11:43:27'),
(118, 'O-059', 'Falan Syahrul Muzaki', 'Gunungkidul', '1999-11-24', 1, 'Islam', 23, 'Krdonmiri, Semugih, Rongkop, Gunungkidul', 9, 4.8, 2.75, 4.25, 2, 3, 0, '2015-07-01 11:44:01', '2015-07-01 11:51:33'),
(119, 'O-033', 'Fendi Bastian', 'Gunungkidul', '1999-12-31', 1, 'Islam', 14, 'Plarung, Sawahan, Ponjong', 9, 5, 4.75, 4.25, 2, 3, 0, '2015-07-01 11:45:37', '2015-07-01 11:45:37'),
(120, 'O-034', 'Dani Irawan', 'Gunungkidul', '2000-07-22', 1, 'Islam', 14, 'Plarung, Sawahan, Ponjong', 8.4, 4.2, 5.25, 5.25, 2, 3, 0, '2015-07-01 11:48:12', '2015-07-01 11:48:12'),
(121, 'M-074', 'Melly Handayani', 'Kulonprogo', '2000-02-18', 2, 'Islam', 19, 'Sunggingan, Umbulrejo, Ponjong, Gunungkidul', 9, 7, 5, 4.25, 3, 3, 0, '2015-07-01 11:48:12', '2015-07-01 11:48:12'),
(123, 'O-067', 'Ihza Mahendra', 'Gunungkidul', '1999-10-26', 1, 'Islam', 12, 'Bedoyo Kidul, Bedoyo, Ponjong, Gunungkidul', 8.2, 4.2, 4.25, 5.25, 2, 1, 0, '2015-07-01 11:49:28', '2015-07-01 11:49:28'),
(124, 'O-037', 'Robiyanto', 'Gunungkidul', '1998-03-16', 1, 'Islam', 14, 'Jatisari, Sawahan, Ponjong', 8, 3.6, 3.75, 3.75, 2, 1, 0, '2015-07-01 11:50:32', '2015-07-01 11:50:32'),
(125, 'A-019', 'Retno Devi Cahyanti', 'Gunungkidul', '2000-03-11', 2, 'Islam', 24, 'Trengguno Lor, Sidorejo, Ponjong, Gunungkidul', 8, 3, 3.5, 4.5, 1, 3, 0, '2015-07-01 11:51:02', '2015-07-01 11:51:02'),
(126, 'O-040', 'Ganda Prasetyo', 'Gunungkidul', '1999-03-01', 1, 'Islam', 24, 'Trengguna Kidul, Sidorejo, Ponjong, Gunungkidul', 7.6, 5.4, 3, 5.75, 2, 1, 0, '2015-07-01 11:51:43', '2015-07-01 11:51:43'),
(127, 'O-065', 'Arum Nugroho', 'GunungKidul', '1999-05-08', 1, 'Islam', 12, 'Surubendo,Bedoyo,Ponjong', 8.6, 4.4, 4.25, 4.75, 2, 1, 0, '2015-07-01 11:52:51', '2015-07-01 11:52:51'),
(128, 'O-068', 'Rutimron Roni', 'Gunungkidul', '1999-05-26', 1, 'Islam', 12, 'Pringluwang, Bedoyo, Ponjong', 7.6, 4.2, 4.75, 5.25, 2, 2, 0, '2015-07-01 11:54:58', '2015-07-01 11:54:58'),
(129, 'M-073', 'Bani Dwi Maulana', 'Jakarta', '2000-05-03', 1, 'Islam', 24, 'Trengguno, Sidorejo, Ponjong, Gunungkidul', 8.4, 3.4, 4, 3.75, 3, 3, 0, '2015-07-01 11:55:19', '2015-07-01 11:55:19'),
(130, 'O-041', 'Rendra Andrea', 'Wonogiri', '2000-06-10', 1, 'Islam', 25, 'Ngulu Wetan, Pracimantoro ', 9.4, 6.4, 3, 6.25, 2, 2, 0, '2015-07-01 11:55:20', '2015-07-01 11:55:20'),
(131, 'O-043', 'Susilo Edi Wibowo', 'Gunungkidul', '2000-01-28', 1, 'Islam', 13, 'Condong, Botodayaan, Rongkop, Gunungkidul', 9, 6.8, 7, 8, 2, 1, 0, '2015-07-01 11:55:26', '2015-07-01 11:55:26'),
(132, 'O-052', 'Nasrul Yahya', 'Gunungkidul', '2000-01-11', 1, 'Islam', 4, 'Sawit Lor, Gombang, Ponjong, Gunungkidul', 9.2, 7.4, 8.25, 5.5, 2, 2, 0, '2015-07-01 11:56:09', '2015-07-01 11:56:09'),
(133, 'A-017', 'Alfina Dwiandani', 'Gunungkidul', '2000-06-14', 2, 'Islam', 12, 'Ngalasombo,Bedoyo,Ponjong,Gunungkidul', 7.8, 3.4, 4.5, 4, 1, 3, 0, '2015-07-01 11:56:11', '2015-07-01 11:56:11'),
(134, 'O-049', 'Toni Hartanto', 'Gunungkidul', '1999-02-15', 1, 'Islam', 3, 'Sriten, Karangwuni, Rongkop, Gunungkidul', 7.6, 3.6, 5.25, 6.25, 2, 2, 0, '2015-07-01 11:57:45', '2015-07-01 11:57:45'),
(135, 'O-070', 'Ivan Setyandi', 'Gunungkidul', '2000-06-25', 1, 'Islam', 12, 'Serut, Bedoyo, Ponjong, Gunungkidul', 8.6, 5, 3.5, 5.25, 2, 1, 0, '2015-07-01 11:58:18', '2015-07-01 11:58:18'),
(136, 'M-069', 'Novita Febrianti', 'Gunungkidul', '1999-11-14', 2, 'Islam', 13, 'Ngricik, Melikan, Rongkop, Gunungkidul', 9, 6.2, 8.5, 8.25, 3, 3, 0, '2015-07-01 11:58:39', '2015-07-01 11:58:39'),
(137, 'O-076', 'Muhammad Rifki Ikhsandi', 'Magelang', '2000-07-31', 1, 'Islam', 18, 'Klumpit,Kenteng', 8.6, 5.4, 3.5, 5.25, 2, 1, 0, '2015-07-01 11:58:49', '2015-07-01 11:58:49'),
(138, 'O-054', 'Arif Yanuar Jaya Melana', 'Gunungkidul', '2000-01-01', 1, 'Islam', 1, 'Kranggan, Ngeposari, Semanu, Gunungkidul', 7.2, 4.8, 7, 5.75, 2, 3, 0, '2015-07-01 11:59:43', '2015-07-01 11:59:43'),
(139, 'O-056', 'Ibnu Fajar Hermawan', 'Gunungkidul', '1999-07-19', 1, 'Islam', 18, 'Prampelan, Kenteng, Ponjong, Gunungkidul', 6.8, 3.2, 2.25, 3.5, 2, 1, 0, '2015-07-01 12:00:14', '2015-07-01 12:00:14'),
(140, 'O-055', 'Idham Zulianta', 'Gunungkidul', '2000-07-11', 1, 'Islam', 1, 'Prampelan 1, Kenteng, Ponjong, Gunugkidul', 8.6, 4.4, 5.25, 5.75, 2, 3, 0, '2015-07-01 12:00:47', '2015-07-01 12:00:47'),
(141, 'A-018', 'Putri Permata Sari', 'Wonogiri', '1999-07-12', 2, 'Islam', 13, 'Semamapir,Semugih,Rongkop,Gunungkidul', 9, 8.8, 3.75, 6.75, 1, 2, 0, '2015-07-01 12:01:11', '2015-07-01 12:01:11'),
(142, 'M-070', 'Bety Gusmiarni', 'Gunungkidul', '2000-08-03', 2, 'Islam', 13, 'Siyono, Petir, Rongkop, Gunungkidul', 8.4, 4.6, 3, 4.75, 3, 3, 0, '2015-07-01 12:01:18', '2015-07-01 12:01:18'),
(143, 'O-069', 'Riski Mistriono', 'Gunungkidul', '1999-04-05', 1, 'Islam', 12, 'Pringluwang, Bedoyo, Ponjong, Gunungkidul', 7, 4, 4, 4.5, 2, 1, 0, '2015-07-01 12:01:21', '2015-07-01 12:01:21'),
(144, 'O-74', 'Eko Prasetyo', 'Gunungkidul', '1998-07-28', 1, 'Islam', 12, 'Surubendo, Bedoyo, Ponjong, Gunungkidul', 4.8, 3.2, 3.75, 3.5, 2, 1, 0, '2015-07-01 12:02:35', '2015-07-01 12:02:35'),
(145, 'O-062', 'Endi Setyawan', 'GunungKidul', '2005-07-06', 1, 'Islam', 19, 'Suradadi,UmbulRejo,Ponjong', 7.8, 4.4, 3.75, 5.5, 2, 1, 0, '2015-07-01 12:03:08', '2015-07-01 12:03:08'),
(146, 'O-053', 'Dimas Pangestu', 'Gunungkidul', '2000-11-02', 1, 'Islam', 1, 'Jaten, Ponjong, Gunungkidul', 9, 7, 5.75, 8, 2, 2, 0, '2015-07-01 12:03:10', '2015-07-01 12:03:10'),
(147, 'O-081', 'Rio Yuda Prasetyo', 'Gunungkidul', '2000-04-07', 1, 'Islam', 12, 'Ngabean Lor, Karangasem, Ponjong,Gunungkidul', 6.4, 3.2, 4, 4.5, 2, 2, 0, '2015-07-01 12:04:41', '2015-07-01 12:04:41'),
(148, 'O-071', 'Galang Rifki Anggara', 'Gunungkidul', '2000-01-25', 1, 'Islam', 12, 'Bedoyo, Ponjong, Gunungkidul', 7.4, 6.6, 4.5, 5.25, 2, 1, 0, '2015-07-01 12:04:59', '2015-07-01 12:04:59'),
(149, 'O-045', 'Agung Nugroho Saputra', 'Purbalingga', '1999-05-25', 1, 'Islam', 27, 'Trengguna, Sidorejo, Ponjong, Gunungkidul', 7, 5.4, 2.75, 3.75, 2, 3, 0, '2015-07-01 12:06:35', '2015-07-01 12:06:35'),
(150, 'O-061', 'Doni Ariyanto', 'Gunungkidul', '2000-02-14', 1, 'Islam', 12, 'Pringluwang, Bedoyo, Ponjong, Gunungkidul', 6.8, 3.6, 5, 4.5, 2, 1, 0, '2015-07-01 12:07:07', '2015-07-01 12:07:07'),
(151, 'O-064', 'Kevin Putra Kurniawan', 'GunungKidul', '1999-12-20', 1, 'Katolik', 12, 'Bedoyo Lor,Bedoyo,Ponjong', 7.2, 6.2, 4.5, 6.5, 2, 1, 0, '2015-07-01 12:07:15', '2015-07-01 12:07:15'),
(152, 'O-063', 'Aldian', 'Gunungkidul', '1999-11-23', 1, 'Islam', 12, 'Kayangan, Pringombo, Rongkop, Gunungkuidul', 8, 6.2, 4.5, 3.5, 2, 2, 0, '2015-07-01 12:07:23', '2015-07-01 12:07:23'),
(153, 'O-050', 'Rohmat Ryzal Saputra', 'Gunungkidul', '1999-09-11', 1, 'Islam', 3, 'Weru, Petir, Rongkop, Gunungkidul', 8, 6, 5.25, 6.25, 2, 2, 0, '2015-07-01 12:07:59', '2015-07-01 12:07:59'),
(154, 'O-072', 'Arif Putra Pamu', 'Gunungkidul', '2000-06-29', 1, 'Islam', 12, 'Karang Asem,Ponjong,Gunungkidul', 9, 4.6, 3.25, 4.5, 2, 2, 0, '2015-07-01 12:08:09', '2015-07-01 12:08:09'),
(155, 'O-075', 'Aldi Dwianto', 'Gunungkidul', '1999-06-17', 1, 'Islam', 12, 'Surubendo, Bedoyo, Ponjong, Gunungkidul', 7.8, 4.2, 3.5, 4.75, 2, 1, 0, '2015-07-01 12:09:21', '2015-07-01 12:09:21'),
(156, 'O-085', 'Rico Agus Purwanto', 'Gunungkidul', '2000-08-15', 1, 'Islam', 19, 'Sunggingan, Umbulrejo, Ponjong', 9.8, 7.4, 2.75, 6.25, 2, 1, 0, '2015-07-01 12:09:46', '2015-07-01 12:09:46'),
(157, 'O-073', 'Octama Dika Mahendra', 'Banyumas', '1999-10-02', 1, 'Islam', 19, 'Suradadi,Umbulrejo,Ponjong', 6.8, 5, 3.25, 6, 2, 1, 0, '2015-07-01 12:11:03', '2015-07-01 12:11:03'),
(158, 'O-046', 'Aula Handika', 'Gunungkidul', '2000-03-06', 1, 'Islam', 28, 'Jimbaran, Tambakromo, Ponjong, Gunungkidul', 7.6, 7.6, 7.5, 7.75, 2, 3, 0, '2015-07-01 12:11:10', '2015-07-01 12:11:10'),
(159, 'O-080', 'Indra Surya Ramadan', 'Gunungkidul', '1999-12-09', 1, 'Islam', 12, 'Ngabean kidul, karangasem, Ponjong', 6.2, 4.8, 4.25, 3.75, 2, 1, 0, '2015-07-01 12:11:18', '2015-07-01 12:11:18'),
(160, 'O-066', 'Rutimron Ramo', 'Gunungkidul', '1999-05-26', 1, 'Islam', 12, 'Pringluang,Bedoyo,Ponjong,Gunungkidul', 8, 3.6, 4.75, 5.5, 2, 2, 0, '2015-07-01 12:12:32', '2015-07-01 12:12:32'),
(161, 'O-060', 'Anggoro Priyo Wicaksono', 'Gunungkidul', '1999-03-16', 1, 'Islam', 12, 'Bedoyo Kidul, Bedoyo, Ponjong, Gunungkidul', 8.2, 7, 7, 7, 2, 1, 0, '2015-07-01 12:12:40', '2015-07-01 12:12:40'),
(162, 'O-079', 'Dedi Pamungkas', 'Gunungkidul', '1997-11-16', 1, 'Islam', 12, 'Klepu, Karangasem, Ponjong, Gunungkidul', 6.2, 4.2, 3.5, 3.75, 2, 1, 0, '2015-07-01 12:12:46', '2015-07-01 12:12:46'),
(163, 'M-065', 'Tri Winarsih', 'Gunungkidul', '1999-02-07', 2, 'Islam', 21, 'Wuni, Nglindur, Girisubo, Gunungkidul', 6.8, 6.2, 4, 6.25, 3, 3, 0, '2015-07-01 12:14:31', '2015-07-01 12:14:31'),
(164, 'O-058', 'Diki Octavianto', 'GunungKidul', '1999-10-17', 1, 'Islam', 19, 'Sunggingan,Umbulrejo,Ponjong', 8.4, 5.8, 6.25, 7.75, 2, 1, 0, '2015-07-01 12:14:38', '2015-07-01 12:14:38'),
(165, 'O-057', 'Jalu Wira Buana', 'Gunungkidul', '2000-04-09', 1, 'Islam', 13, 'Pakel, Pringombo, Rongkop, Gunungkidul', 8, 3.8, 3.5, 3.5, 2, 3, 0, '2015-07-01 12:15:05', '2015-07-01 12:15:05'),
(166, 'M-061', 'Lina Listiyani', 'Gunungkidul', '2000-09-12', 2, 'Islam', 18, 'Cerme, Kenteng, Ponjong, Gunungkidul', 6.4, 4.8, 3.75, 5.5, 3, 1, 0, '2015-07-01 12:18:11', '2015-07-01 12:18:11'),
(167, 'M-064', 'Sinta Nuriani', 'GunungKidul', '2000-01-29', 2, 'Islam', 21, 'Wuni,Nglindur,Girisubo', 9.4, 5, 3.5, 5.25, 3, 3, 0, '2015-07-01 12:18:41', '2015-07-01 12:18:41'),
(168, 'M-057', 'Novita Mardiyanti', 'Gunungkidul', '1999-11-29', 2, 'Islam', 18, 'Betoro lor, Karangasem, Ponjong, Gunungkidul', 8.6, 7.6, 5.25, 7.75, 3, 1, 0, '2015-07-01 12:20:46', '2015-07-01 12:20:46'),
(169, 'M-050', 'Revy Anjan Syahnakri', 'GunungKidul', '1999-10-21', 1, 'Islam', 5, 'Semuluh Kidul,Ngeposari,Semanu', 8.2, 7.4, 6, 7.75, 3, 3, 0, '2015-07-01 12:21:55', '2015-07-01 12:21:55'),
(170, 'M-049', 'Roy Agus Prasetya', 'GunungKidul', '1999-08-30', 1, 'Islam', 12, 'Kebowan,Gombang,Ponjong', 9.6, 5.2, 4.25, 6.25, 3, 3, 0, '2015-07-01 12:24:31', '2015-07-01 12:24:31'),
(171, 'M-078', 'Ovi Siti Alifah', 'Gunungkidul', '1999-11-26', 2, 'Islam', 29, 'Karang ijo wetan', 8.6, 3.8, 3.25, 4.75, 3, 1, 0, '2015-07-01 12:24:57', '2015-07-01 12:24:57'),
(172, 'O-088', 'Oki Dedianto', 'Gunungkidul', '2000-04-13', 1, 'Islam', 23, 'Pringombo,Rongkop,Gunungkidul', 8.8, 4.6, 4, 4.25, 2, 3, 0, '2015-07-01 12:26:54', '2015-07-01 12:26:54'),
(173, 'O-089', 'Fani Lambang Susanto', 'Gunungkidul', '2000-04-13', 1, 'Islam', 13, 'Pringombo, Rongkop, Gunungkidul', 8.8, 5.6, 3.25, 5.75, 2, 3, 0, '2015-07-01 12:27:07', '2015-07-01 12:27:07'),
(174, 'M-076', 'Muhammad Rafi Akbar', 'Gunungkidul', '2000-09-19', 1, 'Islam', 29, 'Sumber lor,Ponjong', 8.6, 6, 4.75, 4, 3, 1, 0, '2015-07-01 12:27:23', '2015-07-01 12:27:23'),
(175, 'M-056', 'Nurul Aprilia Wati', 'GunungKidul', '1999-04-14', 2, 'Islam', 24, 'Trengguno,Sidorejo,Ponjong', 8.4, 4, 4, 5.25, 3, 1, 0, '2015-07-01 12:27:34', '2015-07-01 12:27:34'),
(176, 'O-083', 'Muhammad Ridlo Pamungkas', 'Cilacap', '2000-11-18', 1, 'Islam', 13, 'Semugih, Semugih, Rongkop, Gunungkidul', 8.4, 7, 4.5, 5.25, 2, 3, 0, '2015-07-01 12:28:25', '2015-07-01 12:28:25'),
(177, 'A-031', 'Gesta Revionessa Nurhuda', 'Gunungkidul', '2000-03-18', 1, 'Islam', 30, 'Bolo dukuh lor, Sidorejo, Ponjong', 6.6, 3.4, 3.5, 4.25, 1, 3, 0, '2015-07-01 12:29:34', '2015-07-01 12:29:34'),
(178, 'M-077', 'Chindi Wulan Dhufi', 'Gunungkidul', '2000-01-07', 2, 'Islam', 29, 'Sumber Kidul,Ponjong', 8.2, 4.8, 4.5, 4.75, 2, 1, 0, '2015-07-01 12:29:43', '2015-07-01 12:29:43'),
(179, 'M-053', 'Linda Novitasari', 'GunungKidul', '1999-07-30', 2, 'Islam', 18, 'Jati Karangasem Ponjong', 8.4, 4.4, 3, 5.25, 3, 1, 0, '2015-07-01 12:30:16', '2015-07-01 12:30:16'),
(180, 'O-084', 'Heru Prasetya', 'Gunungkidul', '1999-06-12', 1, 'Islam', 13, 'Gebang kulon', 8.4, 4.6, 3.75, 4.75, 2, 3, 0, '2015-07-01 12:30:54', '2015-07-01 12:30:54'),
(181, 'O-086', 'Wahyu', 'Gunungkidul', '2000-05-05', 1, 'Islam', 19, 'Pragak, Sunggingan, Umbulrejo, Ponjong, Gunungkidul', 8, 4.8, 2.5, 4, 2, 1, 0, '2015-07-01 12:31:37', '2015-07-01 12:31:37'),
(182, 'O-087', 'Wahyudi', 'Gunungkidul', '2000-05-05', 1, 'Islam', 19, 'Pragak, Sunggingan, Umbul Rejo, Ponjong', 8.4, 5.6, 3.5, 5.75, 2, 1, 0, '2015-07-01 12:31:53', '2015-07-01 12:31:53'),
(183, 'M-058', 'Ritma Anggitan', 'Gunungkidul', '2000-01-08', 2, 'Islam', 18, 'Betoro lor, Karangasem, Ponjong, Gunungkidul', 9, 6.8, 3.75, 6, 3, 1, 0, '2015-07-01 12:32:32', '2015-07-01 12:32:32'),
(184, 'A-029', 'Yoga Rahmatulloh', 'Gunungkidul', '1999-08-23', 1, 'Islam', 30, 'Slingi, Umbulrejo, Ponjong', 8, 3.8, 4, 4.75, 1, 3, 0, '2015-07-01 12:32:40', '2015-07-01 12:32:40'),
(185, 'M-055', 'Adinda Rizma Elfariyani', 'GunungKidul', '1999-11-26', 2, 'Islam', 1, 'Geblug,Kenteng,Ponjong', 8.4, 4.4, 3.5, 4.25, 3, 1, 0, '2015-07-01 12:33:45', '2015-07-01 12:33:45'),
(186, 'O-082', 'Dicky Setyawan', 'Gunungkidul', '2000-04-01', 1, '0', 13, 'Tambak ,Melikan,Rongkop,Gunungkidul', 8.6, 5.4, 7.25, 7.5, 2, 1, 0, '2015-07-01 12:34:32', '2015-07-01 12:34:32'),
(187, 'O-077', 'Andrey Oktavian', 'Gunungkidul', '1999-12-04', 1, 'Islam', 21, 'Wuni, Nglindur, Girisubo, Gunungkidul', 7.2, 3.4, 4.25, 4.5, 2, 1, 0, '2015-07-01 12:34:47', '2015-07-01 12:34:47'),
(188, 'A-030', 'Andiska Nurcahya Kusuma', 'Gunungkidul', '2000-07-18', 1, 'Islam', 30, 'Karangijo Kulon, Ponjong, Gunungkidul', 8.8, 4.2, 3.75, 5.5, 1, 3, 0, '2015-07-01 12:34:53', '2015-07-01 12:34:53'),
(189, 'M-048', 'Tikawati', 'Gunungkidul', '1999-11-01', 2, 'Islam', 12, 'Kayu areng,Pucanganom,Rongkop,Gunungkidul', 8.8, 6, 9, 7, 3, 2, 0, '2015-07-01 12:35:36', '2015-07-01 12:35:36'),
(190, 'M-059', 'Dwi Kurniawan', 'GunungKidul', '1999-12-29', 1, 'Islam', 23, 'Kerdonmiri Semugih,Rongkop', 8.2, 5.4, 4, 4, 3, 1, 0, '2015-07-01 12:36:40', '2015-07-01 12:36:40'),
(191, 'O-078', 'Alvin Al'' Azhar', 'Wonogiri', '1999-11-12', 1, 'Islam', 21, 'Sentul, Tileng, Girisubo, Gunungkidul', 8.6, 3.8, 3.5, 5.5, 2, 1, 0, '2015-07-01 12:38:10', '2015-07-01 12:38:10'),
(192, 'O-096', 'Ian Nuryana', 'Gunungkidul', '1999-05-24', 1, 'Islam', 30, 'Karangijo Kulon, Ponjong, Gunungkidul', 9, 5.8, 3.25, 5.5, 2, 1, 0, '2015-07-01 12:38:32', '2015-07-01 12:38:32'),
(193, 'O-092', 'Muhammad Alfiansyah', 'Gunungkidul', '1999-04-27', 1, 'Islam', 30, 'Karangijo Kulon, Ponjong, Gunungkidul', 8, 3.4, 4, 4.75, 2, 1, 0, '2015-07-01 12:41:42', '2015-07-01 12:41:42'),
(194, 'M-071', 'Dwi Nuryana', 'Jakarta', '2001-03-17', 2, 'Islam', 13, 'Kayangan, Pringombo, Rongkop, Gunungkidul', 8.4, 6.6, 7.5, 8.25, 3, 1, 0, '2015-07-01 12:45:10', '2015-07-01 12:45:10'),
(195, 'O-093', 'Ganjar Priwibowo', 'Gunungkidul', '2000-03-09', 1, 'Islam', 29, 'Plataran,sumbergiri,Ponjong,GK', 8.6, 3.6, 3.5, 4.75, 2, 1, 0, '2015-07-01 12:45:21', '2015-07-01 12:45:21'),
(196, 'O-090', 'Abdulrrahman Setyabudi', 'Gunungkidul', '1999-10-03', 1, 'Islam', 30, 'Karangijo Wetan, Ponjong, Gunungkidul', 8.6, 5.6, 4.25, 5.25, 2, 2, 0, '2015-07-01 12:48:44', '2015-07-01 12:48:44'),
(197, 'O-095', 'Andi Kurniawan', 'Gunungkidul', '2000-03-28', 1, 'Islam', 29, 'Gunungkidul', 8.2, 3.2, 5.25, 4.75, 2, 1, 0, '2015-07-01 12:49:40', '2015-07-01 12:49:40'),
(198, 'M-O72', 'Retno Putri  Anggraeni', 'Gunungkidul', '2000-03-22', 2, 'Islam', 13, 'Tambak, Melikan, Rongkop, Gk', 8.6, 5.2, 4.5, 6.25, 3, 1, 0, '2015-07-01 12:51:55', '2015-07-01 12:51:55');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `description_en` text NOT NULL,
  `position` int(11) NOT NULL,
  `status` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `synchronize_tokens`
--

CREATE TABLE IF NOT EXISTS `synchronize_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `key` int(11) DEFAULT NULL,
  `token_type` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tagged`
--

CREATE TABLE IF NOT EXISTS `tagged` (
  `id` varchar(36) NOT NULL,
  `foreign_key` varchar(36) NOT NULL,
  `tag_id` varchar(36) NOT NULL,
  `model` varchar(255) NOT NULL,
  `language` varchar(6) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `times_tagged` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_TAGGING` (`model`,`foreign_key`,`tag_id`,`language`),
  KEY `INDEX_TAGGED` (`model`),
  KEY `INDEX_LANGUAGE` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` varchar(36) NOT NULL,
  `identifier` varchar(30) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  `keyname` varchar(30) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `occurrence` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_TAG` (`identifier`,`keyname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `role_id` int(4) NOT NULL,
  `request_reset_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastlogin` datetime NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `avatar`, `phone`, `address`, `description`, `role_id`, `request_reset_token`, `lastlogin`, `created`, `modified`, `status`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, '34234', 'Ponjong', NULL, 1, '', '0000-00-00 00:00:00', NULL, NULL, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
